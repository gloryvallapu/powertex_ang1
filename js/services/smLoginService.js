shopMyToolsApp.service('smLoginService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.smloginMethod = function (logindata, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/sm_login',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "mail": logindata.username, "password": logindata.password, "usertype": usertype }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.acInvoiceDetailsget = function (userid, usertype, to_date, from_date) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/po_information?userid=' + userid + '&usertype=' + usertype + '&to_date=' + to_date + '&from_date=' + from_date,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.acInvoiceDetailsPost = function (usertype, inv_info, ponum, userId, invoice_date) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/inv_generation',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "usertype": usertype, "po_num": ponum, "invoice_info": inv_info, "user_id": userId, "invoice_date": invoice_date }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.finalInvoiceGeneration = function (inv_info, ponum, userId) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',
			url: HOMEPAGE_SERVICE + '/inv_generation',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "po_num": ponum, "acpt_carton_info": inv_info, "user_id": userId }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.Invoicepreparation = function (rfqno) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/inv_preparation?rfq_num=' + rfqno,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.whchallanpostMethod = function (po_num, inv_num, inv_info) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/invchalan_info',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "po_num": po_num, "inv_num": inv_num, "invoice_info": inv_info }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getEstimateQtyDetails = function (userType, userid) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/estimate_qty?usertype=' + userType + '&userid=' + userid,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getPackingListQtyDetails = function (userType) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/invoice_data?usertype=' + userType,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.prepareInvoice = function (po_num, inv_data) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',
			url: HOMEPAGE_SERVICE + '/estimate_qty',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "po_num": po_num, "estimated_qty_info": inv_data }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.printInvoice = function (po_num, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/pinv_print?po_num=' + po_num + '&usertype=' + usertype,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.printSingleInvoice = function (p_inv_num, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/print_invoice_info?p_inv_num=' + p_inv_num + '&usertype=' + usertype,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getChallanList = function (usertype, userid) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/chalan_info?usertype=' + usertype + '&userid=' + userid,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
	this.getReqChallanCompletedList = function (chalan_num) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/invchalan_info?chalan_num=' + chalan_num,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getChallanCompletedList = function (userid, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/chalan_data?userid=' + userid + '&usertype=' + usertype,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.generateInwardNo = function (userid) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/inv_get?userid=' + userid,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.generateInward = function (inv_num) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/inward_num',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "inv_num": inv_num }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getInwardList = function (userid, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/inward_num?userid=' + userid + '&usertype=' + usertype,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.generateGRN = function (grnList, inward_num, inward_date, userid) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/grn_num_gen',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "more_info": grnList, "inward_num": inward_num, "inward_date": inward_date, "userid": userid, "exchange_date": "", "currency": "" }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getGrnList = function (userid) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/deal_order_info?userid=' + userid,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getPoRecords = function (userid, userType) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/pohistory?userid=' + userid + '&usertype=' + userType,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getInvoiceRecords = function (userid, userType) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/invhistory?userid=' + userid + '&usertype=' + userType,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

})