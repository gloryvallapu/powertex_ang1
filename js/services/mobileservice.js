shopMyToolsApp.service('getmobileservice', function ($q, $http, HOMEPAGE_SERVICE) {
	this.getaddress = function (mbl) {
        // alert(mbl)
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/customer_info?mobile='+mbl,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W'  },
			// data: { "ip_address": ipAddress, "user_type": "web" }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
    };
    
    	this.userRegistration = function (registrationData,shippingaddress) {
		var deferred = $q.defer();

		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/custpos_regd',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' , 'secret_key': '4r5t@W' },
			data: {
				"firstname": registrationData.firstname, "lastname": registrationData.lastname,
				"mobile": registrationData.user_mobile, "email": registrationData.email,
				 "shippingaddress":shippingaddress,"billing_address":shippingaddress
			}

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.getproductdetails=function(bar){
		//alert(bar)
			var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/barcode?barcode='+bar,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' , 'secret_key': '4r5t@W' },
			// data: {
			// 	"barcode":bar
			// }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	}

	this.saveorder=function(invoiceDate,userId,moreInfo){
		//alert(bar)
			var deferred = $q.defer();

		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/pos_invoice',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' , 'secret_key': '4r5t@W' },
			data: {
				"invoice_date":invoiceDate,"userid":userId,"moreinv_info":moreInfo
			}

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	}

	this.getPosf_invMethod=function(f_inv,mobile,userid){
			var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/pos_print_invoice?f_inv_num='+f_inv+'&mobile='+mobile+'&userid='+userid,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' , 'secret_key': '4r5t@W' },
		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	}


	

})