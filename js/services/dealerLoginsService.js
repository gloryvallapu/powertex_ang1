shopMyToolsApp.service('dealerloginService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.dealerloginAuthentication = function (obj) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/dealer_login_logout',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "user_type": "Dealer", "mail": obj.username, "password": obj.password }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.dealerlogout = function (token) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/dealer_logout',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "token": token }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.smlogoutMethod = function (token) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/sm_logout',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "token": token }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};


	this.dealerdata = function (obj) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/dealer_data',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "user_type": "Dealer", "mail": obj.username, "password": obj.password }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	


});


shopMyToolsApp.service('dealerregService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.dealerregistrationAuthentication = function (obj) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/dealer_register',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
			data: {
				"user_type": obj.registerType, "dealer_name": obj.dealername, "dealer_password": obj.delpwd, "dealer_shopname": obj.delshopname, "reg_shopname": obj.dregshopname, "dealer_cin": obj.delcin, "dealer_country": obj.delcountry,
				"dealer_state": obj.delstate, "dealer_city": obj.delcity, "dealer_town": obj.deltown, "dealer_pin": obj.delpincode, "dealer_isd": obj.delisd,
				"dealer_phone": obj.delphone, "dealer_mail": obj.delemail, "dealer_gstin": obj.delgst, "dealer_mobile": obj.delmobile, "address1": obj.address1, "address2": obj.address2, "address3": obj.address3
			}
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
});

shopMyToolsApp.service('smLoginService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.smloginMethod = function (obj) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/sm_login',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "mail": obj.username, "password": obj.password }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

})
shopMyToolsApp.service('adminLoginService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.adminLoginMethod = function (obj) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/admin_login',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "mail": obj.username, "password": obj.password }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.adminLogoutMethod = function (token) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/admin_logout',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "token": token }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

})