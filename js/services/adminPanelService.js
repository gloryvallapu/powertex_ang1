shopMyToolsApp.service('adminPanelService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.adminPanelAuthentication = function (obj) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/users_saving',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: {
				"usertype": obj.usertype, "username": obj.userName, "user_email": obj.userEmail, "user_mobile": obj.userMobile, "user_password": obj.userPwd, "user_city": obj.userCity, "branch_code": obj.branch_code, "branchName": obj.branch, "shopee_type": '', "accounts": obj.acuser_mail, "wh": obj.whuser_mail,
			}
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.masterDataExcelupload = function (file) {
		var deferred = $q.defer();
		// var fd = new FormData();
		// fd.append('file', file);
		// fd.append('filetype', 'xls');
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/masterdata_upload',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: {
				"prod_excel": file
			}
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.masterAttrDataupload = function (file) {
		var deferred = $q.defer();
		// var fd = new FormData();
		// fd.append('file', file);
		// fd.append('filetype', 'xls');
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/attributes_upload',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: {
				"attribute_excel": file
			}
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
	this.masteruploadedHistoryData = function (file) {
		var deferred = $q.defer();
		// var fd = new FormData();
		// fd.append('file', file);
		// fd.append('filetype', 'xls');
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/prod_history',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.masterdatadownload = function (type) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/masterdata_upload?datatype=' + type,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.companyProfileService = function (companyProfile) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/company_details',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { companyProfile }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.companyShptypeService = function (dealer, branch, subdealer) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/branch_dealer_details?dealer=' + dealer + '&branch=' + branch + '&sub_dealer=' + subdealer,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getDealers = function (branch_code) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/branch_dealers?branch_code=' + branch_code,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getDealersForSmAssignment = function (branch_code) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/dealer_info?branch_code=' + branch_code,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.updateManagerForSalaes = function (sales_email, acc_email, wh_email) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',
			url: HOMEPAGE_SERVICE + '/users_saving',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "sales_email": sales_email, "acc_email": acc_email, "wh_email": wh_email }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};




});

shopMyToolsApp.service('adminPanelGetService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.adminPanelGetAuthentication = function () {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/users_details',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
	this.deleteuserdetails = function (userid) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/users_delete?userid=' + userid,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
	this.sMasigneeMethod = function (branch_code) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/usersList?branch_code=' + branch_code,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};




});

shopMyToolsApp.service('dealerVerificationService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.dealerVerificationAuthentication = function () {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/dealer_details',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.dealerHistory = function () {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/dealer_history',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.activeDealersInfo = function (branch_code) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/dealer_branches?branch_code=' + branch_code,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.dealerActiveMethod = function (dealerData, mail, billing_addr, shipping_addr) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/dealer_active',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: {
				"dealer_mail": mail, "adhar_num": dealerData.adhar, "yr_exp_field": dealerData.exp, "pan": dealerData.pan, "turnover": dealerData.turnover, "billing_address": billing_addr, "shipping_address": shipping_addr
			}
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getProcessDealersForShopee = function () {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/dealer_active',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getActiveDealersInfo = function (branch_code) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/deal_based_branch?branch_code=' + branch_code,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getActiveDealersSMupdate = function (dealer_mail, sales_mail, sales_mobile) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',
			url: HOMEPAGE_SERVICE + '/deal_based_branch',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "dealer_mail": dealer_mail, "sales_mail": sales_mail, "sales_mobile": sales_mobile }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

});

shopMyToolsApp.service('delVerificationPostService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.delVerificationPostAuthentication = function (mail, usertype, branch, branch_code, status) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',

			url: HOMEPAGE_SERVICE + '/dealer_accept',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: {
				"status": status, "dealer_mail": mail, "branch": branch, "branch_type": usertype, "admin_remarks": "good", "branch_code": branch_code
			}
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.dealerActivation = function (dealer_data) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',

			url: HOMEPAGE_SERVICE + '/dealer_active',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { dealer_data }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

});