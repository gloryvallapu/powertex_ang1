shopMyToolsApp.service('product_detailed_service', function ($q, $http, PRODUCT_DETAIL_SERVICE) {

	this.getAllDetailsOfProduct = function (productName) {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: PRODUCT_DETAIL_SERVICE + '/productdetails?product_name=' + productName,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', "secret_key": "4r5t@W" }

		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
});


shopMyToolsApp.service('notify_service', function ($q, $http, PRODUCT_DETAIL_SERVICE) {

	this.notifyMethod = function (email, uploadName) {

		var deferred = $q.defer();

		$http({
			method: 'POST',
			url: PRODUCT_DETAIL_SERVICE + '/notify',
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
			data: { "productname": uploadName, "email": email }

		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
});

// shopMyToolsApp.service('reviews_service', function ($q, $http, PRODUCT_DETAIL_SERVICE) {

// 	this.reviewsMethod = function (riviewRating, userName, reviewSummery, mobileNumber, ratingComments, productDescription, userId) {

// 		var deferred = $q.defer();


// 		$http({
// 			method: 'POST',
// 			url: PRODUCT_DETAIL_SERVICE + '/reviews',
// 			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
// 			data: { "rating": riviewRating, "user_name": userName, "review": reviewSummery, "mobile_number": mobileNumber, "rating_comments": ratingComments, "user_id": userId, "prod_desc": productDescription }
// 			//alert(url);


// 		}).then(function success(data) {
// 			deferred.resolve(data);
// 		}, function error(data) {
// 			deferred.reject(data);
// 		});
// 		return deferred.promise;
// 	};
// });

shopMyToolsApp.service('referralEmailservice', function ($q, $http, PRODUCT_DETAIL_SERVICE) {

	this.sendEmailMethod = function (referalEmail, product, userEmail, productImg) {

		var deferred = $q.defer();


		$http({
			method: 'POST',
			url: PRODUCT_DETAIL_SERVICE + '/refer',
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
			data: { "prod_name": product, "email": userEmail, "refer_email": referalEmail, "prod_photo": productImg }

		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
});

shopMyToolsApp.service('newHomePageService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.newHomePageMethod = function (link, from) {

		var deferred = $q.defer();


		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/newhomepage?from=' + from + '&link=' + link,
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.selectdataMethod = function (link, from) {

		var deferred = $q.defer();


		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/selectdata?from=' + from + '&link=' + link,
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.offersMethod = function (subCatObj, userid) {

		var deferred = $q.defer();


		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/offersproducts?upload_subcategory=' + subCatObj + '&userid=' + userid,
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.newArrivalMethod = function (subCatObj, userid) {

		var deferred = $q.defer();


		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/newarrivalsproducts?upload_subcategory=' + subCatObj + '&userid=' + userid,
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.addNewWishListMethod = function (userId, productName) {
		var deferred = $q.defer();

		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/newwishlist',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "user_id": userId, "product_name": productName }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.yourWishlistMethod = function (userid) {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/newyourwishlist?user_id=' + userid,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.removeWishListItemMethod = function (userId, productName) {
		var deferred = $q.defer();

		$http({
			method: 'DELETE',
			url: HOMEPAGE_SERVICE + '/removewishlist',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "user_id": userId, "product_name": productName }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};


	this.getNewCategoriesMethod = function () {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/newcategories',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.searchProductsMethod = function (productName) {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/newmatchproduct?product_name=' + productName,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.searchProductsMoreMethod = function (productName) {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/newsearchproduct?product_name=' + productName,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.postcompareProductsMethod = function (compareProductList, userID) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/compareproduct',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "product_name": compareProductList, "user_id": userID }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.compareProductsMethod = function (compareProductList, userID) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/add_compare_product',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "productname": compareProductList, "user_id": userID }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.getCompareProductsMethod = function (userID) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/comp_prod_details?user_id=' + userID,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			//		data: { "product_name": compareProductList ,"user_id":userID}

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.delCompareProductsMethod = function (productName, userID, clear) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/comp_prod_delete?compare_products=' + clear,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "product_name": productName, "user_id": userID }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};






});

shopMyToolsApp.service('newProductDetailedService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.getnewDetailsOfProduct = function (productName) {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/productdetails?product_name=' + productName,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', "secret_key": "4r5t@W" }

		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.newreviewsMethod = function (riviewRating, userName, reviewSummery, mobileNumber, ratingComments, productDescription, userId) {

		var deferred = $q.defer();


		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/reviews',
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
			data: { "rating": riviewRating, "user_name": userName, "review": reviewSummery, "mobile_number": mobileNumber, "rating_comments": ratingComments, "user_id": userId, "prod_desc": productDescription }
			//alert(url);


		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getnewAllCategoriesFilterOfProduct = function (category, subCategoryName, brandName, pricerange, fromVal, toVal, val) {
		var deferred = $q.defer();

		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/newproductlist',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "category": category, "from": fromVal, "to": toVal, "brand": brandName, "subcategory": subCategoryName, "pricerange": pricerange, "val": val }

		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

});

shopMyToolsApp.service('distributorService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.getVendorsList = function () {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/save_vendor',
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' }

		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};


	this.getdistributortcat = function () {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/catsubbrand',
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' }

		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getdistributortSubcat = function (Category) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/subbrand?category=' + Category,
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
			data: { "category": Category }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
	this.getdistributorbrand = function (subCategory) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/sub_brand?subcategory=' + subCategory,
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
			data: { "subcategory": subCategory }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
	this.getdistributortcatget = function (category, subcategory, brand, vendor, userid) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/catsubcatbrandget',

			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
			data: { "category": category, "sub_category": subcategory, "brand": brand, "vendor_code": vendor, "userid": userid }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getvolumediscount = function (qty, prodname, userid) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/volumediscount?qty=' + qty + '&upload_name=' + prodname + '&userid=' + userid,
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' }
			// from=10-03-2018&link=Smt
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.submitdealerorderdetails = function (items, userid, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/dealer_order',
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
			data: { "product_info": items, "userid": userid, "user_type": usertype }
			//alert(url);
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.delaerPricedetails = function (qty, prodname, percentage) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/price',
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
			data: { "qty": qty, "prod_name": prodname, "percentage": percentage }
			//alert(url);
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.addToCartmethod = function (product, userId) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/dealer_addtocart',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "orderitem": product, "user_id": userId, "order_status": "init" }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});
		return deferred.promise;
	};

	this.updateViewCart = function (item, userId) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',
			url: HOMEPAGE_SERVICE + '/dealer_addtocart',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "orderitem": item, "user_id": userId }

		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};



	this.deleteDealerCartItemData = function (data, userId) {
		var deferred = $q.defer();
		$http({
			method: 'DELETE',
			url: HOMEPAGE_SERVICE + '/dealer_addtocart?userid=' + userId + '&product=' + data,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});

		return deferred.promise;
	};

	this.addedCartView = function (userId, random_no) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/dealer_addtocart?userid=' + userId + '&random_no=' + random_no,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			defrred.reject(data);
		});
		return deferred.promise;
	};

});

shopMyToolsApp.service('CartService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.addToCartmethod = function (prodname, qty, userId, random_no) {
		var deferred = $q.defer();

		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/initiateorder',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "productdescription": prodname, "qty": qty, "user_id": userId, "random_no": "" }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.addToCartcustomer = function (cartitems, randomno, userid) {
		var deferred = $q.defer();

		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/addtocart_site',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "user_id": userid, "random_no": randomno, "orderitem": cartitems }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.viewCartcustomer = function (randomno, userid) {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/addtocart_site?userid=' + userid + '&random_no=' + randomno,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },


		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};
	this.updateviewCartcustomer = function (prodname, qty, random_no, userid) {
		var deferred = $q.defer();

		$http({
			method: 'PUT',
			url: HOMEPAGE_SERVICE + '/addtocart_site',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "user_id": userid, "random_no": random_no, "product_name": prodname, "qty": qty }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.deleteCartItemcust = function (productname, userid, randomno) {
		var deferred = $q.defer();

		$http({
			method: 'DELETE',
			url: HOMEPAGE_SERVICE + '/addtocart_site',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "product": productname, "userid": userid, "random_no": randomno }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

});

shopMyToolsApp.service('pogenerationService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.pogeneration = function (rfqno) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/po_generation?rfq_num=' + rfqno,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}
	this.splitconformMethod = function (status, userid, rfqlistdata, rfqno) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/po_generation',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "product_status": status, "userid": userid, "accept_products": rfqlistdata, "rfq_num": rfqno }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.splitdataMethod = function (userid, rfqno) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/split_data?user_id=' + userid + '&rfq_num=' + rfqno,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}
	this.updateSplitconformMethod = function (userid, rfqlistdata, rfqno) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',
			url: HOMEPAGE_SERVICE + '/po_generation',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "userid": userid, "accept_products": rfqlistdata, "rfq_num": rfqno }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

});


shopMyToolsApp.service('dealerdashboardService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.pendingrfqMethod = function (usertype, userid) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/rfq_get?usertype=' + usertype + '&userid=' + userid,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}
	this.getpendingrfqProductDetails = function (rfqno) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/rfq_details?rfq_num=' + rfqno,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}
	this.dealersplitconfirm = function (data, rfqno, user_type) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/dealerorder_confirm',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "rfq_num": rfqno, "data": data, "user_type": user_type }
		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.dealerrfqdeleteMethod = function (rfq_no, prod_id) {
		var deferred = $q.defer();
		$http({
			method: 'DELETE',
			url: HOMEPAGE_SERVICE + '/rfq_del',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "rfq_num": rfq_no, "product_id": prod_id }
		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	}


	// this.pendingPoMethod =function(rfqno){
	// 	var deferred = $q.defer();
	// 	$http({
	// 		method: 'GET',
	// 		url: HOMEPAGE_SERVICE + '/po_details?rfq_num='+rfqno,
	// 		headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

	// 	}).then(function success(data) {
	// 		deferred.resolve(data);

	// 	}, function error(data) {
	// 		deferred.reject(data);

	// 	});

	// 	return deferred.promise;

	// }




});


shopMyToolsApp.service('podashboardService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.pendingPodetails = function (userid, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/po_details?userid=' + userid + '&usertype=' + usertype,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.PostPoApproved = function (rfqno, status, userid) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/po_details',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "rfq_num": rfqno, "status": status, "userid": userid }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}
	this.getPoDetailsDealer = function (userid) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/dealer_po_details?user_id=' + userid,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

});

shopMyToolsApp.service('userProfileService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.userprofileDetails = function (userid, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/users_profile?userid=' + userid + '&usertype=' + usertype,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.addAddress = function (userid, shipping_address, billing_address, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',
			url: HOMEPAGE_SERVICE + '/users_profile?userid=' + userid,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "shipping_address": shipping_address, "billing_address": billing_address, "usertype": usertype }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.deleteAddress = function (userid, usertype, addunique_nbr) {
		var deferred = $q.defer();
		$http({
			method: 'DELETE',
			url: HOMEPAGE_SERVICE + '/users_profile',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "userid": userid, "usertype": usertype, "addunique_nbr": addunique_nbr }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	// this.addcustAddress = function (userid, shipping_addr, billing_addr) {
	// 	var deferred = $q.defer();
	// 	$http({
	// 		method: 'POST',
	// 		url: HOMEPAGE_SERVICE + '/add_cust_address',
	// 		headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
	// 		data: { "billingaddress": billing_addr, "shippingaddress": shipping_addr, "userid": userid }

	// 	}).then(function success(data) {
	// 		deferred.resolve(data);

	// 	}, function error(data) {
	// 		deferred.reject(data);

	// 	});

	// 	return deferred.promise;

	// }

	this.editmobilenum = function (userid, mobile_num, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/mobile_update',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "usertype": usertype, "mobile": mobile_num, "userid": userid }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}
	this.enterotp = function (userid, mobile, otp, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',
			url: HOMEPAGE_SERVICE + '/mobile_update',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "userid": userid, "mobile": mobile, "otp": otp, "usertype": usertype }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.setAsDefault = function (userid, unique_nbr, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',
			url: HOMEPAGE_SERVICE + '/setas_default_add',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "userid": userid, "addunique_nbr": unique_nbr, "user_type": usertype }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.getAllItemscount = function (userID, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/wishadd_count?userid=' + userID + '&usertype=' + usertype,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};






});


shopMyToolsApp.service('checkoutService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.getDealersListMethod = function (lat_long, pincode) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/shoplist',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "lat_long": lat_long, "pincode": pincode }
		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}
	this.pincodecheckMethod = function (pincode) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/pincdechk?pincode=' + pincode,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.saveOrderMethod = function (orderArray) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/checkout',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: orderArray,
		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.getpayuDetailsMethod = function () {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/payudata',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'payu_secret_key': '4r5s@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.sendEnquiry = function (shop) {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/enquiry_sent?shop=' + shop,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};







});








