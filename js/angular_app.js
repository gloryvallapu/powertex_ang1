var shopMyToolsApp = angular.module('shopMyTools', ['ngRoute', 'ui.bootstrap', 'ngMessages', 'slickCarousel', 'angular-steps', 'xlsx-model']);


// function preventBack(){window.history.forward();}
// setTimeout("preventBack()", 0);
// window.onunload=function(){null};


// document.addEventListener('contextmenu', function(e) {
//   e.preventDefault();
// });




// http://localhost/powertex/Gayathri/powertex/#/resetPassword/%235bab638b99306d148c525aeb
// http://pptshopee.com/#/resetPassword/%235bab638b99306d148c525aeb

var uri = window.location.href;
var userArray = uri.split("/%23");
var userId = userArray[1];
var url = "http://pptshopee.com/#/resetPassword/%23" +userId;
if (window.location.href== url) {
  var uri = decodeURIComponent(url);
  window.location.href = uri;
}






/* currency controller code start here 16-1-2018  */
shopMyToolsApp.filter('noFractionCurrency',
  ['$filter', '$locale', function (filter, locale) {
    var currencyFilter = filter('currency');
    var formats = locale.NUMBER_FORMATS;
    return function (amount, currencySymbol) {
      var value = currencyFilter(amount, currencySymbol);
      var sep = value.indexOf(formats.DECIMAL_SEP);
      // console.log(formats.DECIMAL_SEP);
      if (amount >= 0) {
        return value.substring(0, sep);
      }
      return value.substring(0, sep) + ')';
    };
  }]);
/* currency controller code end here 16-1-2018  */

shopMyToolsApp.filter('slice', function () {
  return function (arr, start, end) {
    return arr.slice(start, end);
  };
});





shopMyToolsApp.directive('loading', function () {
  return {
    restrict: 'E',
    replace: true,
    template: '<div class="loading" id="overlay"><img src="http://i49.tinypic.com/j5z8mb.gif" width="80" height="80" /></div>',
    link: function (scope, element, attr) {
      scope.$watch('loading', function (val) {
        if (val)
          $(element).show();
        else
          $(element).hide();
      });
    }
  }
});






shopMyToolsApp.config(['$routeProvider', '$locationProvider', '$httpProvider',
  function ($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/home.html'
      })

      .when('/invoiceOrders', {
        templateUrl: 'views/invoiceOrders.html'
      })
      .when('/dashboardrr', {
        templateUrl: 'views/dashboard.html'
      })
      .when('/checkout', {
        templateUrl: 'views/checkoutPage.html'
      })
      .when('/compareProducts', {
        templateUrl: 'views/compareProducts.html'
      })
      .when('/brandPage', {
        templateUrl: 'views/brandPage.html'
      })

      .when('/searchPage', {
        templateUrl: 'views/search.html'
      })
      .when('/searchPage1', {
        templateUrl: 'views/search1.html'
      })
      .when('/payu', {
        templateUrl: 'views/payu_biz.html'
      })
      .when('/collections', {
        templateUrl: 'views/collections.html'
      })
      .when('/emergingBrands', {
        templateUrl: 'views/emergingBrands.html'
      })
      .when('/addressBook', {
        templateUrl: 'views/addressBook.html'
      })
      .when('/addAddress', {
        templateUrl: 'views/addAddress.html'
      })
      .when('/editAccount', {
        templateUrl: 'views/editAccount.html'
      })
      .when('/productDetailPage', {
        templateUrl: 'views/brand_Detail_Page.html',
        cache: false,
      })
      .when('/categorylist', {
        templateUrl: 'views/categories_page.html'
      })
      .when('/categoryPage1', {
        templateUrl: 'views/categories_page1.html'
      })
      .when('/myorders', {
        templateUrl: 'views/myorders.html'
      })
      .when('/pendingOrders', {
        templateUrl: 'views/pendingorders.html'
      })

      .when('/invoice', {
        templateUrl: 'views/invoice.html'
      })
      .when('/viewCart', {
        templateUrl: 'views/viewCart.html'
      })
      .when('/topBrands', {
        templateUrl: 'views/topbrands.html'
      })
      .when('/resetPassword', {
        templateUrl: 'views/resetPassword.html'
      })
      .when('/deals', {
        templateUrl: 'views/deals.html'
      })
      .when('/coupons', {
        templateUrl: 'views/coupons.html'
      })
      .when('/createRFQ', {
        templateUrl: 'views/createRFQ.html'
      })

      .when('/dealerViewCart', {
        templateUrl: 'views/dealerViewCart.html'
      })
      .when('/distributorRegistration', {
        templateUrl: 'views/distributorRegistration.html'
      })
      .when('/rfqValidationpage', {
        templateUrl: 'views/rfqValidationpage.html'
      })
      .when('/Dashboard', {
        templateUrl: 'views/dealerDashbord.html'
      })
      .when('/pendingrfq', {
        templateUrl: 'views/pendingrfq.html'
      })
      .when('/dealerverification', {
        templateUrl: 'views/dealerverification.html'
      })
      .when('/internalusers', {
        templateUrl: 'views/internalusers.html'
      })
      .when('/splitedDealerData', {
        templateUrl: 'views/splitedDealerData.html'
      })
      .when('/pendingpo', {
        templateUrl: 'views/smpendingpo.html'
      })
      .when('/dealerpendingpo', {
        templateUrl: 'views/dealerpendingpo.html'
      })
      .when('/proformaInvoice', {
        templateUrl: 'views/proformaInvoice.html'
      })
      .when('/packingListPreparation', {
        templateUrl: 'views/packingListPreparation.html'
      })
      .when('/invoicePreparation', {
        templateUrl: 'views/invoicePreparation.html'
      })
      .when('/admin_shop_details', {
        templateUrl: 'views/admin_shop_details.html'
      })
      .when('/master_data_upload', {
        templateUrl: 'views/master_data_upload.html'
      })
      .when('/offer_products_form', {
        templateUrl: 'views/offer_products_form.html'
      })
      .when('/finalInvoiceGeneration', {
        templateUrl: 'views/finalInvoiceGeneration.html'
      })
      .when('/checkout_page', {
        templateUrl: 'views/checkout_page.html'
      })
      .when('/print_invoice_page', {
        templateUrl: 'views/print_invoice_page.html'
      })
      .when('/packingListPrint', {
        templateUrl: 'views/packingListPrint.html'
      })
      .when('/packingList', {
        templateUrl: 'views/packingList.html'
      })
      .when('/challanCompleted', {
        templateUrl: 'views/challanCompleted.html'
      })
      .when('/challanGeneration', {
        templateUrl: 'views/challanGeneration.html'
      })
      .when('/myProfile', {
        templateUrl: 'views/myProfile.html'
      })
      .when('/barcode_page', {
        templateUrl: 'views/barcode_page.html'
      })
      .when('/wishlist', {
        templateUrl: 'views/wishlist.html'
      })
      .when('/dealersHistory', {
        templateUrl: 'views/dealersHistory.html'
      })
      .when('/success', {
        templateUrl: 'views/success.html'
      })
      .when('/finv_print', {
        templateUrl: 'views/finv_print.html'
      })
      .when('/challanPrint', {
        templateUrl: 'views/challanPrint.html'
      })
      .when('/offers_products_page', {
        templateUrl: 'views/offers_products_page.html'
      })
      .when('/newArraivals_page', {
        templateUrl: 'views/newArraivals_page.html'
      })
      .when('/registration', {
        templateUrl: 'views/registration.html'
      })
      // .when('/forgotPassword', {
      //   templateUrl: 'views/forgotPassword.html'
      // })
      .when('/resetPassword', {
        templateUrl: 'views/resetPassword.html'
      })
      .when('/po_print_page', {
        templateUrl: 'views/po_print_page.html'
      })
      .when('/company_profile', {
        templateUrl: 'views/company_profile.html'
      })
      .when('/inwardGeneration', {
        templateUrl: 'views/inwardGeneration.html'
      })
      .when('/grnPreparation', {
        templateUrl: 'views/grnPreparation.html'
      })
      .when('/dealerSegregation', {
        templateUrl: 'views/dealerSegregation.html'
      })
      .when('/dealerShopInfo', {
        templateUrl: 'views/dealerShopInfo.html'
      })
      .when('/shopeeCreation', {
        templateUrl: 'views/shopeeCreation.html'
      })
      .when('/dealerOrderedList', {
        templateUrl: 'views/dealerOrderedList.html'
      })
      .when('/po_history', {
        templateUrl: 'views/po_history.html'
      })
      .when('/invoice_history', {
        templateUrl: 'views/invoice_history.html'
      })
      .when('/pos_finv', {
        templateUrl: 'views/pos_finv_print.html'
      })
      .when('/activeDealersInfo', {
        templateUrl: 'views/activeDealersInfo.html'
      })

    .otherwise({
      redirectTo: '/'
    });


    //  $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('');


  }]);



shopMyToolsApp.directive('myEnter', function () {
  return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
      if (event.which === 13) {
        scope.$apply(function () {
          scope.$eval(attrs.myEnter);
        });

        event.preventDefault();
      }
    });
  };
});



var cartArray = [], compareProducts = [], dealProductPrice = [];

var localCartArray = [];

var orderArray = [];


shopMyToolsApp.filter('titleCase', function () {
  return function (input) {
    input = input || '';
    return input.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
  };
});



shopMyToolsApp.filter("rounded", function () {
  return function (value) {
    value = parseFloat(value).toFixed(2);
    return value;
  }
});

shopMyToolsApp.directive('errSrc', function () {
  return {
    link: function (scope, element, attrs) {
      element.bind('error', function () {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });

      attrs.$observe('ngSrc', function (value) {
        if (!value && attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
    }
  }
});

