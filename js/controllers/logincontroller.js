shopMyToolsApp.controller('loginController', ['$scope', '$http', '$location',
  '$rootScope', 'loginService', 'registrationService', '$window', 'dealerloginService', 'smLoginService', 'adminLoginService', 'dealerloginService', '$route', '$modal',
  function ($scope, $http, $location, $rootScope, loginService, registrationService, $window, dealerloginService, smLoginService, adminLoginService, dealerloginService, $route, $modal) {
    $window.scrollTo(0, 0);

    $rootScope.delregdata = {};
    $scope.wishlistShow = true;
    $rootScope.delregdata.dealername = window.localStorage['username'];
    $rootScope.delregdata.shopname = window.localStorage['shopname'];
    $rootScope.delregdata.regshopname = window.localStorage['reg_shopname'];
    $rootScope.delregdata.dealer_cin = window.localStorage['dealer_cin'];
    $rootScope.delregdata.country = window.localStorage['country'];
    $rootScope.delregdata.state = window.localStorage['state'];
    $rootScope.delregdata.city = window.localStorage['city'];
    $rootScope.delregdata.town = window.localStorage['town'];
    $rootScope.delregdata.isd = window.localStorage['ISD'];
    $rootScope.delregdata.phone = window.localStorage['phone'];
    $rootScope.delregdata.pincode = window.localStorage['pincode'];
    $rootScope.delregdata.email = window.localStorage['email'];
    $rootScope.delregdata.gst = window.localStorage['gstin'];
    $rootScope.delregdata.mobile = window.localStorage['mobile'];
    $rootScope.delregdata.address = window.localStorage['address'];
    $rootScope.delregdata.dealerStatus = window.localStorage['dealer_status'];
    $rootScope.dealerusertype = window.localStorage['dealerusertype']
    $rootScope.dealerdataShow = true;
    if ($rootScope.dealerStatus == "Accept") {
      $rootScope.dealerdataShow = true;
    }
    else {
      $rootScope.dealerdataShow = false;
    }

    if (localStorage.getItem('previousUrl')) {
      $scope.previousUrlArray = localStorage.getItem('previousUrl').split("#!/");
      $scope.previousUrl = $scope.previousUrlArray[1];
    }


    // var url = "//freegeoip.net/json/";
    // $http.get(url).then(function (response) {
    //   console.log(response.data.ip);
    //   $rootScope.ip = response.data.ip;
    //   alert($rootScope.ip);
    // });

    $scope.home = function () {
      window.location.href = "./";
    }

    $scope.fblogin = function () {
      //alert("hai");
      FB.login(function (response) {
        if (response.authResponse) {
          console.log('Welcome!  Fetching your information.... ');
          FB.api('/me', 'GET', { fields: 'email,first_name,name,picture' }, function (response) {
            console.log('Good to see you, ' + response.name + '.');
            console.log("hai" + response.email);
            console.log(response.picture);
            console.log(response)
          });
        } else {
          console.log('User cancelled login or did not fully authorize.');
        }
      });

    }

    // $rootScope.usertype = "web";
    $scope.customershow = true;
    $scope.SelectedRadio = function (obj) {
      $rootScope.usertype = obj;
      if ($rootScope.usertype == "web") {
        $scope.customershow = true;
      } else if ($rootScope.usertype == "Dealer") {
        $scope.customershow = true;
      }
      else if ($rootScope.usertype == "salesManager" || $rootScope.usertype == "accountant") {
        $scope.customershow = false;
      } else if ($rootScope.usertype == "admin") {
        $scope.customershow = false;
      }
    }

    //Customer
    $scope.customerLogin = function (loginData) {
      // if ($scope.loginForm.$valid) {
      loginService.userAuthentication(loginData.username, loginData.password, $rootScope.ip).then(function (data) {
        if (data.data.status == 'Success') {
          $scope.wishlistShow = true;
          window.localStorage['username'] = data.data.username;
          // alert($rootScope.username);
          window.localStorage['token'] = data.data.token;
          window.localStorage['user_id'] = data.data.user_id;
          $scope.userName = data.data.username;
          window.localStorage['email'] = data.data.userinfo.email;
          window.localStorage['mobile'] = data.data.userinfo.user_mobile;
          window.localStorage['customermobile'] = data.data.userinfo.user_mobile;
          localStorage.setItem('userInfo', JSON.stringify(data.data.userinfo));
          window.localStorage['usertype'] = data.data.userinfo.user_type;
          if (data.data.GSTnumber != '') {
            localStorage.setItem('gstNumber', data.data.GSTnumber);
          } else {
            localStorage.setItem('gstNumber', '');
          }
          localStorage.setItem('shippingAddressInfo', JSON.stringify(data.data.shipping_address));
          localStorage.setItem('billingAddressInfo', JSON.stringify(data.data.billing_address));
          window.localStorage['user_name'] = $scope.userName;
          if ($scope.previousUrl == 'viewCart') {
            $scope.currentUrl = $scope.previousUrlArray[0].concat("#!/checkout");
            window.location.href = $scope.currentUrl;
          } else {
            window.location.href = "./";
          }
        } else {
          // alert(data.data.status)
        }
      })
      // }
    }
    //dealer
    $scope.dealerlogin = function (loginData) {
      $scope.delearLoginData = loginData;
      // if ($scope.loginForm.$valid) {
      dealerloginService.dealerloginAuthentication(loginData).then(function (data) {
        if (data.data.status == 'success') {
          // alert(JSON.stringify(data))
          $scope.wishlistShow = false;
          $scope.dealerregstatus = data.data.Dealer_status;
          window.localStorage['usertype'] = data.data.dealer_data.user_type;
          window.localStorage['token'] = data.data.token;
          window.localStorage['user_id'] = data.data.Dealer_userid;
          $rootScope.dealerdata = data.data.dealer_data;
          window.localStorage['username'] = data.data.dealer_data.dealer_name;
          $rootScope.name = window.localStorage['username'];
          window.localStorage['shopname'] = data.data.dealer_data.dealer_shopname;
          window.localStorage['password'] = data.data.dealer_data.dealer_password;
          window.localStorage['reg_shopname'] = data.data.dealer_data.reg_shopname;
          window.localStorage['dealer_cin'] = data.data.dealer_data.dealer_cin;
          window.localStorage['country'] = data.data.dealer_data.dealer_country;
          window.localStorage['state'] = data.data.dealer_data.dealer_state;
          window.localStorage['city'] = data.data.dealer_data.dealer_city;
          window.localStorage['town'] = data.data.dealer_data.dealer_town;
          window.localStorage['pincode'] = data.data.dealer_data.dealer_pin;
          window.localStorage['ISD'] = data.data.dealer_data.dealer_isd;
          window.localStorage['phone'] = data.data.dealer_data.dealer_phone;
          window.localStorage['email'] = data.data.dealer_data.dealer_mail;
          window.localStorage['gstin'] = data.data.dealer_data.dealer_gstin;
          window.localStorage['mobile'] = data.data.dealer_data.dealer_mobile;
          window.localStorage['address'] = data.data.dealer_data.dealer_address;
          window.localStorage['dealer_status'] = data.data.dealer_data.dealer_status;

          // $("#loginModel").modal('hide');
          // alert(data.data.status);
          if (data.data.Dealer_status == "Pending") {
            alert('your details not yet verified by Admin');
          }
          else if ($scope.dealerregstatus == "Accept") {
            window.location.href = "./distributorRegistration.html";
          }
          else {
            //  $window.location.reload();
            $location.path("Dashboard");
          }

        } else {
          alert(data.data.status);
        }
      })
      // }
    }

    //Sales Manager Login
    $scope.smlogin = function (loginData) {
      smLoginService.smloginMethod(loginData, $rootScope.usertype).then(function (data) {
        if (data.data.status == 'success') {
          $scope.wishlistShow = false;
          window.localStorage['usertype'] = data.data.usertype;
          window.localStorage['token'] = data.data.token;
          window.localStorage['rfq_num_PO'] = data.data.rfq_num;
          window.localStorage['po_num'] = data.data.po_num;
          $rootScope.po_num = window.localStorage['po_num'];
          window.localStorage['smuser_id'] = data.data.user_id;
          window.localStorage['username'] = data.data.username;
          //  alert(data.data.status);
          // $window.location.reload();
          $location.path("Dashboard");
        }
        else {
          alert('Invalid credentials');
        }
      })
    }
    //  $scope.smlogin("");

    //admin Login

    $scope.adminlogin = function (loginData) {
      adminLoginService.adminLoginMethod(loginData).then(function (data) {
        if (data.data.status == 'success') {
          $scope.wishlistShow = false;
          window.localStorage['usertype'] = data.data.user_type;
          window.localStorage['username'] = data.data.user_type;
          window.localStorage['token'] = data.data.token;

          //   $window.location.reload();
          $location.path("Dashboard");
        }
        else {
          alert('Invalid credentials');
        }

      })

    }

    $scope.dealeractive = function (obj) {
      $scope.dealercompleteData = obj;

      $scope.billing_addr = [{ "d_no": obj.shipping_dno, "street": obj.shipping_street, "city": obj.shipping_city, "state": obj.shipping_state, "zip_code": obj.shipping_zip }];

      $scope.shipping_addr = [{ "d_no": obj.billing_dno, "street": obj.billing_street, "city": obj.billing_city, "state": obj.billing_state, "zip_code": obj.billing_zip }];

      dealerloginService.dealerActiveMethod($scope.dealercompleteData, $rootScope.dealerregData, $scope.billing_addr, $scope.shipping_addr).then(function (data) {

        if (data.data.status == 'Success') {

          alert(data.data.dealer_status);
          $location.path("Dashboard");

        }
      })
    }

    $rootScope.registration = function (dealerregdata) {
      $rootScope.dealerregData = dealerregdata;
    }

    $rootScope.login = function (loginData) {
      if (loginData.usertype == "web") {
        $scope.customerLogin(loginData);
      } else if (loginData.usertype == "Dealer") {
        $scope.dealerlogin(loginData);
      }
      else if (loginData.usertype == "salesManager" || loginData.usertype == "accountant" || loginData.usertype == "whManager") {
        $scope.smlogin(loginData);
      } else if (loginData.usertype == "admin") {
        $scope.adminlogin(loginData);
      }
    }

    $scope.backToLogin = function () {
      $scope.modalInstance.dismiss('close');
      $("#loginModel").modal('show');
      //window.location.href = "./";
    }

    $scope.createAccount = function () {
      $("#loginModel").modal('hide');
      $location.path("registration");
    }

    $scope.register = function (registrationData) {

      if (!registrationData.gstnumber) {
        registrationData.gstnumber = "";
      }
      window.localStorage['userInfo'] = registrationData;
      localStorage.setItem('userInfo', JSON.stringify(registrationData));
      //   
      if ($scope.registerForm.$valid) {
        if (registrationData.newsletter == true) {
          registrationData.newsletter = "checked";
        } else {
          registrationData.newsletter = "unchecked";
        }
        $rootScope.registrationData = registrationData;
        window.localStorage['mobile'] = $rootScope.registrationData.user_mobile;
        window.localStorage['customermobile'] = $rootScope.registrationData.user_mobile;

        if ($rootScope.registrationData.password == $rootScope.registrationData.confirm_password) {
          registrationService.userRegistration($rootScope.registrationData, $rootScope.usertype).then(function (data) {
            if (data.data.status == 'Data saved successfully') {
              $("#otpmodal").modal('show');
            } else {
              alert(data.data.status)
            }
          })
        } else {
          alert('"Password and Confirm password does not matched');
        }


      }

    }

    $scope.gotoLogin = function () {
      window.location.href = "./";
    }

    $scope.goToHome = function () {
      window.location.href = "./";
    }

    $scope.goToDashboard = function () {

      if (window.localStorage['token']) {
        $location.path("dashboard");

      }
      else {

        window.location.href = "./login.html";
      }
    }


    $scope.completeRegistrtion = function (otp) {
      if (otp == undefined) {
        $scope.error = true;
      } else {
        $scope.error = false;
        if (typeof ($scope.otp) == 'string') {
          $scope.otp = otp
        } else {
          $scope.otp = JSON.stringify(otp)
        }

        registrationService.verifyOTP($scope.otp, window.localStorage['mobile'], $rootScope.ip).then(function (data) {
          //alert(JSON.stringify(data))

          if (data.data.status == 'Data saved successfully') {
            window.localStorage['token'] = data.data.token;
            window.localStorage['user_id'] = data.data.user_id;
            window.localStorage['email'] = $rootScope.registrationData.email;
            window.localStorage['user_name'] = $rootScope.registrationData.firstname;
            if ($rootScope.registrationData.gstnumber != '') {
              localStorage.setItem('gstNumber', $rootScope.registrationData.gstnumber);
            }
            $("#otpmodal").modal('hide');
            window.location.href = "./";
          } else if (data.data.status == 'Mobile no / email already exists..') {
            alert(data.data.status + 'Please Login');
          }
          else {
            alert('Enter valid OTP')
          }
        })
      }

    }

    $scope.resendOTP = function () {
      registrationService.resendOTP(window.localStorage['mobile']).then(function (data) {
        // alert(JSON.stringify(data))

        alert(data.data.return)

      })
    }

    $scope.inputType = 'password';
    $scope.inputType1 = 'password';


    // Hide & show password function
    $scope.toggleShowPassword = function () {
      //alert("asdf");
      if ($scope.inputType1 == 'password') {
        $scope.inputType1 = 'text';
        $scope.showPassword1 = true;
      }
      else {
        $scope.inputType1 = 'password';
        $scope.showPassword1 = false;
      }
    };

    $scope.confirmToggleShowPassword = function () {
      //alert("asdf");
      if ($scope.inputType == 'password') {
        $scope.inputType = 'text';
        $scope.showPassword = true;
      }
      else {
        $scope.inputType = 'password';
        $scope.showPassword = false;
      }
    };

    $scope.loginppt = function (loginData) {
      $scope.username = loginData.username;
      $scope.password = loginData.password;;

      loginService.loginpptAuthentication($scope.username, $scope.password).then(function (data) {
        if (data.data.status == "more than one usertypes") {
          $scope.usertypes = data.data.user_types;
          $scope.single_usertpe = true;
        } else if (data.data.status == "success") {
          $("#loginModel").modal('hide');
          $scope.single_usertpe = false;
          $scope.usertype = data.data.usertype;
          window.localStorage['branch_code'] = data.data.branch_code;
          window.localStorage['usertype'] = $scope.usertype;
          window.localStorage['token'] = data.data.token;
          window.localStorage['user_id'] = data.data.user_id;
          if ($scope.usertype == "web") {
            window.localStorage['username'] = data.data.userinfo.firstname;
            window.localStorage['mobile'] = data.data.userinfo.user_mobile;
            window.localStorage['email'] = data.data.userinfo.email;
            if (data.data.GSTnumber != '') {
              localStorage.setItem('gstNumber', data.data.GSTnumber);
            } else {
              localStorage.setItem('gstNumber', '');
            }
            localStorage.setItem('shippingAddressInfo', JSON.stringify(data.data.shipping_address));
            localStorage.setItem('billingAddressInfo', JSON.stringify(data.data.billing_address));
            window.location.href = "./";
            // $location.path('/');
            // $window.location.reload();
          } else {
            if ($scope.usertype == "Dealer") {
              if (data.data.Dealer_status == "Active") {
                window.localStorage['welcome_msg'] = data.data.welcome_msg;
                $scope.welcome_msg = window.localStorage['welcome_msg'];
                window.localStorage['company_details'] = angular.toJson(data.data.company_details);
                $location.path("Dashboard");
                //  $window.location.reload();
              } else if (data.data.Dealer_status == "Accept") {
                alert('Your Status in Pending, Admin need to Approval then only you can Login');
                $scope.goToDealerLogout();
              } else if (data.data.Dealer_status == "Pending") {
                alert('Your Status in Pending, Admin need to Approval then only you can Login');
                $scope.goToDealerLogout();
              } else if (data.data.Dealer_status == "Process") {
                alert('Your Status in Pending, Admin need to Approval then only you can Login');
                $scope.goToDealerLogout();
              }
            } else {
              window.localStorage['welcome_msg'] = data.data.welcome_msg;
              $scope.welcome_msg = window.localStorage['welcome_msg'];
              //  window.localStorage['company_details'] = angular.toJson(data.data.company_details);
              $location.path("Dashboard");
              //    $window.location.reload();
            }
            window.localStorage['username'] = data.data.username;
            window.location.reload();

          }
        } else if (data.data.status == "invalid password") {
          alert(data.data.status);
        } else {
          alert(data.data.status);
        }
      })
    }


    $scope.goToDealerLogout = function () {
      dealerloginService.dealerlogout(window.localStorage['token']).then(function (data) {
        if (data.data.status == 'success') {
          $window.localStorage.clear();
          $scope = $scope.$new(true);
          $rootScope = $rootScope.$new(true);
          //window.location.href = "./login.html";
          window.location.href = "./";
          // $location.path('/');
        } else {
          // alert(data.data.status);
        }
      })
    }


    //  Dealer_status








    /*forgot password*/

    $scope.forgotpswd = function () {
      $("#loginModel").modal('hide');
      $scope.modalInstance = $modal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        scope: $scope,
        templateUrl: 'views/forgot_pwd_modals.html',
        controller: function ($scope) {
          $scope.close = function () {
            $scope.modalInstance.dismiss('close');
          }
        }
      });

    }

    /*forgot password*/






  }]);



shopMyToolsApp.controller('userProfileController', ['$scope', '$rootScope', 'userProfileService', '$location', '$route', '$modal',
  function ($scope, $rootScope, userProfileService, $location, $route, $modal) {

    $rootScope.otpModalShow = false;
   $rootScope.add_address = false;
   $rootScope.edit_addr = false;
   $scope.shipping_addr = {};

    $scope.clickedUser = {};
    $scope.unique_no = "";
    $scope.setAsDefault = true;
    $scope.username = window.localStorage['username'];
    $scope.cust_profile = false;
    if (window.localStorage['usertype'] == "web") {
      $scope.cust_profile = true;
    } else if (window.localStorage['usertype'] == "Dealer") {
      $scope.cust_profile = false;
    }
    if (window.localStorage['token'] == undefined) {
      $location.path('/');
    }
    $scope.myprofile = function () {
      userProfileService.userprofileDetails(window.localStorage['user_id'], window.localStorage['usertype']).then(function (data) {
        if (data.data.status == "Success") {
          $scope.profile_info = data.data.profile_info;
          $scope.billing_info = data.data.profile_info.billing_address;
          $scope.shipping_info = data.data.profile_info.shipping_address;
          $scope.default_address = data.data.profile_info.default_address;
          // alert($scope.default_address);

          for (i = 0; i < $scope.billing_info.length && i < $scope.shipping_info.length; i++) {
            if ($scope.uniq_no == $scope.billing_info[i].addunique_nbr || $scope.uniq_no == $scope.shipping_info[i].addunique_nbr) {
              $scope.setAsDefault = false;
            }


          }

        }
      })
    }
    $scope.myprofile();

    $scope.AddAddress = function () {
      $rootScope.add_address = true;
      $scope.header_title = "Add User Address" 
      $scope.modalInstance = $modal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        scope: $scope,
        templateUrl: 'views/myprofile_address_modals.html',
        controller: function ($scope) {
          $scope.close = function () {
            $scope.modalInstance.dismiss('close');
          }
        }
      });
    }





    $scope.addAddressProfile = function (data) {
      if (window.localStorage['usertype'] == "Dealer") {
        $scope.shipping_address = [{
          "d_no": data.d_no,
          "street": data.street,
          "city": data.city,
          "state": data.state,
          "zipcode": data.zipcode,
          "addunique_nbr": $scope.unique_no,
          "country": data.country
        }]
        $scope.billing_address = "";

      }

      userProfileService.addAddress(window.localStorage['user_id'], $scope.shipping_address, $scope.billing_address, window.localStorage['usertype']).then(function (data) {
        if (data.data.status == "details added successfully") {
          alert("Address Added Successfully!");
          $scope.myprofile();
          $scope.modalInstance.close();
          $rootScope.add_address = false;
          $scope.edit_shipping_addr = {};
        }
      })
    }


    $scope.addcustomeraddr = function (shippingObj, billingObj) {
      if (window.localStorage['usertype'] == "web") {
        $scope.shipping_address = [{ "full_name": shippingObj.fullname, "mobile": shippingObj.mobile, "d_no": shippingObj.d_no, "street": shippingObj.street, "city": shippingObj.city, "state": shippingObj.state, "zipcode": shippingObj.zipcode, "addunique_nbr": $scope.unique_no, "country": shippingObj.country }];
        $scope.billing_address = [{ "full_name": billingObj.fullname, "mobile": billingObj.mobile, "d_no": billingObj.d_no, "street": billingObj.street, "city": billingObj.city, "state": billingObj.state, "zipcode": billingObj.zipcode, "addunique_nbr": $scope.unique_no, "country": billingObj.country }];
      }

      $scope.addAddressProfile();
    }

    $scope.editAddressProfile = function (data, type) {
      $scope.header_title = "Edit User Address" 
      $rootScope.add_address = false;
       $rootScope.edit_addr = true;
      // $("#editAddressList").modal('show');

      $scope.modalInstance = $modal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        scope: $scope,
        templateUrl: 'views/myprofile_address_modals.html',
        controller: function ($scope) {
          $scope.close = function () {
            $scope.modalInstance.dismiss('close');
          }
        }
      });

      $scope.addr_type = type;
      $scope.unique_no = data.addunique_nbr;
      $scope.edit_shipping_addr = {};
      $scope.edit_shipping_addr.fullname = data.full_name;
      $scope.edit_shipping_addr.mobile = data.mobile;
      $scope.edit_shipping_addr.d_no = data.dno;
      $scope.edit_shipping_addr.street = data.street;
      $scope.edit_shipping_addr.city = data.city;
      $scope.edit_shipping_addr.state = data.state;
      $scope.edit_shipping_addr.zipcode = data.zipcode;
      $scope.edit_shipping_addr.country = data.country;
    }

  
    $scope.editAddress = function (data) {
      if ($scope.addr_type == "shipping") {
        $scope.shipping_address = [{ "full_name": data.fullname, "mobile": data.mobile, "d_no": data.d_no, "street": data.street, "city": data.city, "state": data.state, "zipcode": data.zipcode, "addunique_nbr": $scope.unique_no, "country": data.country }];
        $scope.billing_address = [];
      } else {
        $scope.shipping_address = [];
        $scope.billing_address = [{ "full_name": data.fullname, "mobile": data.mobile, "d_no": data.d_no, "street": data.street, "city": data.city, "state": data.state, "zipcode": data.zipcode, "addunique_nbr": $scope.unique_no, "country": data.country }];
      
      
      }
      $scope.addAddressProfile(data);

    }

    $scope.deleteShippAddress = function (addrobj) {
      $scope.addunique_nbr = addrobj.addunique_nbr;
      userProfileService.deleteAddress(window.localStorage['user_id'], window.localStorage['usertype'], $scope.addunique_nbr).then(function (data) {
        if (data.data.status == "Deleted Successfully") {
          alert("Address Deleted Successfully!");
          $route.reload();
          $scope.myprofile();
        }
      })
    }

   $scope.shippingIsSameAsBilling = {};

    $scope.$watch('shippingIsSameAsBilling.addr', function (value) {
      if (value) {
        $scope.billing_addr = $scope.shipping_addr;
      } else {
        $scope.billing_addr = {};
      }
    });

    $scope.profile = {};
   

    $scope.editmobileNo = function (data) {
      $rootScope.otpModalShow = false;
      if (window.localStorage['usertype'] == "web") {
        $scope.profile.mobile_no = data.mobile;
      } else if (window.localStorage['usertype'] == "Dealer") {
        $scope.mobile_no = data.dealer_mobile;
      }
      // $("#editmobileNo").modal('show');

      $scope.modalInstance = $modal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        scope: $scope,
        templateUrl: 'views/my_profile_modals.html',
        controller: function ($scope) {
          $scope.close = function () {
            $scope.modalInstance.dismiss('close');
          }
        }
      });
    }


    $scope.editmobilenum = function (data) {
      $scope.mobile_no = data;
      if($scope.mobile_no != undefined && $scope.mobile_no != ""){
      userProfileService.editmobilenum(window.localStorage['user_id'], $scope.mobile_no, window.localStorage['usertype']).then(function (data) {
        if (data.data.status == "Mobile Number Saved successfully.") {
          window.localStorage['mobile_no'] = data.data.mobile;
          $rootScope.otpModalShow = true;

        }
     
      })
    }else{
      alert('please enter mobile number');
    }
    }

 
    $scope.enterotp = function (otp) {
      $scope.otp = otp;
      userProfileService.enterotp(window.localStorage['user_id'], window.localStorage['mobile_no'], $scope.otp, window.localStorage['usertype']).then(function (data) {
        if (data.data.status == "Mobile Number Updated successfully.") {
          window.localStorage['mobile_no'] = data.data.mobile;
          $scope.modalInstance.close();
          $rootScope.otpModalShow = false;  
          $scope.profile = {}; 
          $scope.myprofile();
        }
      
      })
    }




    $scope.setAsDefault = function (data, type) {
      $scope.unique_no = data.addunique_nbr;
      $scope.default_addr_type = type;
      userProfileService.setAsDefault(window.localStorage['user_id'], $scope.unique_no, window.localStorage['usertype']).then(function (data) {
        if (data.data.Status == "Address Set as Default") {
          alert(data.data.Status);
          window.localStorage['unique_no'] = data.data.addunique_nbr;
          $scope.uniq_no = window.localStorage['unique_no'];
          $scope.myprofile();

        }
      })

    }

    $scope.allItemcount = function () {

      userProfileService.getAllItemscount(window.localStorage['user_id'], window.localStorage['usertype']).then(function (data) {

        if (data.data.status == "success") {
          window.localStorage['cart_count'] = data.data.cart_count;
          window.localStorage['wish_count'] = data.data.wishlist_count;
        }
      })

    }
    $scope.allItemcount();

    $scope.goToDashboard = function () {
      $location.path("Dashboard");
    }



  }]);

  

shopMyToolsApp.controller('forgetPassword', ['$scope', '$rootScope', 'loginService', '$location', '$route',
  function ($scope, $rootScope, loginService, $location, $route) {
    $rootScope.otpShow = false;
    $rootScope.emailSent = false;
    $scope.forgotPwdemail = {};
    $scope.forgotPwdotp = {};
    $rootScope.errorShow = false;
    $scope.otperror = false;

    $scope.forgotPaswd = function (data) {
      $scope.username = data;
      window.localStorage['username'] = data;
      if (data !=undefined && data !="") {
        $rootScope.errorShow = false;
        loginService.forgortpassword($scope.username).then(function (data) {
          if (data.data.status == 'OTP send') {
            $rootScope.otpShow = true;
          } else if (data.data.status == 'Email Sent') {
            window.localStorage['time_stamp'] = data.data.time_stamp;
            $rootScope.emailSent = true;
          }
        })
      } else {
         $rootScope.errorShow = true;
      }
    }

    $scope.otpForgotPwd = function (otp) {
      $scope.otp = otp;
      if (otp !=undefined && otp !="") {
        loginService.forgotpswdotp($scope.username, $scope.otp).then(function (data) {
          if (data.data.status == "success") {
            window.localStorage['user_id'] = data.data.user_id;
            $scope.modalInstance.dismiss('close');
            $location.path("/resetPassword");
          }else{
            alert(data.data.status);
          }
        })
      } else {
        $scope.otperror = true;
      }
    }

    $scope.resetPassword = function (data) {

      $scope.usertype = window.localStorage['usertype'];
      $scope.newPswd = data.newPassword;
      $scope.confirmPswd = data.confirmPassword;

      $scope.userId = window.localStorage['user_id'];
      $scope.timeStamp = "";

      if ($scope.usertype == undefined && $scope.userId == undefined) {
        $rootScope.urlString = window.location.href;
        $rootScope.url = $rootScope.urlString;
        $scope.userArray = $rootScope.url.split("#");
        $scope.userId = $scope.userArray[2];
        $scope.timeStamp = "";
      } else if (($scope.usertype == undefined) && ($scope.userId = !undefined)) {
        $scope.userId = window.localStorage['user_id'];
      } else {
        alert('Please Loguot and then Reset Your Password');
      }

      if ($scope.resetForm.$valid) {
        if ($scope.newPswd == $scope.confirmPswd) {
          loginService.resetPswdMethod($scope.userId, $scope.newPswd, $scope.confirmPswd, $scope.timeStamp).then(function (data) {
            if (data.data.status == 'password changed successfully') {
              alert(data.data.status);
              window.location.href = "./";
            } else {
              alert(data.data.status)
            }
            // alert(data.data.status);

          })
        } else {
          alert('New Password and Confirm Password should be same');
        }
      }

    }

    $scope.resendForgotOTP = function(){
      loginService.forgortpassword(window.localStorage['username']).then(function (data) {
        if (data.data.status == 'OTP send') {
          alert('OTP Sent Successfully');
          $scope.forgotPwdotp = {};
          $rootScope.otpShow = true;
          $scope.otperror = false;
        } 
      })
    }


  }]);

