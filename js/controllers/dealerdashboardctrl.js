

shopMyToolsApp.controller('dashboardViewController', ['$scope', '$location', '$rootScope', '$modal',
    function ($scope, $location, $rootScope, $modal) {

        $scope.dealer = false;
        $scope.sales = false;
        $scope.accountant = false;
        $scope.whmanger = false;
        $scope.admin = false;
        $scope.purchase = false;
        if ($rootScope.token == undefined) {
            $location.path('/');
        }

        $scope.welcome_msg = window.localStorage['welcome_msg'];
        $scope.branch_code = window.localStorage['branch_code'];

        $scope.user_type = window.localStorage['usertype'];

        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];
            // $rootScope.customerdashboardShow = false;
        }
        else if (window.localStorage['usertype'] == "Admin") {
            $scope.admin = true;
            // $rootScope.customerdashboardShow = true;
        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.sales = true;
            $rootScope.porfq_num = window.localStorage['rfq_number_PO']
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            $scope.userid = window.localStorage['user_id'];
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountant = true;
        } else if (window.localStorage['usertype'] == "Purchase-Manager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.purchase = true;
        }

        $scope.pendingrfq = function () {
            $location.path("pendingrfq");
        }
        $scope.pendingpo = function () {
            $location.path("pendingpo");
        }
        $scope.proformaInvoice = function () {
            $location.path("proformaInvoice");
        }
        $scope.adminNewPanel = function () {
            $location.path("admin_shop_details");
        }
        $scope.masterdataUpload = function () {
            $location.path("master_data_upload");
        }
        $scope.dealerverification = function () {
            $location.path("dealerverification");
        }
        $scope.internalUsers = function () {
            $location.path("internalusers");
        }
        $scope.wharehouseInvoicePreparation = function () {
            $location.path("packingListPreparation");
        }
        $scope.finalInvoice = function () {
            $location.path("finalInvoiceGeneration");
        }

        $scope.challanGeneration = function () {
            $location.path("challanGeneration");
        }
        $scope.challanCompleted = function () {
            $location.path("challanCompleted");
        }
        $scope.packingList = function () {
            $location.path("packingList");
        }
        $scope.dealerHistory = function () {
            $location.path("dealersHistory");
        }
        $scope.companyprofile = function () {
            $location.path("company_profile");
        }
        $scope.inwardGeneration = function () {
            $location.path("inwardGeneration");
        }
        $scope.grnPrepartion = function () {
            $location.path("grnPreparation");
        };
        $scope.dealerSegregation = function () {
            if ($scope.branch_code == 0000) {
                $location.path("dealerSegregation");
            } else {
                $location.path("dealerShopInfo");
            }
        };
        $scope.barcode_page = function () {
            $location.path("barcode_page");
        };
        $scope.getDealerOrders = function () {
            $location.path("dealerOrderedList");
        };
        $scope.getPoRecords = function () {
            $location.path("po_history");
        };
        $scope.getInvoiceRecords = function () {
            $location.path("invoice_history");
        };
        $scope.activeDealersInfo = function () {
            $location.path("activeDealersInfo");
        }

    }]);


shopMyToolsApp.controller('dealerdashboardController', ['$scope', '$location',

    '$rootScope', 'dealerdashboardService', '$modal',

    function ($scope, $location, $rootScope, dealerdashboardService, $modal) {

        $scope.dealershow = false;
        $scope.salesdetails = false;
        $scope.accountshow = false;
        $scope.whmanger = false;
        $scope.purchase = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];

        if (window.localStorage['usertype'] == "Dealer") {
            $scope.createrfqshow = true;
            $scope.dealershow = true;
            $scope.userid = window.localStorage['user_id'];
        }
        else if (window.localStorage['usertype'] == "Admin") {
            $scope.adminshow = true;
        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.salesdetails = true;
            $rootScope.porfq_num = window.localStorage['rfq_number_PO']
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            $scope.userid = window.localStorage['user_id'];
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountshow = true;
        } else if (window.localStorage['usertype'] == "Purchase-Manager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.purchase = true;
        }



        $scope.moreRfqDataList = [];

        $scope.getpendingRfqData = function () {
            $scope.loading = true;
            dealerdashboardService.pendingrfqMethod(window.localStorage['usertype'], $scope.userid).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'Success') {
                    $scope.rfq_moreinfo = data.data.more_info;
                    $scope.data = $scope.rfq_moreinfo;
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                    $scope.rfq_moreinfo.forEach(function (items) {
                        items.rfq_list.forEach(function (item) {
                            // if (item.split_status != 'Reject') {
                            $scope.moreRfqDataList.push(item);
                            // }
                        })
                    })
                } else {
                    $scope.rfq_moreinfo = [];
                }

            })
        }
        $scope.getpendingRfqData();



        $scope.listItems = [];
        $scope.poReqDataList = [];



        $scope.getMoreDetails = function (item) {
            $scope.productRfqList = [];
            $scope.rfqNumber = item.rfq_num;
            $scope.reqPoBtn = item.action;
            $scope.moreRfqData = true;

            $scope.dataList = item.rfq_list;
            $scope.data1 = item.rfq_list;
            $scope.totalItems1 = $scope.data1.length;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;

            item.rfq_list.forEach(function (data) {
                if (Object.keys(data).length > 0) {
                    $scope.productRfqList.push(data);
                }
            })




            $scope.poReqDataList = [];
            for (i = 0; i < $scope.productRfqList.length; i++) {
                // if (Object.keys($scope.productRfqList[i]).length > 0) {
                if ($scope.productRfqList[i].product_status == 'Accept') {
                    $scope.poReqDataList.push({ "product_name": $scope.productRfqList[i].productname, "dealer_status": 'Confirm', "unique_nbr": "" });
                } else if ($scope.productRfqList[i].product_status == 'Split') {
                    $scope.productRfqList[i].split_info.forEach(function (data) {
                        $scope.poReqDataList.push({ "product_name": data.productname, "dealer_status": 'Confirm', "unique_nbr": data.unique_nbr });
                    })
                }
                // }    

            }
        }

        $scope.getSplitData = function (item) {
            $scope.rfqNumber = item.rfq_num;
            $scope.productId = item.productid;
            $scope.productname = item.productname;
            $scope.split_info = item.split_info;
            //   $("#addedToCart").modal('show');

            var modalInstance = $modal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                scope: $scope,
                templateUrl: 'views/splitScheduleModal.html',
                controller: function ($scope) {
                    $scope.close = function () {
                        modalInstance.dismiss('close');
                    }
                }
            });

        }


        $scope.getStatus = function (val, item) {
            $scope.status = val;
            for (i = 0; i < $scope.poReqDataList.length; i++) {
                if (item.split_status != 'Split') {
                    if ($scope.poReqDataList[i].product_name == item.productname) {
                        $scope.poReqDataList[i].dealer_status = $scope.status;
                    }
                }
            }
        }

        $scope.getSplitStatus = function (val, item) {
            alert(val);
            $scope.status = val;
            $scope.uniqueNbr = item.unique_nbr;
            for (i = 0; i < $scope.poReqDataList.length; i++) {
                if ($scope.poReqDataList[i].unique_nbr == $scope.uniqueNbr) {
                    $scope.poReqDataList[i].dealer_status = $scope.status;
                }
            }
        }



        $scope.requestforPo = function () {
            $scope.loading = true;
            dealerdashboardService.dealersplitconfirm($scope.poReqDataList, $scope.rfqNumber, window.localStorage['usertype']).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'Success') {
                    $scope.po_num = data.data.Po_number;
                    window.localStorage['Po_number'] = data.data.Po_number;
                    window.localStorage['rfq_number_PO'] = data.data.rfq_number;
                    // $("#requestPOModal").modal('show');

                    $scope.displayNumber = $scope.po_num;
                    $scope.displayMsg = "Your Purchase Order Generated Successfully!";
                    var modalInstance = $modal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        scope: $scope,
                        templateUrl: 'views/numberModalPopup.html',
                        controller: function ($scope) {
                            $scope.close = function () {
                                modalInstance.dismiss('close');
                            }
                        }
                    });

                    $scope.moreRfqDataList = [];
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                    $scope.moreRfqData = false;
                    $scope.getpendingRfqData();
                }
            })
        }

        $scope.smsplit = function (obj) {
            window.localStorage['rfqnumber'] = obj;
            $location.path("rfqValidationpage");
        }


        $scope.Podetails = function (item, index) {
            $rootScope.rfq_num = item.rfq_num;
            $scope.po_num = item.po_num;
            $scope.index = index;
            $scope.podetailshow = !$scope.podetailshow;
        }

        $rootScope.B2BDashboard = function () {
            $location.path('Dashboard');
        };

        $scope.gotoShopping = function () {
            $location.path('createRFQ');
        }

        $scope.deleterfqno = function (data, index) {
            $scope.index = index;
            $scope.rfqno = data.rfq_num;
            if (data.productid == undefined) {
                $scope.prod_id = "all";
            } else {
                $scope.prod_id = data.productid;
            }

            dealerdashboardService.dealerrfqdeleteMethod($scope.rfqno, $scope.prod_id).then(function (data) {
                if (data.data.status == 'success') {
                    alert('rfq deleted successfully');
                    $scope.getpendingRfqData();
                    $scope.dataList.splice($scope.index, 1);
                }
            }

            )
        }

    }]);