﻿//var app = angular.module("smtApp", []);

shopMyToolsApp.controller('homeController', ['$scope', '$http', '$location',

    '$rootScope',

    '$route', '$timeout',

    'logoutService', '$window', '$window', 'newHomePageService', '$filter', '$timeout', 'dealerloginService', 'smLoginService', 'adminLoginService', 'userProfileService',

    function ($scope, $http, $location, $rootScope, $route,

        $timeout,

        logoutService, $window, $window, newHomePageService, $filter, $timeout, dealerloginService, smLoginService, adminLoginService, userProfileService) {

        $scope.product = {};
        $scope.welcome_msg = window.localStorage['welcome_msg'];

        $window.scrollTo(0, 0);


            

        $scope.username = window.localStorage['username'];
        $scope.usertype = window.localStorage['usertype'];
        $scope.wishlist = true;
        $scope.dashboard = false;
        $scope.single_usertpe = true;

        if (window.localStorage['usertype'] == "web") {
            $scope.dashboard = false;
        } else if (window.localStorage['usertype'] == undefined) {
            $scope.dashboard = false;
        } else {
            $scope.dashboard = true;
        }

        $rootScope.token = window.localStorage['token'];

        // if (window.localStorage['token'] == undefined) {
        //     $location.path('/');
        // }

        $scope.goToWishList = function () {

            if (window.localStorage['token']) {
                localStorage.setItem('wishlistBreadCrumbFlag', 'false');
                $location.path("wishlist");

            }
        }

        $scope.myprofile = function () {
            $location.path("myProfile");
        }
        $scope.myDashboard = function () {
            $location.path("Dashboard");
        }
        $scope.offers_products_page = function () {
            $location.path("offers_products_page");
        }
        $scope.newArraivals_page = function () {
            $location.path("newArraivals_page");
        }


        $scope.getNewHeaderData = function (obj) {
            
            $scope.link = obj;
            if ($scope.link == '') {
                $scope.today = '';
            }
            else {
                $scope.today = $filter('date')(new Date(), 'dd-MM-yyyy');
            }
            $scope.set_menu_color = function (obj) {
                if ($scope.link == 'Powertex') {
                    return { background: "#259b78", border: "1px solid #259b78" }
                }
            }
            $scope.set_header_color = function (obj) {
                if ($scope.link == 'Powertex') {
                    return { background: "#259b78", color: " #fff", border: "1px solid #259b78" }
                }
            }
            $scope.set_top_box_clr = function (obj) {
                if ($scope.link == 'Powertex') {
                    return { background: "#666", border: "1px solid #666" }
                }
            }
            $scope.set_top_icons_clr = function (obj) {
                if ($scope.link == 'Powertex') {
                    return { color: "#fff" }
                }
            }


            $rootScope.collectionData = '';
            $rootScope.topBrandsArray = '';
            $rootScope.emergingBrandsArray = '';
            $rootScope.bannerArray = '';
            $rootScope.shopmytools = '';
            $rootScope.customerservice = '';
            $scope.loading = true;
            newHomePageService.newHomePageMethod(obj, $scope.today).then(function (data) {
                $scope.loading = false;

                //     console.log(JSON.stringify(data))
                if (data.data.status == 'Success') {
                    $scope.headerLogoData = data.data.HeaderLogo;
                    $scope.headerLogoData.forEach(function (element) {
                        $scope.headerLogoData = element;
                    }, this);
                    $rootScope.collectionData = data.data.Details1;
                    $rootScope.topBrandsArray = data.data.Details2;
                    $rootScope.emergingBrandsArray = data.data.Details3;
                    $rootScope.bannerArray = data.data.BannerData;
                    $rootScope.shopmytools = data.data.Footer;
                    $rootScope.customerservice = data.data.Footer2;
                    $scope.phonenumber = data.data.SitePhnNbr;
                    $scope.phonenumber.forEach(function (element) {
                        $scope.phonenumber = element;
                    }, this);
                }
            })
        }

        $scope.getNewHeaderData("Powertex")
        $scope.getoffersData = function (categoryobj) {
            $scope.userid = window.localStorage['user_id'];
            if (window.localStorage['user_id'] == undefined || window.localStorage['usertype'] == "salesManager" || window.localStorage['usertype'] == "accountant" || window.localStorage['usertype'] == "whManager" || window.localStorage['usertype'] == "Admin" || window.localStorage['usertype'] == "Dealer" || window.localStorage['usertype'] == "Purchase-Manager") {
                $scope.userid = "";
            }

            newHomePageService.offersMethod(categoryobj, $scope.userid).then(function (data) {
                //  console.log(JSON.stringify(data))
                if (data.data.status == 'Success') {
                    $rootScope.offersdata = data.data.offer_products;
                }
            })
        }
        $scope.getoffersData("")


        $scope.getnewArrivalData = function (categoryObj) {
            $scope.userid = window.localStorage['user_id'];
            if (window.localStorage['user_id'] == undefined || window.localStorage['usertype'] == "salesManager" || window.localStorage['usertype'] == "accountant" || window.localStorage['usertype'] == "whManager" || window.localStorage['usertype'] == "Admin" || window.localStorage['usertype'] == "Dealer" || window.localStorage['usertype'] == "Purchase-Manager") {
                $scope.userid = "";
            }
            newHomePageService.newArrivalMethod(categoryObj, $scope.userid).then(function (data) {
                //  console.log(JSON.stringify(data))
                if (data.data.status == 'Success') {
                    $rootScope.newarrivalsData = data.data.Newarrivals_products;
                    // alert($rootScope.newarrivalsData);
                }

            })
        }
        $scope.getnewArrivalData("")


        $scope.getnewCategories = function () {

            $scope.loading = true;
            newHomePageService.getNewCategoriesMethod().then(function (data) {
                $scope.loading = false;
                // alert(JSON.stringify(data));
                if (data.data.status == 'success') {

                    $scope.categoryData = data.data.categories;



                } else {

                    //  alert(data.data.status)

                }

            })

        }
        $scope.getnewCategories();



        $scope.categoryBasedProducts = function (categoryName) {
            window.localStorage['category'] = categoryName;
            window.localStorage['subcategory'] = [];
            $scope.categoryURL = document.URL.split("#!/");

            if ($scope.categoryURL[1] == 'categorylist') {
                $window.location.reload();
                $location.path("categorylist");
            } else {

                $location.path("categorylist");
            }
        }

        $scope.subCategoryMethod = function (subCategory, categoryName) {
           window.localStorage['subcategory'] = subCategory;
            window.localStorage['category'] = categoryName;
            $scope.categoryURL = document.URL.split("#!/");
            if ($scope.categoryURL[1] == 'categorylist') {
                $window.location.reload();
                $location.path("categorylist");
            } else {

                $location.path("categorylist");
            }


        }




        $scope.slickConfigBrands = {
            enabled: true,
            autoplay: true,
            draggable: false,
            autoplaySpeed: 5000,
            responsive: [
                // {
                //     breakpoint: 1024,
                //     settings: "unslick"
                // },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 468,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 320,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }

            ]
        };


        $scope.getnewProductDetails = function (productObj) {
            window.localStorage['productName'] = productObj.productname;
            window.localStorage['category'] = productObj.category;
            window.localStorage['subcategory'] = productObj.sub_category;
            $location.path("productDetailPage");
        }


        $scope.goToCustomerLogout = function () {
            logoutService.userLogout(window.localStorage['token']).then(function (data) {
                if (data.data.status == 'success') {
                    $window.localStorage.clear();
                    $scope = $scope.$new(true);
                    $rootScope = $rootScope.$new(true);
                    //  window.location.href = "./login.html";
                    window.location.href = "./";
                } else {
                }
            })
        }
        //dealer logout

        $scope.goToDealerLogout = function () {
            dealerloginService.dealerlogout(window.localStorage['token']).then(function (data) {
                if (data.data.status == 'success') {
                    $window.localStorage.clear();
                    $scope = $scope.$new(true);
                    $rootScope = $rootScope.$new(true);
                    //window.location.href = "./login.html";
                    window.location.href = "./";
                } else {
                    // alert(data.data.status);
                }
            })
        }

        //Sales Manager Logout
        $scope.goToSMLogout = function () {
            dealerloginService.smlogoutMethod(window.localStorage['token']).then(function (data) {
                if (data.data.status == 'success') {
                    $window.localStorage.clear();
                    $scope = $scope.$new(true);
                    $rootScope = $rootScope.$new(true);
                    // window.location.href = "./login.html";
                    window.location.href = "./";
                } else {
                    // alert(data.data.status);
                }

            })

        }
        //admin Logout
        $scope.goToadminLogout = function () {

            adminLoginService.adminLogoutMethod(window.localStorage['token']).then(function (data) {

                if (data.data.status == 'success') {

                    $window.localStorage.clear();

                    $scope = $scope.$new(true);

                    $rootScope = $rootScope.$new(true);

                    // window.location.href = "./login.html";
                    window.location.href = "./";

                    $("#specificationsTable").modal('show');

                } else {

                    // alert(data.data.status);

                }

            })

        }



        $scope.goToLogout = function () {
            if (window.localStorage['usertype'] == "Dealer") {
                $scope.goToDealerLogout();

            }
            else if (window.localStorage['usertype'] == "web") {
                $scope.goToCustomerLogout();
            } else if (window.localStorage['usertype'] == "salesManager" || window.localStorage['usertype'] == "accountant" || window.localStorage['usertype'] == "whManager" || window.localStorage['usertype'] == "Purchase-Manager") {
                $scope.goToSMLogout();

            } else {
                $scope.goToadminLogout();
            }

        }

        if (window.localStorage['usertype'] == "Dealer" || window.localStorage['usertype'] == "salesManager" || window.localStorage['usertype'] == "Admin" || window.localStorage['usertype'] == "whManager" || window.localStorage['usertype'] == "Purchase-Manager") {
            $rootScope.dealerdashboardShow = true;
            $rootScope.customerdashboardShow = false;
        }
        else {
            $rootScope.dealerdashboardShow = false;
            $rootScope.customerdashboardShow = true;
        }





        $scope.goToLogin = function () {

            $location.path("login");
        }

        $scope.goToAddressBook = function () {

            $location.path("addressBook");

        }

        $rootScope.showHintFlag = 'false';

        $rootScope.showHint = function (searchKey) {
            if (searchKey.length >= '3') {

                newHomePageService.searchProductsMethod(searchKey).then(function (data) {

                    //alert(JSON.stringify(data))

                    if (data.data.status == 'success') {

                        $rootScope.searchedProducts = data.data.product_info;
                        //$scope.cat=upload_category.$rootScope.searchedProducts;
                        //alert($scope.cat)

                        $rootScope.recommendedList = data.data.recommended;

                        $rootScope.showHintFlag = 'true';



                    } else if (data.data.status == 'fail') {

                        //  alert(data.data.data)

                        $rootScope.searchedProducts = [];
                        $rootScope.showHintFlag = 'true';

                    }

                })

            } else if (searchKey.length == '0') {

                $rootScope.showHintFlag = 'false';
                $location.path("/")

            }

        }

        $rootScope.test = function (searchKey) {

            if (searchKey.length > 0) {
                localStorage.setItem('searchkey', searchKey);

                $rootScope.showHintFlag = 'false';

                $location.path("searchPage");

                $scope.searchPageURL = document.URL.split("#!/");

                if ($scope.searchPageURL[1] == 'searchPage') {

                    $location.path("searchPage1");

                }
                else {

                    $location.path("searchPage");

                }
            } else if (searchKey.length == '0') {
                $rootScope.showHintFlag = 'false';
                $location.path("/")
            }



        }




        $scope.goToCollections = function () {

            $location.path("collections");

        }



        $scope.goToTopBrands = function () {

            $location.path("topBrands");

        }



        $scope.gotoDashBoards = function () {

            $location.path("Dashboard");

        }



        $scope.goToEmergingBrands = function () {

            $location.path("emergingBrands");

        }



        $scope.goToHome = function () {

            window.location.href = "./";

        }

        $scope.wish = window.localStorage['checkValue'];

        $scope.addToWishList = function (productObj) {
            newHomePageService.addNewWishListMethod(window.localStorage['user_id'], productObj.productname).then(function (data) {
                if (data.data.status == 'product saved successfully') {
                    $rootScope.wishlistcount = data.data.items_count;
                    $("#addedToWishList").modal('show');
                } else {
                    alert(data.data.status);
                }
            })
        }

        $scope.removeWishListItem = function (wishListObj) {
            if ($window.confirm("Are you sure you want to delete this product from Wishlist?")) {
                newHomePageService.removeWishListItemMethod(window.localStorage['user_id'], wishListObj.productname).then(function (data) {
                    //    alert(JSON.stringify(data))
                    if (data.data.status == 'product removed successfully') {
                        $scope.wishlist = false;

                        // window.localStorage['wishlistcount'] = data.data.wishlist_count;
                        // $scope.getWishlistItems();
                        //  $window.location.reload();
                    } else {
                        alert(data.data.status)
                    }
                })
            }
        }



        $scope.addToWishListData1 = function (obj, checkval) {
            if (window.localStorage['token']) {
                if (checkval == 'false') {
                    $scope.removeWishListItem(obj);
                } else {
                    $scope.addToWishList(obj);

                }
            } else {
                alert('Please Login to Add To WishList')
            }
            $scope.getoffersData("");
            $scope.getnewArrivalData("");
        }



        $scope.gotoCustCartPage = function () {
            $location.path("viewCart")
        }
        $scope.gotodealerCartPage = function () {
            $location.path("dealerViewCart")
        }



        //distributor
        $scope.viewCart = function () {
            if (window.localStorage['usertype'] == "Dealer" || window.localStorage['usertype'] == "Purchase-Manager") {
                $scope.gotodealerCartPage();
            }
            else {
                $scope.gotoCustCartPage();
            }
        }


        $scope.getCategoryBasedOnBrands = function (brandObj) {

            window.localStorage['categoryName'] = "";

            localStorage.setItem('brandName', brandObj.brandname)

            // window.localStorage['brandName'] = brandObj.brandname;

            $location.path("brandPage");

        }


        $scope.gotoMyOrders = function () {
            $location.path("myorders")
        }


        $scope.allItemcount = function () {
            userProfileService.getAllItemscount(window.localStorage['user_id'], window.localStorage['usertype']).then(function (data) {
                if (data.data.status == "success") {
                    $rootScope.totalItems = data.data.cart_count;
                    $rootScope.wishlistItems = data.data.wishlist_count;
                }
            })
        }
        if (window.localStorage['usertype'] == "Dealer" || window.localStorage['usertype'] == "web") {
            $scope.allItemcount();
        }

        $scope.goToDashboard = function () {
            $location.path("Dashboard");
        }

    }]);





