shopMyToolsApp.controller('checkoutCntrl', function ($scope, $rootScope, checkoutService, $location) {
    $scope.dealer = {};
    $scope.cartObj = $rootScope.cartorderObj
    $scope.cartgrandtotal = $rootScope.grandTotal;
    $scope.delivery_addrShow = false;
    $scope.success = false;
    $scope.getbtn = true;
    $scope.inShow = false;
    $scope.payuShow = false;
    $scope.sendEnquiry = true;
    $scope.enquiryinst = false;
    $scope.nxtbtn = false;
    if (window.localStorage['token'] == undefined) {
        $location.path('/');
    }

    $rootScope.B2BDashboard = function () {
        window.location.href = "./";
    };

    $scope.gotoShopping = function () {
        window.location.href = "./";
    }

    //  $scope.cartArrayItems = $rootScope.cartArrayItems;
    $scope.getshoplist = function (pincode, altMobile, gstnumber) {
        $scope.altMobile = altMobile;

        if ($scope.gstnumber == undefined) {
            $scope.gstnumber = localStorage.getItem('gstNumber')
        } else {
            $scope.gstnumber = gstnumber;
        }
        // $scope.pincode ="500083";
        // $scope.lat_long =[17.38504,78.486671];

        checkoutService.pincodecheckMethod(pincode).then(function (data) {
            //    console.log(JSON.stringify(data))

            if (data.data.status == 'Success') {

                $scope.delivery_addrShow = false;
                $scope.disableShippingbtn = false;
                $scope.nxtbtn = false;

                var geocoder = new google.maps.Geocoder();

                geocoder.geocode({ 'address': JSON.stringify(pincode) }, function (results, status) {

                    if (status == google.maps.GeocoderStatus.OK) {

                        var latitude = results[0].geometry.location.lat();

                        var longitude = results[0].geometry.location.lng();


                        // alert("Latitude: " + latitude + "\nLongitude: " + longitude);

                        $scope.latLongArray = [];

                        $scope.latLongArray.push(latitude, longitude);

                        $scope.getDealersList($scope.latLongArray, pincode)
                        $scope.getbtn = false;
                        $scope.enquiryinst = false;

                    } else {

                        // alert("Request failed.")

                    }

                });

            }
            else {
                $scope.loading = false;
                $scope.delivery_addrShow = true;
                $scope.inShow = false;
                $scope.getbtn = false;
                $scope.enquiryinst = true;
                $scope.disableShippingbtn = false;
                $scope.nxtbtn = true;

                $scope.shippingType = "delivery"
                $scope.dealersList = '';
            }

        })
    }
    // $scope.getshoplist("")

    $scope.getDealersList = function (latLongArray, pincode) {
        $scope.loading = true;
        checkoutService.getDealersListMethod(latLongArray, pincode).then(function (data) {

            //    alert(JSON.stringify(data))
            $scope.loading = false;

            if (data.data.status == 'success') {

                $scope.dealersList = data.data.dealer_list;

                if ($scope.dealersList.length > 0) {
                    $scope.shippingType = "Pickup";
                    $scope.inShow = true;
                }


            }


        })

    }

    $scope.status = "Accepted";
    $scope.disableShippingbtn = false;
    $scope.customermobile = window.localStorage['mobile'];

    $scope.saveDealerAddress = function (dealer) {
        //  alert(JSON.stringify(dealer))
        $scope.disableShippingbtn = true;
        $scope.nxtbtn = true;

        $scope.dealerAddress = dealer.shop_name;
    }

    $scope.checkPaymentType = function (payment) {

        $scope.paymenttype = payment;
        if ($scope.paymenttype == "payu") {
            $scope.proceed();
            $scope.payuShow = true;
        } else {
            $scope.payuShow = false;
        }


        // $scope.disablePaymentbtn = false;

    }
    $scope.getPayuDetails = function () {

        checkoutService.getpayuDetailsMethod().then(function (data) {

            if (data.data.status == 'payu data') {

                $scope.payuData = data.data.data;

                $scope.merchant_key = $scope.payuData.merchant_id;

                $scope.salt_key = $scope.payuData.salt_key;

            }

        })

    }

    if (window.localStorage['token']) {
        //  $scope.viewCartItems();

        $scope.getPayuDetails();
    } else {
        $location.path("/")
    }

    $scope.firstname = window.localStorage['user_id'];
    $scope.email = window.localStorage['email'];


    $scope.payuMoneyFunc = function ($location, $sce) {

        $scope.txnId = window.localStorage['finalOrderId'];

        $scope.string = $scope.merchant_key + '|' + $scope.txnId + '|' + $rootScope.grandTotal + '|' + $scope.productinfo + '|' + $scope.firstname + '|' + $scope.email + '|||||||||||' + $scope.salt_key;

        $scope.encrypttext = sha512($scope.string);

        $scope.hash = $scope.encrypttext;

        // console.log($scope.hash)

    }

    $scope.orderItems = function (data) {
        $scope.cartArrayItems = data;
        $scope.cartArraylength = data.length;
    }

    $rootScope.orderItemArray = [];




    $scope.cartArrayItems.forEach(function (cartItem) {

        $rootScope.orderItemArray.push({

            "sno": "", "productdescription": cartItem.upload_name, "qty": cartItem.qty,

            "unitprice": cartItem.mrp, "enduser_price": "", "total": JSON.stringify(((cartItem.mrp * cartItem.tax) / 100 * cartItem.qty) + (cartItem.mrp * cartItem.qty)),

            "tax": cartItem.tax, "tax_amount": JSON.stringify(cartItem.tax_amount), "sub_total": JSON.stringify(cartItem.mrp * cartItem.qty)

        })

    });



    $scope.proceed = function () {

        // if ($scope.paymenttype == 'payu') {

        //     $location.path("payu");

        // }


        $scope.orderArray = {
            "status": $scope.status,
            "shop": $scope.dealerAddress,
            "alt_mobile": $scope.altMobile,
            "pic_alt_mobile": $scope.altMobile,
            "customermobile": $scope.customermobile,
            "totalamount": JSON.stringify($scope.cartgrandtotal),
            "orderitems": $rootScope.orderItemArray,
            // "cupon_id": $scope.couponId,
            // "discount": $scope.couponAmt,
            "user_type": "web",
            "billingaddress": $scope.billing_addr,
            "shippingaddress": $scope.shipping_addr,
            "shippingtype": $scope.shippingType,
            "totalquantity": JSON.stringify($rootScope.totalItems),
            "paymenttype": $scope.paymenttype,
            "total_items": JSON.stringify($rootScope.totalItems),
            "user_id": window.localStorage['user_id'],
            "gst_number": $scope.gstnumber
        }

        if ($scope.shippingType == "delivery") {
            $scope.shipping_addr = $scope.shipping_addr;
            $scope.billing_addr = $scope.billing_addr;
        } else if ($scope.shippingType == "Pickup") {
            $scope.shipping_addr = [];
            $scope.billing_addr = [];
        }









        checkoutService.saveOrderMethod($scope.orderArray).then(function (data) {
            if (data.data.status == "data saved") {
                $scope.finalOrderId = data.data.orderid;
                window.localStorage['finalOrderId'] = $scope.finalOrderId;
                if ($scope.paymenttype == 'cashondelivery') {
                    // $location.path("success");
                    $scope.success = true;

                }
            }


            //alert(JSON.stringify(data));

        })
    }
    $scope.shippingAddress = {};

    $scope.shippingAddress = function (data, checkvalue) {
        $scope.shipping = data;

        if (checkvalue == true) {
            $scope.billingAddress = $scope.shipping;
        } else if (checkvalue == undefined || checkvalue == false) {
            $scope.billingAddress = {};
        }

    }
    $scope.shipping_addr = [];
    $scope.billing_addr = [];
    $scope.saveaddress = function (shippingadr, billing) {
        $scope.shipping_addr.push({ "full_name": shippingadr.fullname, "mobile": shippingadr.mobile, "d_no": shippingadr.dno, "street": shippingadr.street, "city": shippingadr.city, "state": shippingadr.state, "zipcode": shippingadr.zipcode });
        $scope.billing_addr.push({ "full_name": billing.fullname, "mobile": billing.mobile, "d_no": billing.dno, "street": billing.street, "city": billing.city, "state": billing.state, "zipcode": billing.zipcode });
    }

    $scope.changePindetails = function (data) {
        $scope.getbtn = true;
    }


    $scope.sendEnquiry = function () {

        checkoutService.sendEnquiry($scope.dealerAddress).then(function (data) {
            alert(JSON.stringify(data));
            if (data.data.status == "Success") {
                $scope.sendEnquiry = false;
                alert("your Enquiry sent to dealer. dealer will contact you soon");
                $location.path('/');
            }
        })
    }
});

