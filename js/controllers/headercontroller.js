shopMyToolsApp.controller('headerController', ['$scope', '$http', '$location',
    '$rootScope',
    '$route', '$timeout', '$window',
    function ($scope, $http, $location, $rootScope,
        $route, $timeout, $window) {

        $window.scrollTo(0, 0);
        $scope.user_name = window.localStorage['user_name'];
        $scope.token = window.localStorage['token'];
        $rootScope.cartArray = cartArray;
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }


        $scope.coupons = function () {
            //window.location.href ="http://localhost/smtwithpython/SmtSite/index.html#!/coupons";
            window.location.href = "http://toolsomg.com/#!/coupons";
        }

        $scope.goToWishList = function () {
            if (window.localStorage['token']) {
                // $location.path("wishlist");
                //  window.location.href ="http://localhost/smtwithpython/SmtSite/index.html#!/wishlist";
                window.location.href = "http://toolsomg.com/#!/wishlist";
            } else {
                window.location.href = "./login.html"
            }

        }

        $scope.goToCheckout = function () {

            if (window.localStorage['user_id']) {

                if ($rootScope.cartArray.length != 0) {

                    //  $location.path("checkout");

                    // window.location.href ="http://localhost/smtwithpython/SmtSite/index.html#!/checkout";

                    window.location.href = "http://toolsomg.com/#!/checkout";
                }

            }

        }

        // $rootScope.addToWishList = function (productObj) {

        //     // alert('1')
        //     if (window.localStorage['token']) {
        //         addToWishListService.addToWishListMethod(window.localStorage['user_id'], productObj.upload_name).then(function (data) {



        //             if (data.data.status == 'product saved successfully') {
        //                 $("#addedToWishList").modal('show');
        //             } else {
        //             }
        //         })
        //     } else {
        //         alert('Please Login to Add To WishList')
        //     }
        // }

        $rootScope.getProductDetails = function (productObj) {

            window.localStorage['productName'] = productObj.upload_name;

            localStorage.removeItem('isReviewStatus');

            $rootScope.showHintFlag = 'false';

            localStorage.setItem('breadCrumb', productObj.upload_category);

            localStorage.setItem('breadCrumb1', productObj.upload_subcategory);


            //  $window.open("http://localhost/smtwithpython/SmtSite/index.html#!/productDetailPage");

            //   $window.open("http://toolsomg.com/#!/productDetailPage");


            // $location.path("productDetailPage")

        }

        $scope.getProductDetailsFromCompare = function (productObj) {
            window.localStorage['productName'] = productObj;

            localStorage.removeItem('isReviewStatus');

            $rootScope.showHintFlag = 'false';
            //    $window.open("http://localhost/smtwithpython/SmtSite/index.html#!/productDetailPage");
            //   $window.open("http://toolsomg.com/#!/productDetailPage");
            //  $location.path("productDetailPage")
        }

        $scope.gotoCartPage = function () {
            //alert("1")
            //    window.location.href ="http://localhost/smtwithpython/SmtSite/index.html#!/viewCart";
            //    window.location.href = "http://toolsomg.com/#!/viewCart";
        }



        // $scope.getCategories = function () {
        //     getAllCategoriesService.getCategoriesMethod().then(function (data) {
        //         if (data.data.status == 'success') {
        //             $scope.categoryArray = data.data.categories;
        //             // alert(JSON.stringify($scope.categoryArray))
        //         } else {
        //             //  alert(data.data.status)
        //         }
        //     })




        // }

        $rootScope.test = function (searchKey) {
            if (searchKey.length > 0) {
                localStorage.setItem('searchkey', searchKey);
                $rootScope.showHintFlag = 'false';
                //  window.location.href ="http://localhost/smtwithpython/SmtSite/index.html#!/searchPage";
                // window.location.href = "http://toolsomg.com/#!/searchPage";
            }

        }

        //  $scope.getCategories();

        $scope.subCategoryMethod = function (subCategory, categoryName) {
            //alert('1')
            window.localStorage['categoryName'] = "";
            window.localStorage['subCategoryName'] = "";
            window.localStorage['categoryName'] = categoryName;
            window.localStorage['subCategoryName'] = subCategory;
            //  window.location.href ="http://localhost/smtwithpython/SmtSite/index.html#!/categoryPage";
            // window.location.href = "http://toolsomg.com/#!/categoryPage";
        }

        $rootScope.showHintFlag = 'false';
        // $rootScope.showHint = function (searchKey) {
        //     //$scope.showHintFlag = 'true';
        //     //alert(searchKey.length)
        //     if (searchKey.length >= '3') {
        //         searchProductsService.searchProductsMethod(searchKey).then(function (data) {
        //             //alert(JSON.stringify(data))
        //             if (data.data.status == 'success') {
        //                 $rootScope.searchedProducts = data.data.product_info;
        //                 $rootScope.recommendedList = data.data.recommended;
        //                 $rootScope.showHintFlag = 'true';
        //             } else if(data.data.status == 'fail') {
        //                 alert(data.data.data)
        //             }
        //         })
        //     } else if (searchKey.length == '0') {
        //         $rootScope.showHintFlag = 'false';
        //     }

        // }


        if (localStorage.getItem('compareProducts')) {

            $rootScope.compareProducts = JSON.parse(localStorage.getItem('compareProducts'));
        } else {
            $rootScope.compareProducts = compareProducts;
        }

        $scope.goToLogin = function () {
            // $location.path("login");
            window.location.href = "./login.html";
        }

        $scope.goToAddressBook = function () {
            $location.path("addressBook");
        }

        $scope.goToLogout = function () {
            // $location.path('login')
            window.location.href = "./login.html";
        }

        $scope.goToHome = function () {
            window.location.href = "./index.html";;
        }

        // $rootScope.viewCartItems = function () {

        //     if (localStorage.getItem('randomNumber')) {

        //         viewCartService.cartItemsWithLoginMethod(window.localStorage['user_id'], localStorage.getItem('randomNumber')).then(function (data) {

        //             //console.log('1'+JSON.stringify(data))

        //             if (data.data.status == 'success') {

        //                 $rootScope.cartArray = data.data.total_item_list;

        //                 $scope.orderId = data.data.orderid;

        //                 window.localStorage['orderId'] = $scope.orderId;

        //                 localStorage.removeItem('randomNumber');

        //             } else {

        //                 //alert(data.data.status);

        //             }

        //         })

        //     } else {

        //         viewCartService.viewCartMethod(window.localStorage['user_id']).then(function (data) {

        //             // console.log('1'+JSON.stringify(data))

        //             if (data.data.status == 'success') {

        //                 $rootScope.cartArray = data.data.item_list;

        //                 $scope.orderId = data.data.orderid;

        //                 window.localStorage['orderId'] = $scope.orderId;

        //             } else {

        //                 //alert(data.data.status);

        //             }

        //         })

        //     }



        // }


        if (window.localStorage['user_id']) {

            //   $scope.viewCartItems();
        }


        // $scope.getCartItemsWithoutLogin = function () {
        //     viewCartService.cartItemsWithoutLoginMethod(localStorage.getItem('randomNumber')).then(function (data) {
        //         //console.log('1'+JSON.stringify(data))
        //         if (data.data.status == 'success') {
        //             $rootScope.cartArray = data.data.item_list;
        //             // localStorage.setItem('localCartArray', JSON.stringify(data.data.item_list));
        //             $scope.orderId = data.data.orderid;
        //             window.localStorage['orderId'] = $scope.orderId;
        //         } else {
        //             //alert(data.data.status);
        //         }
        //     })
        // }


        // $scope.clearCart = function () {

        //     // alert('1')
        //     if (window.confirm("Are you sure you want to clear cart? ")) {

        //         if (window.localStorage['user_id']) {

        //             deleteCartService.deleteCartMethod(window.localStorage['user_id'], "all").then(function (data) {

        //                 // alert(JSON.stringify(data))

        //                 if (data.data.status == 'all products deleted successfully') {

        //                     $rootScope.cartArray = [];

        //                   //  $scope.viewCartItems();

        //                 }

        //             })

        //         } else {

        //             deleteCartService.deleteCartMethod(localStorage.getItem('randomNumber'), "all").then(function (data) {

        //                 // alert(JSON.stringify(data))

        //                 if (data.data.status == 'all products deleted successfully') {

        //                     $rootScope.cartArray = [];

        //                     // localStorage.removeItem('randomNumber');

        //                  //   $scope.getCartItemsWithoutLogin();

        //                 }

        //             })

        //         }
        //     }

        // }
        $scope.removeCompareItem = function (categoryObj) {

            if ($window.confirm("Are you sure you want to delete this product from comparison?")) {

                $rootScope.compareProducts.splice(JSON.parse(localStorage.getItem('compareProducts')).indexOf(categoryObj.upload_name), 1);
                //alert(JSON.stringify($rootScope.compareProducts))
                localStorage.setItem('compareProducts', JSON.stringify($rootScope.compareProducts))
            }
        }

        $scope.clearCompareProducts = function () {
            if ($window.confirm("Are you sure you want to clear all products?")) {
                $rootScope.compareProducts = localStorage.getItem('compareProducts');
                $rootScope.compareProducts = [];
                localStorage.setItem('compareProducts', JSON.stringify($rootScope.compareProducts));
            }

        }



        // $scope.compareProductsMethod = function () {
        //     //alert("1")

        //     if ($rootScope.compareProducts.length > 1) {
        //        // $location.path("compareProducts");
        //     //  window.location.href ="http://localhost/smtwithpython/SmtSite/index.html#!/compareProducts";
        //     window.location.href ="http://toolsomg.com/#!/compareProducts"
        //     } else {
        //         alert('Please add one more Product to Compare')
        //     }

        // }

        if (localStorage.getItem('randomNumber')) {
            //  $scope.getCartItemsWithoutLogin();
        }


        $scope.searchProductsMore = function (searchKey) {
            localStorage.setItem('searchkey', searchKey);
            //console.log(localStorage.getItem('searchkey'))
            // window.location.href ="http://localhost/smtwithpython/SmtSite/index.html#!/searchPage";

            window.location.href = "http://toolsomg.com/#!/searchPage";
        }

        $scope.goToDashboard = function () {

            if (window.localStorage['token']) {
                //    window.location.href="http://localhost/smtwithpython/SmtSite/index.html#!/dashboard";
                window.location.href = "http://toolsomg.com/#!/dashboard";

            }
            else {

                window.location.href = "./login.html";
            }
        }

        $rootScope.hideDiv = function () {

            $rootScope.showHintFlag = !$rootScope.showHintFlag;

        }

        $scope.categoryBasedProducts = function (categoryName) {

            //     window.localStorage['categoryName'] = "";
            //     window.localStorage['categoryName'] = categoryName;
            //     window.localStorage['brandName'] = "";
            //     window.localStorage['subCategoryName'] = "";
            //     //  window.location.href ="http://localhost/smtwithpython/SmtSite/index.html#!/categoryPage";
            //    window.location.href = "http://toolsomg.com/#!/categoryPage";
            $location.path("categoryPage");
        }

        $rootScope.getProductDetailsFromCart = function (productObj) {

            //  alert(productObj)

            window.localStorage['productName'] = productObj.productdescription;

            localStorage.removeItem('isReviewStatus');

            $rootScope.showHintFlag = 'false';
            // $window.open("http://localhost/smtwithpython/SmtSite/index.html#!/productDetailPage");

            // $window.open("http://toolsomg.com/#!/productDetailPage");


            //  $location.path("productDetailPage")

        }

        //    $scope.removeCartItem = function (cartObj) {


        //         if (window.confirm("Are you sure you want to delete product from shopping cart")) {
        //             if (window.localStorage['user_id']) {

        //                 deleteCartService.deleteCartMethod(window.localStorage['user_id'], cartObj.productdescription).then(function (data) {




        //                     if (data.data.status == 'product deleted successfully') {




        //                     } else if (data.data.status == 'success') {


        //                     }

        //                 })
        //             } else {
        //                 deleteCartService.deleteCartMethod(localStorage.getItem('randomNumber'), cartObj.productdescription).then(function (data) {




        //                     if (data.data.status == 'product deleted successfully') {




        //                     } else if (data.data.status == 'success') {


        //                     }

        //                 })
        //             }
        //         }


        //     }


    }]);