shopMyToolsApp.controller('productCategoriesCtrl', ['$scope', '$rootScope', '$location', '$window', '$filter', 'distributorService', 'newProductDetailedService',
  function ($scope, $rootScope, $location, $window, $filter, distributorService, newProductDetailedService) {

    
    $scope.gridview = true

    $scope.View = "grid";
    $scope.viewby = "12";
    $scope.sort_by = "popularty";
    // $scope.currentPageNumber = 1;
    $scope.fromVal = "0";
    $scope.pricerange = "0-10000";
    $scope.brand = "Powertex";
    $scope.toVal = 12;
    // $window.scrollTo(0, 0);
    // $scope.isReadonly = true;
    // $scope.showSubCat = 'true';

    if (window.localStorage['subcategory'] != '') {
      //  $scope.showSubCat = 'false';
      $scope.breadCrumb = window.localStorage['category'];
      $scope.breadCrumb1 = window.localStorage['subcategory'];
    } else {
      //  $scope.showSubCat = 'true';
      $scope.breadCrumb = window.localStorage['category'];
    }


    $scope.goToHome = function () {
      $location.path("/")
    }

    $scope.pageNavigate = function (breadCrumb) {
      window.localStorage['category'] = breadCrumb;
      window.localStorage['subcategory'] = "";
      $scope.breadCrumb1 = '';

      // $scope.getProductCategories($scope.fromVal, $scope.toVal);
    }


    $scope.getdistributorcatgetdata = function () {
      $scope.brand = [];
      if (window.localStorage['subcategory'] == []) {
        $scope.subcat = [];
      } else {
        $scope.subcat = [window.localStorage['subcategory']];
      }
      distributorService.getdistributortcatget([window.localStorage['category']], $scope.subcat, $scope.brand).then(function (data) {
        // console.log(JSON.data)
        if (data.data.status == 'Success') {
          $scope.getCategoryData = data.data.product_data;
          // $scope.data = $scope.getCategoryData;
          // $scope.viewby = 10;
          // $scope.totalItems = $scope.data.length;
          // $scope.currentPage = 1;
          // $scope.itemsPerPage = $scope.viewby;

        }

      })


    }
    $scope.getdistributorcatgetdata();


    $scope.getnewProductDetails = function (productObj) {

      window.localStorage['productName'] = productObj.upload_name;
      $location.path("productDetailPage");

    }

    $scope.categorylistfilter = function () {
      $scope.brand = [""];
      newProductDetailedService.getnewAllCategoriesFilterOfProduct(window.localStorage['category'], $scope.subcat, $scope.brand, $scope.pricerange, $scope.fromVal, $scope.toVal, $scope.sort_by).then(function (data) {
        // console.log(JSON.data)
        if (data.data.status == 'success') {
          $scope.getCategorylist = data.data.value;
          $scope.subcatArray = data.data.filterdata.subcategory;
          //  $rootScope.selectedArray= data.data.value;
          // $scope.data = $scope.getCategoryData;
          // $scope.viewby = 10;
          // $scope.totalItems = $scope.data.length;
          // $scope.currentPage = 1;
          // $scope.itemsPerPage = $scope.viewby;
          $scope.nodata = false;
        }
        else if (data.data.status == 'No data avialbale') {
          $scope.nodata = true;
          // alert(data.data.status);
        }

      })


    }
    $scope.categorylistfilter();

    $scope.setpage = function (page) {
      if ($scope.fromVal = 12) {
        $scope.fromVal = "0";
      }
      $scope.toVal = page;
      $scope.categorylistfilter();
    }
    $scope.sortby = function (val) {
      $scope.sort_by = val;
      $scope.categorylistfilter();
    }
    $scope.getCategorywiseProductcheck = function (price, range) {
      $scope.pricerange = range;
      $scope.categorylistfilter();
    }
    $scope.getsubcatwiseProductcheck = function (subcat) {
      $scope.subcat = subcat;
      $scope.categorylistfilter();
    }



    $scope.selectedView = function (View) {
      if (View == "grid") {
        $scope.gridview = true;
      } else
        $scope.gridview = false;

    }





    // $scope.getProductCategories = function (fromVal, toVal) {
    //   $scope.loading = true;
    //   product_categories_service.getAllCategoriesOfProduct(window.localStorage['categoryName'], window.localStorage['subCategoryName'], fromVal, toVal).then(function (data) {
    //     $scope.loading = false;
    //     if (data.data) {
    //       // alert('1')
    //       $scope.categories = data.data.subcategories;
    //       $scope.fromVal = data.data.from;
    //       $scope.toVal = data.data.to;
    //       // window.localStorage['categories'] = $scope.categories;
    //       localStorage.setItem('subCategories', JSON.stringify($scope.categories))
    //       $rootScope.brandsData = data.data.brand_data;
    //       // alert($rootScope.brandsData)
    //       // window.localStorage['brandsData'] = $rootScope.brandsData;
    //       localStorage.setItem('brandsData', JSON.stringify($rootScope.brandsData))
    //       $scope.minrange = data.data.minprice;
    //       $scope.maxrange = data.data.maxprice;
    //       localStorage.setItem('minrange', $scope.minrange);
    //       localStorage.setItem('maxrange', $scope.maxrange);

    //       $scope.value = data.data.minrange;
    //       $scope.products = data.data.products;
    //       // $scope.displayItems = $scope.products.slice(0, 5);
    //       $rootScope.totalcount = data.data.totalcount;
    //       // alert($rootScope.totalcount)
    //       $scope.productsprice = $scope.products.prices;
    //       $scope.totalItems = data.data.products.length;
    //       $scope.datalists = data.data.products;
    //     } else {

    //     }
    //   })
    // }


    // $scope.goToHome = function () {

    //   $location.path("/")
    // }


    //   $scope.nextBtn = 'true';
    // $scope.setpage = function (page) {

    //   if ($scope.fromVal = 12) {

    //     $scope.fromVal = 0;
    //   }
    //   $scope.toVal = page;
    //   if ($scope.toVal < $rootScope.totalcount) {
    //     $scope.getProductCategories($scope.fromVal, $scope.toVal);
    //     $scope.nextBtn = 'true';
    //   } else {
    //     $scope.toVal = $rootScope.totalcount;
    //     $scope.getProductCategories($scope.fromVal, $scope.toVal);
    //     $scope.nextBtn = 'false';
    //   }
    //   if ($rootScope.selectedArray) {
    //     if ($rootScope.selectedArray.subcategory != '') {
    //       $scope.getCategorywiseProductcheck('cat', $rootScope.selectedArray.subcategory)
    //     }
    //     if ($rootScope.selectedArray.brand != '') {
    //       $scope.getCategorywiseProductcheck('brand', $rootScope.selectedArray.brand)
    //     }

    //     if ($rootScope.selectedArray.maxrange != '') {
    //       $scope.getCategorywiseProductcheck('price', $rootScope.selectedArray.maxrange)
    //     }
    //   }
    //   if ($rootScope.selectedArray1) {
    //     if ($rootScope.selectedArray1.sortdata != '') {

    //       $scope.getCategorywiseProductcheck('sort', $rootScope.selectedArray1.category)
    //     }

    //   }
    //   $scope.currentPageNumber = 1;

    // }

    //   $scope.test1 = function (val) {
    //     $scope.minrange = 10;
    //     $scope.maxrange = 1000;
    //     $scope.maxrange1 = val;

    //   }

    //   $scope.pageNavigate = function (breadCrumb) {
    //     window.localStorage['categoryName'] = breadCrumb;
    //     window.localStorage['subCategoryName'] = "";
    //     $scope.breadCrumb1 = '';
    //     $scope.getProductCategories($scope.fromVal, $scope.toVal);
    //   }



    //   $rootScope.getProductDetailsCat = function (productObj) {

    //     window.localStorage['productName'] = productObj.upload_name;

    //     localStorage.removeItem('isReviewStatus');

    //     localStorage.setItem('breadCrumb', $scope.breadCrumb);

    //     localStorage.setItem('breadCrumb1', productObj.upload_subcategory);

    //     $rootScope.showHintFlag = 'false';
    //     // $window.open("http://localhost/smtwithpython/SmtSite/index.html#!/productDetailPage");

    //    // $window.open("http://toolsomg.com/#!/productDetailPage");


    //       $location.path("productDetailPage")

    //   }


    //   $scope.setOrder = function (id, order, reverse) {
    //     $scope.sorting.id = id;
    //     $scope.sorting.order = order;
    //     $scope.sorting.direction = reverse;
    //   };

    //   $scope.show_by_list = ["12", "24", "36"];

    //   $scope.subCatList = [""];
    //   $scope.brandList = [""];

    //  // $scope.pricefilter = ['']

    //   $scope.showBrandFilter = 'true';

    // $scope.getCategorywiseProductcheck = function (typeval, subCategory) {

    //   //alert(subCategory)
    //  // alert('1'+$scope.subCatList)
    //   $scope.loading = true;
    //   $scope.nextBtn = 'true';
    //   if (typeval == "cat") {
    //      if (typeof (subCategory) == 'object') {
    //       $scope.subCatList = subCategory;
    //     } else {
    //      // alert('1'+$scope.subCatList.indexOf(subCategory))
    //       if ($scope.subCatList.indexOf(subCategory) >= 1) {
    //         // alert($scope.subCatList.indexOf(subCategory))
    //         // alert('1'+$scope.subCatList)
    //        // var index = $scope.subCatList.indexOf(subCategory);
    //         $scope.subCatList.splice($scope.subCatList.indexOf(subCategory),1);
    //         // alert('1'+$scope.subCatList.length)
    //         // $scope.toVal = $scope.viewby;
    //       } else {
    //         $scope.subCatList.push(subCategory);
    //       }
    //     }

    //     if (window.localStorage['categoryName'] != "") {
    //       $scope.categoryName = window.localStorage['categoryName'];
    //     } else {
    //       $scope.categoryName = "";
    //     }
    //     if ($rootScope.selectedArray) {
    //       if ($rootScope.selectedArray.pricerange == '' || $rootScope.selectedArray.pricerange == undefined) {
    //         // alert('1')
    //         if ($rootScope.selectedArray.maxrange) {
    //           $scope.pricerange = $rootScope.selectedArray.minrange + '-' + $rootScope.selectedArray.maxrange;
    //         } else {
    //           $scope.pricerange = localStorage.getItem('minrange') + '-' + localStorage.getItem('maxrange');
    //         }

    //       } else if ($rootScope.selectedArray.maxrange != '' || $rootScope.selectedArray.maxrange != undefined) {
    //         //  alert('2')
    //         $scope.pricerange = $rootScope.selectedArray.minrange + '-' + $rootScope.selectedArray.maxrange;
    //       }
    //       else if ($rootScope.selectedArray.pricerange != '') {
    //         // alert('3')
    //         $scope.pricerange = $scope.minrange + '-' + $rootScope.selectedArray.pricerange;
    //       }
    //     }
    //     else {
    //       // alert('4')
    //       $scope.pricerange = localStorage.getItem('minrange') + '-' + localStorage.getItem('maxrange');
    //     }


    //    // alert($scope.toVal)
    //     product_subcategories_filter.getAllCategoriesFilterOfProduct($scope.categoryName, $scope.subCatList, $scope.brandList, $scope.pricerange, $scope.fromVal, $scope.toVal, $scope.sort_by).then(function (data) {
    //       $scope.loading = false;
    //       if (data.data.status == 'Success') {
    //         $rootScope.categories = JSON.parse(localStorage.getItem('subCategories'));
    //         $rootScope.selectedArray = data.data.filterdata;
    //         localStorage.setItem('selectedArray', JSON.stringify($rootScope.selectedArray));
    //         // if ($scope.subCatList.length == 1) {

    //         //   $scope.getProductCategories($scope.fromVal, $scope.viewby);
    //         // }
    //         $scope.minrange = data.data.minprice;
    //         $scope.maxrange = data.data.maxprice;
    //         $scope.products = data.data.products;
    //          $scope.fromVal = data.data.from;
    //       $scope.toVal = data.data.to;
    //         $rootScope.brandsData = JSON.parse(localStorage.getItem('brandsData'));
    //         $rootScope.totalcount = data.data.totalcount;
    //       }else if(data.data.status == 'No data avialbale'){
    //         $scope.products =[];
    //       }
    //       else {
    //         alert('Categories not available');
    //       }

    //     });
    //   } else if (typeval == 'sort') {
    //     //alert('sort')
    //     if ($rootScope.selectedArray) {
    //       if ($rootScope.selectedArray.pricerange == '' || $rootScope.selectedArray.pricerange == undefined) {
    //         $scope.pricerange = $scope.minrange + '-' + $scope.maxrange;
    //       } else if ($rootScope.selectedArray.pricerange != '') {
    //         $scope.pricerange = $scope.minrange + '-' + $rootScope.selectedArray.pricerange;
    //       }
    //     }
    //     else {
    //       //  alert('2sd')
    //       $scope.pricerange = localStorage.getItem('minrange') + '-' + localStorage.getItem('maxrange');
    //     }

    //     // if($rootScope.selectedArray1){

    //     // }

    //     $scope.categoryName = subCategory;
    //     product_subcategories_filter.getAllCategoriesFilterOfProduct($scope.categoryName, $scope.subCatList, $scope.brandList, $scope.pricerange, $scope.fromVal, $scope.toVal, $scope.sort_by).then(function (data) {
    //       $scope.loading = false;
    //       if (data.data) {
    //         $rootScope.categories = JSON.parse(localStorage.getItem('subCategories'));
    //         $rootScope.selectedArray = data.data.filterdata;
    //         localStorage.setItem('selectedArray', JSON.stringify($rootScope.selectedArray));
    //         // alert(localStorage.getItem('brandsData'))
    //         $rootScope.brandsData = JSON.parse(localStorage.getItem('brandsData'));
    //         $scope.minrange = data.data.minprice;
    //         $scope.maxrange = data.data.maxprice;
    //         $scope.maxrange1 = $rootScope.selectedArray.maxrange;
    //         $scope.products = data.data.products;
    //         $rootScope.totalcount = data.data.totalcount;
    //        // console.log(JSON.stringify($scope.products))
    //       }
    //       else {
    //         alert('Categories not available');
    //       }
    //     });
    //   }
    //   else if (typeval == 'price') {
    //      //alert('prce')
    //     // console.log(subCategory)
    //     $scope.maxrange = subCategory;
    //     if (window.localStorage['categoryName'] != "") {
    //       $scope.categoryName = window.localStorage['categoryName'];
    //     } else {
    //       $scope.categoryName = "";
    //     }

    //     if (window.localStorage['subCategoryName']) {
    //       $scope.subCatList.push(window.localStorage['subCategoryName'])
    //     }
    //    // $scope.pricerange = $scope.minrange + '-' + $scope.maxrange;
    //    if(subCategory.includes('-')){
    //      $scope.pricerange = subCategory;
    //    }else{
    //      $scope.pricerange = $scope.minrange + '-' + $scope.maxrange;
    //    }

    //     product_subcategories_filter.getAllCategoriesFilterOfProduct($scope.categoryName, $scope.subCatList, $scope.brandList, $scope.pricerange, $scope.fromVal, $scope.toVal, $scope.sort_by).then(function (data) {
    //       $scope.loading = false;
    //    //   alert(JSON.stringify(data))
    //       if (data.data.status == 'Success') {
    //         //alert()

    //         $rootScope.selectedArray = data.data.filterdata;
    //         $rootScope.totalcount = data.data.totalcount;
    //         localStorage.setItem('selectedArray', JSON.stringify($rootScope.selectedArray));

    //         $scope.minrange = data.data.minprice;
    //         // $scope.maxrange = data.data.maxprice;
    //         $scope.maxrange = localStorage.getItem('maxrange');

    //         $scope.maxrange1 = $rootScope.selectedArray.maxrange;
    //         $scope.products = data.data.products;
    //         //alert(JSON.stringify($scope.products))
    //         $rootScope.brandsData = JSON.parse(localStorage.getItem('brandsData'));
    //         $rootScope.categories = JSON.parse(localStorage.getItem('subCategories'));
    //       }
    //       else if(data.data.status == 'No data avialbale'){
    //        // alert('Categories not available');
    //        $scope.products=[];
    //         $rootScope.brandsData = JSON.parse(localStorage.getItem('brandsData'));
    //         $rootScope.categories = JSON.parse(localStorage.getItem('subCategories'));
    //       }
    //     });
    //   }
    //   else if (typeval == "brand") {
    //     //  alert('brand')
    //     //alert(subCategory)
    //     if (typeof (subCategory) == 'object') {
    //       $scope.brandList = subCategory;
    //     } else {
    //       if ($scope.brandList.indexOf(subCategory) >= 1) {
    //         // var index = $scope.brandList.indexOf(subCategory);
    //         $scope.brandList.splice($scope.brandList.indexOf(subCategory), 1);
    //       }
    //       else {
    //         // $scope.brandList = [];
    //         $scope.brandList.push(subCategory);
    //       }

    //     }
    //     if (window.localStorage['categoryName'] != "") {
    //       $scope.categoryName = window.localStorage['categoryName'];
    //     } else {
    //       $scope.categoryName = "";
    //     }

    //     if (window.localStorage['subCategoryName']) {
    //       $scope.subCatList.push(window.localStorage['subCategoryName'])
    //     }
    //     product_subcategories_filter.getAllCategoriesFilterOfProduct($scope.categoryName, $scope.subCatList, $scope.brandList, $scope.pricerange, $scope.fromVal, $scope.toVal, $scope.sort_by).then(function (data) {
    //       $scope.loading = false;
    //       if (data.data) {

    //         $rootScope.selectedArray = data.data.filterdata;
    //         localStorage.setItem('selectedArray', JSON.stringify($rootScope.selectedArray));
    //         $rootScope.brandsDataArray = $rootScope.selectedArray.brand;
    //         $rootScope.brandsData = JSON.parse(localStorage.getItem('brandsData'));
    //         $scope.minrange = data.data.minprice;
    //         $scope.maxrange = data.data.maxprice;
    //         $scope.products = data.data.products;
    //         $rootScope.totalcount = data.data.totalcount;
    //         $rootScope.categories = JSON.parse(localStorage.getItem('subCategories'));

    //       }
    //       else {
    //         alert('Categories not available');
    //       }

    //     });

    //   }



    // }



    // $scope.previousPageMethod = function (fromVal, toVal, viewby) {
    //   var fromVal = JSON.parse(fromVal) - JSON.parse(viewby);
    //   //  alert(fromVal)
    //   var toVal = JSON.parse(toVal) - JSON.parse(viewby);
    //   $scope.nextBtn = 'true';
    //   $scope.loading = true;
    //   product_categories_service.getAllCategoriesOfProduct(window.localStorage['categoryName'], window.localStorage['subCategoryName'], fromVal, toVal).then(function (data) {
    //     $scope.loading = false;
    //     if (data.data) {
    //       $scope.categories = data.data.subcategories;
    //       $scope.fromVal = data.data.from;
    //       $scope.toVal = data.data.to;
    //       $scope.currentPageNumber = $scope.currentPageNumber - 1;
    //       window.localStorage['categories'] = $scope.categories;
    //       $scope.brandsData = data.data.brand_data;
    //       window.localStorage['brandsData'] = $scope.brandsData;
    //       $scope.minrange = data.data.minprice;
    //       $scope.maxrange = data.data.maxprice;
    //       $scope.products = data.data.products;
    //      // console.log($scope.products)
    //       $scope.totalcount = data.data.totalcount;
    //      $scope.products.forEach(function (element) {
    //     // alert(element)
    //     if ($scope.sort_by == 'price_low_high') {
    //       element.prices[0].offer_price = parseInt(element.prices[0].offer_price);
    //       $scope.products = $filter('orderBy')($scope.products, 'prices[0].offer_price');
    //     }
    //     else if ($scope.sort_by == 'namefilter') {
    //       $scope.products = $filter('orderBy')($scope.products, 'upload_name');
    //     } else if ($scope.sort_by == 'popularty') {
    //       $scope.products = $filter('orderBy')($scope.products, 'avgrating');
    //     } else if ($scope.sort_by == 'price_high_low') {
    //       element.prices[0].offer_price = parseInt(element.prices[0].offer_price);
    //       $scope.products = $filter('orderBy')($scope.products, '-prices[0].offer_price');
    //     } else if ($scope.sort_by == 'topselling') {
    //       $scope.products = $filter('orderBy')($scope.products, 'salesqty');
    //     }
    //   })
    //       if ($rootScope.selectedArray) {
    //         if ($rootScope.selectedArray.subcategory != '') {
    //           $scope.getCategorywiseProductcheck('cat', $rootScope.selectedArray.subcategory)
    //         }
    //         if ($rootScope.selectedArray.brand != '') {
    //           $scope.getCategorywiseProductcheck('brand', $rootScope.selectedArray.brand)
    //         }

    //         if ($rootScope.selectedArray.maxrange != '' && $scope.price != undefined ) {
    //           $scope.getCategorywiseProductcheck('price', $rootScope.selectedArray.maxrange)
    //         }

    //          if ($rootScope.selectedArray.pricerange != '' && $rootScope.selectedArray.pricerange != undefined && $scope.price != undefined) {
    //             $scope.getCategorywiseProductcheck('price', $rootScope.selectedArray.pricerange)
    //           }
    //       }
    //     } else {

    //     }



    //   });
    // }

    // $scope.nextPageMethod = function (fromVal, toVal, viewby) {

    //   // alert("hai"+toVal);
    //   //alert($rootScope.totalcount);
    //   $scope.loading = true;
    //   if ($rootScope.totalcount < toVal) {
    //     //  alert($rootScope.totalcount);
    //     var fromVal = 0;
    //     var toVal = $rootScope.totalcount;
    //     $scope.nextBtn = 'false';
    //     $scope.loading = false;
    //   } else {
    //     var fromVal = JSON.parse(fromVal) + JSON.parse(viewby);
    //     var toVal = JSON.parse(toVal) + JSON.parse(viewby);
    //     // alert(toVal)
    //     product_categories_service.getAllCategoriesOfProduct(window.localStorage['categoryName'], window.localStorage['subCategoryName'], fromVal, toVal).then(function (data) {
    //       $scope.loading = false;
    //       if (data.data.status == 'Success') {
    //         $scope.categories = data.data.subcategories;
    //         $scope.fromVal = data.data.from;
    //         $scope.toVal = data.data.to;
    //         //alert( $scope.toVal )
    //         window.localStorage['categories'] = $scope.categories;
    //         $scope.brandsData = data.data.brand_data;
    //         window.localStorage['brandsData'] = $scope.brandsData;
    //         $scope.minrange = data.data.minprice;
    //         $scope.maxrange = data.data.maxprice;
    //         $scope.totalcount = data.data.totalcount;
    //         $scope.currentPageNumber = $scope.currentPageNumber + 1;
    //         $scope.products = data.data.products;
    //         // alert(JSON.stringify($rootScope.selectedArray1))
    //        $scope.products.forEach(function (element) {
    //     // alert(element)

    //     //alert($scope.price)
    //     if ($scope.sort_by == 'price_low_high') {
    //       element.prices[0].offer_price = parseInt(element.prices[0].offer_price);
    //       $scope.products = $filter('orderBy')($scope.products, 'prices[0].offer_price');
    //     }
    //     else if ($scope.sort_by == 'namefilter') {
    //       $scope.products = $filter('orderBy')($scope.products, 'upload_name');
    //     } else if ($scope.sort_by == 'popularty') {
    //       $scope.products = $filter('orderBy')($scope.products, 'avgrating');
    //     } else if ($scope.sort_by == 'price_high_low') {
    //       element.prices[0].offer_price = parseInt(element.prices[0].offer_price);
    //       $scope.products = $filter('orderBy')($scope.products, '-prices[0].offer_price');
    //     } else if ($scope.sort_by == 'topselling') {
    //       $scope.products = $filter('orderBy')($scope.products, 'salesqty');
    //     }
    //   })
    //         if ($rootScope.selectedArray) {
    //           // alert('1')
    //           if ($rootScope.selectedArray.subcategory != '') {

    //             $scope.getCategorywiseProductcheck('cat', $rootScope.selectedArray.subcategory)
    //           }
    //           if ($rootScope.selectedArray.brand != '') {
    //             $scope.getCategorywiseProductcheck('brand', $rootScope.selectedArray.brand)
    //           }
    //           if ($scope.price != undefined && $rootScope.selectedArray.maxrange != '') {
    //             $scope.getCategorywiseProductcheck('price', $rootScope.selectedArray.maxrange)
    //           }
    //           if ($rootScope.selectedArray.pricerange != '' && $rootScope.selectedArray.pricerange != undefined && $scope.price != undefined) {
    //             $scope.getCategorywiseProductcheck('price', $rootScope.selectedArray.pricerange)
    //           }
    //         }

    //       }else if(data.data.status == 'No data avialbale'){
    //         $scope.products = [];
    //        //  $scope.nextBtn = 'false';
    //       }
    //        else {
    //         //alert('Categories not available');
    //       }
    //     });
    //   }



    // // }

    // $scope.sortby = function (val123) {
    //   $scope.sort_by = val123;
    //   // alert($scope.sort_by)
    //   //$scope.loading = true;  
    //   $scope.products.forEach(function (element) {
    //     // alert(element)
    //     if ($scope.sort_by == 'price_low_high') {
    //       element.prices[0].offer_price = parseInt(element.prices[0].offer_price);
    //       $scope.products = $filter('orderBy')($scope.products, 'prices[0].offer_price');
    //     }
    //     else if ($scope.sort_by == 'namefilter') {
    //       $scope.products = $filter('orderBy')($scope.products, 'upload_name');
    //     } else if ($scope.sort_by == 'popularty') {
    //       $scope.products = $filter('orderBy')($scope.products, 'avgrating');
    //     } else if ($scope.sort_by == 'price_high_low') {
    //       element.prices[0].offer_price = parseInt(element.prices[0].offer_price);
    //       $scope.products = $filter('orderBy')($scope.products, '-prices[0].offer_price');
    //     } else if ($scope.sort_by == 'topselling') {
    //       $scope.products = $filter('orderBy')($scope.products, 'salesqty');
    //     }
    //   })
    // $scope.products = $filter('orderBy')($scope.products,'prices[0].offer_price' );
    // console.log(JSON.stringify($scope.products))
    // product_subcategories_filter.getAllCategoriesFilterOfProduct(window.localStorage['categoryName'], $scope.subCatList, $scope.brandList, $scope.pricerange, $scope.fromVal, $scope.toVal, $scope.sort_by).then(function (data) {
    //   $scope.loading = false;
    //   if (data.data) {
    //     $scope.fromVal = data.data.from;
    //     $scope.toVal = data.data.to;
    //     $rootScope.categories = data.data.subcategory;
    //    // $scope.brandsData = data.data.brand_data;
    //    $rootScope.brandsData =  localStorage.getItem('brandsData')
    //    // window.localStorage['brandsData'] = $scope.brandsData;
    //     $scope.totalcount = data.data.totalcount;
    //     $scope.minrange = data.data.minprice;
    //     $scope.maxrange = data.data.maxprice;
    //     $scope.products = data.data.products;
    //     $rootScope.selectedArray1=data.data.filterdata;

    //    // alert(JSON.stringify($rootScope.selectedArray1))
    //     // alert('1')
    //     if ($rootScope.selectedArray) {
    //       // alert('2')
    //       if ($rootScope.selectedArray.subcategory != '') {
    //         //alert(hai)
    //         $scope.getCategorywiseProductcheck('cat', $rootScope.selectedArray.subcategory)
    //         // alert(hai)
    //       }
    //       if ($rootScope.selectedArray.brand != '' && $rootScope.selectedArray.subcategory != '' && $rootScope.selectedArray.category != '') {
    //         //	 alert(hai1)
    //         $scope.getCategorywiseProductcheck('brand', $rootScope.selectedArray.brand)
    //       }

    //       if ($rootScope.selectedArray.pricerange != undefined && $rootScope.selectedArray.pricerange != '') {
    //         //  alert('1'+$rootScope.selectedArray.pricerange)
    //         $scope.getCategorywiseProductcheck('price', $rootScope.selectedArray.pricerange)
    //       } else if ($rootScope.selectedArray.maxrange != '') {
    //         // alert('2'+$rootScope.selectedArray.maxrange)
    //         $scope.getCategorywiseProductcheck('price', $rootScope.selectedArray.maxrange)
    //       }
    //     }
    //   } else {

    //   }


    // });


    // }


    //   if (localStorage.getItem('selectedArray')) {
    //     $scope.selectedArray = JSON.parse(localStorage.getItem('selectedArray'));
    //     // alert($scope.selectedArray.category)
    //     if ($scope.selectedArray.category != '' && $scope.selectedArray.subcategory == '' && $scope.selectedArray.brand == '') {
    //       $scope.getProductCategories($scope.fromVal, $scope.toVal);
    //     }
    //     if ($scope.selectedArray.subcategory != '' && $scope.selectedArray.category != '') {
    //       window.localStorage['categoryName'] = $scope.selectedArray.category;
    //       // alert(window.localStorage['categoryName'])
    //       $scope.getCategorywiseProductcheck('cat', $scope.selectedArray.subcategory)

    //     }
    //     if ($scope.selectedArray.brand != '' && $scope.selectedArray.category != '') {
    //       window.localStorage['categoryName'] = $scope.selectedArray.category;
    //       $scope.getCategorywiseProductcheck('brand', $scope.selectedArray.brand)
    //     }
    //     if ($scope.selectedArray.brand != '' && $scope.selectedArray.category != '' && $scope.selectedArray.subcategory != '') {
    //       window.localStorage['categoryName'] = $scope.selectedArray.category;
    //       $scope.getCategorywiseProductcheck('brand', $scope.selectedArray.brand)
    //       $scope.getCategorywiseProductcheck('cat', $scope.selectedArray.subcategory)
    //     }
    //   } else if (window.localStorage['categoryName'] || window.localStorage['subCategoryName']) {
    //     $scope.getProductCategories($scope.fromVal, $scope.toVal);
    //   } else {
    //     $location.path("/");
    //   }




    // }
    //]);


    // shopMyToolsApp.controller('brandProductsCtrl', ['$scope', '$rootScope', '$location', '$window',
    //   function ($scope, $rootScope, $location, $window) {
    //     $scope.brandList = [""]

    //     $scope.x = "Grid";
    //     $scope.viewby = "12";
    //     $scope.sort_by = "popularty";
    //     $scope.currentPageNumber = 1;
    //     $scope.fromVal = 0;
    //     $scope.toVal = 12;
    //     $window.scrollTo(0, 0);

    // alert(window.localStorage['brandName'])

    // if (window.localStorage['brandName']) {
    //   $scope.brandBreadCrumb = window.localStorage['brandName'];
    // }



    // $scope.getCategorywiseProductcheck = function (fromVal, toVal) {

    //   $scope.nextBtn = 'true';

    //   $scope.brandName = localStorage.getItem('brandName');

    //   $scope.brandList.push($scope.brandName);

    //   $scope.categoryName = "";
    //   $scope.subCatList = [""]

    //   product_subcategories_filter.getAllCategoriesFilterOfProduct($scope.categoryName, $scope.subCatList, $scope.brandList, $scope.pricerange, fromVal, toVal, $scope.sort_by).then(function (data) {
    //     if (data.data.status == 'Success') {
    //       $rootScope.categories = data.data.subcategory;
    //       //alert()
    //       $rootScope.selectedArray = data.data.filterdata;
    //       $rootScope.brandsDataArray = $rootScope.selectedArray.brand;
    //       $rootScope.brandsData = [];
    //       $rootScope.brandsDataArray.forEach(function (item) {
    //         if (item) {

    //           localStorage.setItem('brandName', item)
    //           $rootScope.brandsData.push(item)

    //         }
    //       })



    //       $scope.minrange = data.data.minprice;
    //       $scope.maxrange = data.data.maxprice;
    //       $rootScope.products = data.data.products;
    //       $rootScope.totalcount = data.data.totalcount;

    //     }else if(data.data.status == 'No data avialbale'){
    //        $rootScope.products = [];
    //     }
    //     else {
    //       alert('Categories not available');
    //     }

    //   });





    // }

    // $scope.sortby = function (val123) {
    //   $scope.sort_by = val123;
    //   $scope.categoryName = "";
    //   $scope.subCatList = [""]
    //   // alert($rootScope.val)
    //   product_subcategories_filter.getAllCategoriesFilterOfProduct($scope.categoryName, $scope.subCatList, $scope.brandList, $scope.pricerange, $scope.fromVal, $scope.toVal, $scope.sort_by).then(function (data) {
    //     if (data.data) {
    //       $scope.fromVal = data.data.from;
    //       $scope.toVal = data.data.to;
    //       $rootScope.categories = data.data.subcategory;
    //       $scope.brandsData = data.data.brand_data;
    //       window.localStorage['brandsData'] = $scope.brandsData;
    //       $scope.totalcount = data.data.totalcount;
    //       $scope.minrange = data.data.minprice;
    //       $scope.maxrange = data.data.maxprice;
    //       $rootScope.products = data.data.products;
    //       $rootScope.selectedArray1 = data.data.filterdata;


    //     } else {

    //     }


    //   });


    // }

    // $scope.previousPageMethod = function (fromVal, toVal, viewby) {

    //   var fromVal = JSON.parse(fromVal) - JSON.parse(viewby);
    //   var toVal = JSON.parse(toVal) - JSON.parse(viewby);
    //   $scope.nextBtn = 'true';
    //   $scope.categoryName = "";
    //   $scope.subCatList = [""]
    //   product_subcategories_filter.getAllCategoriesFilterOfProduct($scope.categoryName, $scope.subCatList, $scope.brandList, $scope.pricerange, fromVal, toVal, $scope.sort_by).then(function (data) {
    //     if (data.data) {
    //       $rootScope.categories = data.data.subcategory;
    //       //alert()
    //       $rootScope.selectedArray = data.data.filterdata;
    //       $scope.fromVal = data.data.from;
    //       $scope.toVal = data.data.to;
    //       $rootScope.brandsDataArray = $rootScope.selectedArray.brand;
    //       $rootScope.brandsData = [];
    //       $rootScope.brandsDataArray.forEach(function (item) {
    //         if (item) {
    //           $rootScope.brandsData.push(item)
    //         }
    //       })

    //       $scope.minrange = data.data.minprice;
    //       $scope.maxrange = data.data.maxprice;
    //       $rootScope.products = data.data.products;
    //       $rootScope.totalcount = data.data.totalcount;
    //       $scope.currentPageNumber = $scope.currentPageNumber - 1;

    //     }
    //     else {
    //       alert('Categories not available');
    //     }

    //   });
    // }

    // $scope.nextPageMethod = function (fromVal, toVal, viewby) {
    //   //alert("hai"+toVal);
    //   //  alert($rootScope.totalcount);

    //   if ($rootScope.totalcount < toVal) {
    //     //  alert($rootScope.totalcount);
    //     var fromVal = 0;
    //     var toVal = $rootScope.totalcount;
    //     $scope.nextBtn = 'false';
    //     //alert(fromVal)
    //   } else {
    //     var fromVal = JSON.parse(fromVal) + JSON.parse(viewby);
    //     var toVal = JSON.parse(toVal) + JSON.parse(viewby);
    //     $scope.categoryName = "";
    //     $scope.subCatList = [""]
    //     product_subcategories_filter.getAllCategoriesFilterOfProduct($scope.categoryName, $scope.subCatList, $scope.brandList, $scope.pricerange, fromVal, toVal, $scope.sort_by).then(function (data) {
    //       if (data.data) {
    //         $rootScope.categories = data.data.subcategory;

    //         $rootScope.selectedArray = data.data.filterdata;
    //         $scope.fromVal = data.data.from;
    //         $scope.toVal = data.data.to;
    //         $scope.minrange = data.data.minprice;
    //         $scope.maxrange = data.data.maxprice;
    //         $rootScope.products = data.data.products;
    //         $rootScope.totalcount = data.data.totalcount;
    //         $scope.currentPageNumber = $scope.currentPageNumber + 1;


    //       }
    //       else {
    //         alert('Categories not available');
    //       }

    //     });
    //   }



    // }


    //   $scope.setpage = function (page) {
    //     // alert(page)
    //     if ($scope.fromVal = 12) {

    //       $scope.fromVal = 0;
    //     }
    //     $scope.toVal = page;
    //     if ($scope.toVal < $rootScope.totalcount) {
    //       $scope.getCategorywiseProductcheck($scope.fromVal, $scope.toVal);
    //       $scope.nextBtn = 'true';
    //     } else {
    //       $scope.toVal = $rootScope.totalcount;
    //       $scope.getCategorywiseProductcheck($scope.fromVal, $scope.toVal);
    //       $scope.nextBtn = 'false';
    //     }
    //     $scope.currentPageNumber = 1;

    //   }

    //   if (localStorage.getItem('brandName') != "") {

    //     $scope.getCategorywiseProductcheck($scope.fromVal, $scope.toVal);
    //   }



  }])

/*shopMyToolsApp.filter('emptyFilter', function() {
  return function(brandsData) {
    var filteredArray = [];
      angular.forEach(brandsData, function(item) {
        if (item) filteredArray.push(item);
      });
    return filteredArray;  
  };
}*/







