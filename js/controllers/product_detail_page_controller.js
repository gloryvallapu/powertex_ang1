

shopMyToolsApp.controller('product_detailed_controller', ['$scope', '$http', '$location',

    '$rootScope', 'newProductDetailedService', 'newHomePageService', 'CartService', '$route', '$window',

    function ($scope, $http, $location, $rootScope, newProductDetailedService, newHomePageService, CartService, $route, $window) {
        $window.scrollTo(0, 0);
        $rootScope.zoomImg = '';
        $scope.usertype = window.localStorage['usertype'];
        $rootScope.token = window.localStorage['token'];
        $scope.qty = "1";
        if (window.localStorage['usertype'] == "web") {
            $scope.dashboard = false;
        } else if (window.localStorage['usertype'] == undefined) {
            $scope.dashboard = false;
        } else {
            $scope.dashboard = true;
        }

        $scope.getnewProductDetailData = function () {
            newProductDetailedService.getnewDetailsOfProduct(window.localStorage['productName']).then(function (data) {
                if (data.data.status == 'success') {
                    var result = data.data;
                    $scope.brandDetailProductData = result.Product;
                    $scope.productImagesLow = result.Low_Images;
                    $scope.productImagesHigh = result.High_Images;
                    $scope.productFirstImg = $scope.productImagesHigh[0];
                    $scope.productdetailPrices = result.price_info;
                    $rootScope.productattributes = result.attribute_info;
                    $scope.relatedProducts = data.data.Related_Products;
                    $scope.upsellProducts = result.Upsell_products;

                }

            })

        }

        $scope.getnewProductDetailData();

        $scope.decreaseValue = function (qty) {
            if (qty > 1) {
                $scope.qty--;
            }
        }

        $scope.increaseValue = function (qty) {
            $scope.qty++;
        }

        String.prototype.replaceAt = function (index, replacement) {
            return this.substr(0, index) + replacement + this.substr(index + replacement.length);
        }
        $scope.replaceSelected = function (img) {

            var url = "https://s3.ap-south-1.amazonaws.com/gstbucket1/PPT/ProductImages/";
            $scope.productImagesHigh = img;
            $scope.splitpath = $scope.productImagesHigh.split('/')[6];
            $scope.splitpath = $scope.splitpath.replaceAt(2, "2");
            $scope.productFirstImg = url + $scope.splitpath;
            $rootScope.zoomImg = url + $scope.splitpath;

        }

        $scope.openImg = function (img) {
            $rootScope.openimage = $scope.productFirstImg.replace('100X100', '1000X1000');
            $("#openImg").modal('show');
        }

        if (window.localStorage['subcategory'] != '') {
            $scope.breadCrumb = window.localStorage['category'];
            $scope.breadCrumb1 = window.localStorage['subcategory'];
            //alert($scope.breadCrumb1)
        } else if (localStorage.getItem('breadCrumb')) {
            $scope.breadCrumb = localStorage.getItem('breadCrumb');
            $scope.breadCrumb1 = localStorage.getItem('breadCrumb1');
        }
        else {
            $scope.breadCrumb = window.localStorage['category'];
        }

        if (window.localStorage['productName']) {
            $scope.breadCrumb2 = window.localStorage['productName'];
        }

        $scope.goToHome = function () {
            $location.path("/")
        }

        $scope.pageNavigate = function (breadCrumb) {
            window.localStorage['category'] = breadCrumb;
            //localStorage.removeItem('selectedArray');
            $location.path("categorylist");
        }
        $scope.subcat = function (subCat) {
            window.localStorage['subcategory'] = subCat;
            window.localStorage['category'] = $scope.breadCrumb;

            // localStorage.removeItem('selectedArray');
            $location.path("categorylist");
        }


        $scope.slickConfig = {
            enabled: true,
            autoplay: true,
            draggable: false,
            autoplaySpeed: 5500,
            responsive: [
                // {
                //     breakpoint: 1024,
                //     settings: "unslick"
                // },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 468,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 320,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }

            ]
        };

        $scope.addToWishList = function (productObj) {

            // alert(window.localStorage['token']);
            if (window.localStorage['token']) {
                newHomePageService.addNewWishListMethod(window.localStorage['user_id'], productObj).then(function (data) {



                    if (data.data.status == 'product saved successfully') {

                        // window.localStorage['wishlistcount'] = data.data.items_count;

                        $("#addedToWishList").modal('show');
                        $scope.getWishlistItems();

                    } else {

                        alert(data.data.status);

                    }

                })
            } else {

                alert('Please Login to Add To WishList')
            }


        }

        $scope.getWishlistItems = function () {
            $scope.loading = true;
            newHomePageService.yourWishlistMethod(window.localStorage['user_id']).then(function (data) {

                if (data.data.status == 'success') {
                    $rootScope.wishlistBreadCrumbFlag = localStorage.getItem('wishlistBreadCrumbFlag');
                    // alert( $rootScope.wishlistBreadCrumbFlag)
                    $rootScope.yourWishlist = data.data.prod_info;
                    $scope.loading = false;
                    $scope.data = $rootScope.yourWishlist;
                    $scope.viewby = 5;
                    $rootScope.wishlistItems = data.data.prod_info.length;

                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                } else {
                    alert(data.data.status);
                }

            })
        }
        if (window.localStorage['usertype'] == "web") {
            $scope.getWishlistItems();

        }

        $scope.removeWishListItem = function (wishListObj) {
            if ($window.confirm("Are you sure you want to delete this product from Wishlist?")) {
                newHomePageService.removeWishListItemMethod(window.localStorage['user_id'], wishListObj.upload_name).then(function (data) {
                    //    alert(JSON.stringify(data))
                    if (data.data.status == 'product removed successfully') {
                        // window.localStorage['wishlistcount'] = data.data.wishlist_count;
                        $scope.getWishlistItems();
                        //  $window.location.reload();
                    } else {
                        alert(data.data.status)
                    }
                })
            }
        }

        //reviews
        $scope.reviews = function (reviewData, productDescription) {


            newProductDetailedService.newreviewsMethod(reviewData.reviewRating, reviewData.reviewUserName, reviewData.reviewUserSummery, reviewData.reviewUserMobile, reviewData.reviewUserComment, productDescription, window.localStorage['mobile']).then(function (data) {
                alert(JSON.stringify(data));
                if (data.data.success == 'success') {

                    alert(data.data.success)

                } else {
                    alert(data.data.success)
                }
            });
        }

        $scope.getnewProductDetails = function (productObj) {
            window.localStorage['productName'] = productObj.upload_name;
            $location.path("productDetailPage");
            $route.reload();
        };


        $scope.gotoLogin = function () {
            window.location.href = "./";
        }

    }]);

shopMyToolsApp.controller('customercartController', function ($scope, $rootScope, $window, $location, CartService, StepsService) {
    $scope.cartList = [];
    $rootScope.cartpopup = false;
    $scope.cartimg = false;
    $scope.qty = "1";
    // window.localStorage['random_no']= "";

    $rootScope.B2BDashboard = function () {
        $location.path("/");
    }
    $scope.goToHomeFromCart = function () {
        $location.path("/");
    }

    $scope.addTocart = function (productobj, qty) {
        if ($rootScope.token != '' && $rootScope.token != undefined) {
            $scope.random_no = window.localStorage['random_no'];
            $scope.userId = window.localStorage['user_id'];
        } else {
            if (window.localStorage['random_no'] == undefined) {
                $scope.random_no = "";
            } else {
                $scope.random_no = window.localStorage['random_no'];
            }

            $scope.userId = "";
        }
        $rootScope.product_name = productobj.upload_name;
        // window.localStorage['prodname'] =   $scope.productname;
        if (qty != '' && qty != undefined) {
            $scope.quantity = qty;
        } else {
            $scope.quantity = productobj.qty;
        }

        $scope.cartItemsList = [];
        $scope.cartItemsList.push({ "productdescription": $rootScope.product_name, "qty": $scope.quantity });
        if ($scope.cartArray.length > 0) {
            for (i = 0; i < $scope.cartArray.length; i++) {
                if ($rootScope.product_name != $scope.cartArray[i].upload_name) {
                    $scope.cartItemsList.push({ "productdescription": $scope.cartArray[i].upload_name, "qty": $scope.cartArray[i].qty });
                }
                else if ($rootScope.product_name == $scope.cartArray[i].upload_name) {
                    $rootScope.cartpopup = true;
                }
            }
        }


        // if (window.localStorage['random_no'] == "undefined") {
        //     $scope.random_no = "";
        // }
        // else {
        //     $scope.random_no = window.localStorage['random_no'];
        // }

        CartService.addToCartcustomer($scope.cartItemsList, $scope.random_no, $scope.userId).then(function (data) {
            if (data.data.status == 'item added to cart') {
                window.localStorage['random_no'] = data.data.random_number;
                $("#addedToCart").modal('show');
                $scope.viewcart();
            } else {
                //   alert(data.data.status);

            }
        })
    }

    $scope.viewcart = function () {
        if ($rootScope.token != '' && $rootScope.token != undefined) {
            $scope.random_no = window.localStorage['random_no'];

            $scope.userId = window.localStorage['user_id'];
        } else {
            if (window.localStorage['random_no'] == undefined) {
                $scope.random_no = "";
            } else {
                $scope.random_no = window.localStorage['random_no'];
            }

            $scope.userId = "";
        }
        // if (window.localStorage['random_no'] == "undefined") {
        //     $scope.random_no = "";
        // }
        // else {
        //     $scope.random_no = window.localStorage['random_no'];
        // }
        CartService.viewCartcustomer($scope.random_no, $scope.userId).then(function (data) {
            if (data.data.status == 'Success') {
                $scope.cartArray = data.data.total_item_list;
                $rootScope.cartArrayItems = $scope.cartArray;
                //  $scope.grandtotal = data.data.grand_total;
                $rootScope.totalItems = data.data.total_item_list.length;
                $scope.grandtotal = 0;
                for (i = 0; i < $scope.cartArray.length; i++) {
                    var subtotal = $scope.cartArray[i].mrp * $scope.cartArray[i].qty;
                    var tax_amount = $scope.cartArray[i].tax_amount;
                    $scope.grandtotal = $scope.grandtotal + subtotal + tax_amount;
                }

            } else if (data.data.status == 'cart is empyt.') {
                $scope.cartArray = [];
            }
        })
    }
    $scope.viewcart();

    $scope.getQuantity = function (qty, prodobj) {
        $scope.qty = qty;
        $scope.productname = prodobj.upload_name;
        if ($rootScope.token != '' && $rootScope.token != undefined) {
            $scope.random_no = window.localStorage['random_no'];
            $scope.userId = window.localStorage['user_id'];
        } else {
            $scope.random_no = window.localStorage['random_no'];
            $scope.userId = "";
        }
        CartService.updateviewCartcustomer($scope.productname, $scope.qty, $scope.random_no, $scope.userId).then(function (data) {
            if (data.data.status == 'qty updated successfully') {
                // alert(JSON.stringify(data))
                $scope.viewcart();
                // alert(data.data.status);
            }
        })

    }

    $scope.deletecartItem = function (productobj) {
        if (productobj == "all") {
            $scope.productname = productobj;
        } else {
            $scope.productname = productobj.upload_name;
        }
        if ($rootScope.token != '' && $rootScope.token != undefined) {
            $scope.userId = window.localStorage['user_id'];
            $scope.random_no = "";
        } else {
            $scope.userId = "";
            $scope.random_no = window.localStorage['random_no'];
        }
        CartService.deleteCartItemcust($scope.productname, $scope.userId, $scope.random_no).then(function (data) {
            if (data.data.status == 'product deleted successfully') {

                $scope.viewcart();
                alert(data.data.status);
            }
        })
    }

    $scope.checkout_page = function (cartObj, grandtotal) {
        $rootScope.grandTotal = grandtotal;
        $rootScope.cartorderObj = cartObj;
        if (!(window.localStorage['token'])) {

            $("#loginModel").modal('show');
        } else {
            $location.path("checkout_page");
        }

    }

    $scope.decreaseValue = function (obj) {
        if (obj.qty > 1) {
            $scope.qty--;
            //   $scope.addTocart(obj, $scope.qty);
            $scope.getQuantity($scope.qty, obj)
        }
    }

    $scope.increaseValue = function (obj) {
        $scope.qty++;
        //   $scope.addTocart(qty, $scope.qty);
        $scope.getQuantity($scope.qty, obj)
    }


    $scope.stepData = {};
    $scope.submitFinal = function (age) {
        console.log(age);
        $scope.stepData.age = age;
        console.log($scope.stepData);
    }
        // })
        (window, window.angular);
    /* multi select part end here */

});

