shopMyToolsApp.controller('adminPanelCntrl', function ($scope, adminPanelService, adminPanelGetService, delVerificationPostService, $rootScope, $location, $modal) {
    $scope.selectedType = "delhi";
    $scope.adminRegData = {};
    $scope.inputType = 'password';
    $scope.branch_code = window.localStorage['branch_code'];
    $scope.branch_name = window.localStorage['branch_name'];
    $scope.currentPage = 1;
    $scope.itemsPerPage = 10;
    if (window.localStorage['token'] == undefined) {
        $location.path('/');
    }
    $scope.toggleShowPassword = function () {
        if ($scope.inputType == 'password') {
            $scope.inputType = 'text';
        }
        else {
            $scope.inputType = 'password';
        }
    };
    $scope.adminRegData.branch_code = $scope.branch_code;

    $scope.B2BDashboard = function () {
        $location.path('Dashboard');
    }


    // $scope.adminPanelData = function (adminregdata) {
    //     $rootScope.adminPanelregdata = adminregdata;

    //     adminPanelService.adminPanelAuthentication(adminregdata).then(function (data) {

    //         if (data.data.status == 'Success') {
    //             $scope.displayMsg = "User Account Created Successfully!";
    //             //alert(data.data.data);
    //             $scope.modalInstance = $modal.open({
    //                 animation: true,
    //                 ariaLabelledBy: 'modal-title',
    //                 ariaDescribedBy: 'modal-body',
    //                 scope: $scope,
    //                 templateUrl: 'views/model_popup.html',
    //                 controller: function ($scope) {
    //                     $scope.close = function () {
    //                         $scope.modalInstance.dismiss('close');
    //                     }
    //                 }
    //             });
    //             $scope.internaluserGetDetails();
    //         }
    //     })

    //     $location.path('Dashboard');
    // }

    $scope.selectUsertype = function (type) {
        $scope.usertype = type;
        $scope.adminRegData.branch_code = "";
    }

    $scope.selectAccountant = function (obj) {
        $scope.adminRegData.acuser_mail = obj.user_mail;

    }
    $scope.selectWh = function (obj) {
        $scope.adminRegData.whuser_mail = obj.user_mail;
    }
    $scope.selectBranch = function (obj) {
        $scope.branch = obj;
        $scope.adminRegData.branch = obj.branch;
        $scope.adminRegData.branch_code = obj.branch_code;
    }
    $scope.getdealerType = function (type) {
        $scope.branchType = type;
    }



    $scope.adminPanelData = function () {
        adminPanelService.adminPanelAuthentication($scope.adminRegData).then(function (data) {
            if (data.data.status == 'Success') {
                $scope.formName.$setPristine();
                $scope.displayMsg = "User Account Created Successfully!";
                $scope.modalInstance = $modal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    scope: $scope,
                    templateUrl: 'views/model_popup.html',
                    controller: function ($scope) {
                        $scope.close = function () {
                            $scope.modalInstance.dismiss('close');
                        }
                    }
                });
                $scope.adminRegData = {};
                        
                $scope.internaluserGetDetails();
            }
        })

    }


    $scope.getBranch = function (branch_code) {
        $scope.branch_code = branch_code;
        $scope.getDealers($scope.branch_code);
    }

    $scope.getDealers = function (branch_code) {
        adminPanelService.getDealers(branch_code).then(function (data) {
            //  alert(JSON.stringify(data))
            if (data.data.status == 'success') {
                $scope.branch_dealers = data.data.dealers;
                $scope.sub_dealers = data.data.sub_dealers;
            }

        })
    }


    $scope.internaluserGetDetails = function () {
        adminPanelGetService.adminPanelGetAuthentication().then(function (data) {
            //  alert(JSON.stringify(data))
            if (data.data.status == 'Success') {
                $scope.internalUserDetails = data.data.Users_info;
                $scope.totalItems = $scope.internalUserDetails.length;
            }

        })
    }
    $scope.internaluserGetDetails();

    $scope.removeinternaluser = function (internalUser, index) {
        $scope.user_id = internalUser._id.$oid
        adminPanelGetService.deleteuserdetails($scope.user_id).then(function (data) {
            //alert(JSON.stringify(data))
            if (data.data.status == 'success') {
                alert(data.data.message);
                $scope.internaluserGetDetails();
            }
        })
    }

    $scope.sm_Assignment = function () {
        adminPanelGetService.sMasigneeMethod($scope.branch_code).then(function (data) {
            //  alert(JSON.stringify(data))
            if (data.data.status == 'success') {
                $scope.acList = data.data.acc_data;
                $scope.whList = data.data.wh_data;
                $scope.branchList = data.data.branches;
            }
        })
    }
    $scope.sm_Assignment();


    $scope.asigned_Mangers = function (obj) {
        $scope.sales_email = obj.user_mail;
        $scope.ac_Manager = obj.accounts;
        $scope.wh_Manager = obj.wh;

        $scope.modalInstance1 = $modal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            scope: $scope,
            templateUrl: 'views/intrnalUserModal.html',
            controller: function ($scope) {
                $scope.close = function () {
                    $scope.modalInstance1.dismiss('close');
                }
            }
        });
    };





    $scope.acceptreg = function (acceptData, dealerUser) {

        delVerificationPostService.delVerificationPostAuthentication(dealerUser.dealer_mail, $scope.branch, $scope.branchType, acceptData).then(function (data) {
            if (data.data.status == 'Success') {
            } else {
                alert(data.data.status);
            }
        })
    }

    $scope.getsmVal = function (data) {
        $scope.sm_name = data.username;
        $scope.sm_mobile = data.user_mobile;
    }
    $scope.Selectcategory = function (data) {
        $scope.category = data;
    }


    $scope.updateAccManager = function (data) {
        $scope.acc_email = data.user_mail;
    }

    $scope.updateWhManager = function (data) {
        $scope.wh_email = data.user_mail;
    }

    $scope.acc_email = "";
    $scope.wh_email = "";

    $scope.updateManager = function () {

        if (($scope.acc_email == "") && ($scope.wh_email == "")) {
            alert('Please Select');
        } else {
            adminPanelService.updateManagerForSalaes($scope.sales_email, $scope.acc_email, $scope.wh_email).then(function (data) {
                if (data.data.status == 'Success') {
                    alert(data.data.status);
                    $scope.modalInstance1.dismiss('close');
                    $scope.internaluserGetDetails();
                }
                else {
                    alert(data.data.status);
                }
            })
        }

    }

    $scope.uploadFile = function () {
        var file = $scope.excel;
        adminPanelService.masterDataExcelupload(file).then(function (data) {
            if (data.data.status == 'data saved successfuly.') {
                alert('your file uploaded successfully');
            }
            else {
                alert('Something went wrong while file uploading');
            }
        })

    }

    $scope.AttruploadFile = function () {
        var file = $scope.attrexcel;
        adminPanelService.masterAttrDataupload(file).then(function (data) {
            if (data.data.status == 'data saved successfuly.') {
                alert('your file uploaded successfully');
            }
            else {
                alert('Something went wrong while file uploading');
            }
        })

    }

    $scope.masterdatadownload = function (datatype) {

        adminPanelService.masterdatadownload(datatype).then(function (data) {
            if (data.data.status == 'success') {
                $scope.data = JSON.parse(data.data.data);
                $scope.headers = data.data.headers;
                $scope.filename = data.data.file_name;
                $scope.sheet_name = data.data.sheet_name;

            }

            // var myJsonString = JSON.stringify($scope.data);
            // var blob = new Blob([myJsonString], {
            //     type: "application/vnd.ms-excel;charset=charset=utf-8"
            // });
            // saveAs(blob, "Report.xls");

            $(function () {


                var createXLSLFormatObj = [];

                /* XLS Head Columns */
                // var xlsHeader = ["upload_id", "upload_category","upload_subcategory","upload_name","upload_modelno","upload_brand","MRP","upload_warranty","upload_pieceperCarton","STD_non_pack","upload_minimumOrder","upload_netWeight","upload_mrp","upload_price","discount","upload_discount","tax","upload_tax","upload_netPrice","upload_hsncode","upload_locations","frequency","remarks","salesqty","description","avgrating","manual_video","manual_pdf","dealer_a_disc","dealer_b_disc","dealer_c_disc","dealer_d_disc","dealer_e_disc","end_user_disc","vendor_code","volume_offer_code","wh_qty"];
                var xlsHeader = $scope.headers;
                /* XLS Rows Data */
                var xlsRows = $scope.data;


                createXLSLFormatObj.push(xlsHeader);
                $.each(xlsRows, function (index, value) {
                    var innerRowData = [];
                    // $("tbody").append('<tr><td>' + value.EmployeeID + '</td><td>' + value.FullName + '</td></tr>');
                    $.each(value, function (ind, val) {

                        innerRowData.push(val);
                    });
                    createXLSLFormatObj.push(innerRowData);
                });


                /* File Name */
                var filename = $scope.filename;

                /* Sheet Name */
                var ws_name = $scope.sheet_name;

                if (typeof console !== 'undefined') console.log(new Date());
                var wb = XLSX.utils.book_new(),
                    ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

                /* Add worksheet to workbook */
                XLSX.utils.book_append_sheet(wb, ws, ws_name);

                /* Write workbook and Download */
                if (typeof console !== 'undefined') console.log(new Date());
                XLSX.writeFile(wb, filename);
                if (typeof console !== 'undefined') console.log(new Date());

            });
        })

    }


    //  $scope.masterdatadownload();

    // $scope.uploadedHistoryData = function () {
    //     adminPanelService.masteruploadedHistoryData().then(function (data) {
    //         if (data.data.status == 'Success') {
    //             $scope.productinfo = data.data.product_info;
    //         }
    //     })
    // }
    // $scope.uploadedHistoryData();


    $scope.productAllInformation = function (item, index) {
        $rootScope.productList = item;
        $("#productAllInformation").modal('show');
    }
    $scope.dealer_details = function (dealerobj) {
        $rootScope.dealerdata = {};
        $rootScope.dealerdata.billing_addr = dealerobj.billing_address;
        $rootScope.dealerdata.shipping_addr = dealerobj.shipping_address;
        $rootScope.dealerdata.dealer_city = dealerobj.dealer_city;
        $rootScope.dealerdata.turn_over = dealerobj.turn_over;
        $rootScope.dealerdata.pan = dealerobj.pan;
        $rootScope.dealerdata.dealer_gstin = dealerobj.dealer_gstin;
        $rootScope.dealerdata.dealer_phone = dealerobj.dealer_phone;
        $rootScope.dealerdata.dealer_mail = dealerobj.dealer_mail;
        $rootScope.dealerdata.year_exp_field = dealerobj.year_exp_field;
        $rootScope.dealerdata.adhar_num = dealerobj.adhar_num;
        $rootScope.dealerdata.dealer_mobile = dealerobj.dealer_mobile;
        $rootScope.dealerdata.dealer_name = dealerobj.dealer_name;
        $rootScope.dealerdata.adhar_num = dealerobj.adhar_num;

        $("#dealer_shop_details").modal('show');
    }



});


shopMyToolsApp.controller('adminPanelCompanyProfileCntrl', function ($scope, adminPanelService, dealerVerificationService, $location) {

    $scope.companyProfile = {};
    if (window.localStorage['token'] == undefined) {
        $location.path('/');
    }
    $scope.dealerVerificationGetDetails = function () {
        dealerVerificationService.dealerVerificationAuthentication().then(function (data) {
            //alert(JSON.stringify(data))
            if (data.data.status == 'Success') {
                $scope.delVerifyDetails = data.data.Dealers_info;
                $scope.branches = data.data.branches;
                $scope.categorylist = data.data.categories;
            }
        })
    }
    $scope.dealerVerificationGetDetails();


    $scope.getDealers = function (data) {
        $scope.branch_code = data.branch_code;
        adminPanelService.getDealers($scope.branch_code).then(function (data) {
            //  alert(JSON.stringify(data))
            if (data.data.status == 'success') {
                $scope.branch_dealers = data.data.dealers;
                $scope.sub_dealers = data.data.sub_dealers;
            }

        })
    };


    $scope.CompanyProfile = function (obj) {

        $scope.company_profile = {
            "shopee_code": obj.shpcode,
            "shopee_name": obj.shpname,
            "display_name": obj.displayname,
            "shopee_type": obj.shptype,
            "pan": obj.pan,
            "gst_num": obj.gst,
            "company_address": obj.address,
            "shope_city": obj.city,
            "shope_state": obj.state,
            "shope_country": obj.country,
            "shope_pincode": obj.pincode,
            "branch_code": obj.branch_code.branch_code,
            "dealer_code": obj.dealer_code,
            "subdealer_code": obj.sub_dealercode
        };
        console.log($scope.company_profile);
        // adminPanelService.companyProfileService($scope.company_profile).then(function (data) {
        //     if (data.data.status == 'success') {
        //         alert("successfully updated company details");
        //     }
        //     $scope.company_profile = {};
        // })
    };

    $scope.B2BDashboard = function () {
        $location.path('Dashboard');
    };

});


shopMyToolsApp.controller('adminPanelDealerVeificationCntrl', function ($scope, adminPanelService, dealerVerificationService, delVerificationPostService, $location) {

    $scope.B2BDashboard = function () {
        $location.path('Dashboard');
    };
    if (window.localStorage['token'] == undefined) {
        $location.path('/');
    }
    $scope.branch_code = window.localStorage['branch_code'];
    $scope.dealer_details = {};
    $scope.dealer_statustt = {};

    $scope.getDealersForSmAssignment = function () {
        adminPanelService.getDealersForSmAssignment($scope.branch_code).then(function (data) {
            if (data.data.status == 'success') {
                $scope.delVerifyDetails = data.data.dealerinfo;
                $scope.categorylist = data.data.categories;
                $scope.salesManagers_list = data.data.SalesManager_info;
                $scope.dealer_codes = data.data.dealercode;
                $scope.subdealer_codes = data.data.subdealercode;
            }
        })
    }
    $scope.getDealersForSmAssignment();

    $scope.getDealerCode = function (dealer_code) {
        $scope.dealer_details.dealer_code = dealer_code;
    }

    $scope.getSubdealerCode = function (subdealer_code) {
        $scope.dealer_details.subdealer_code = subdealer_code;
    }

    $scope.getCategory = function (category) {
        $scope.dealer_details.dealer_category = category;
    }

    $scope.getSM = function (salesManager) {
        $scope.dealer_details.sales_contact_person = salesManager.user_mail;
        $scope.dealer_details.sales_contact_mobile = salesManager.user_mobile;
    }

    $scope.acceptreg = function (status, dealerUser) {
        // alert($scope.dealer_statustt.status);
        $scope.dealer_details.dealer_status = status;
        $scope.dealer_details.dealer_mail = dealerUser.dealer_mail;

        if ($scope.dealer_details.dealer_code == undefined && $scope.dealer_details.dealer_category == undefined && $scope.dealer_details.sales_contact_person == undefined) {
            $scope.dealer_statustt.status = '';
            alert('Please Select an Option');
        } else if ($scope.dealer_details.dealer_code != undefined && $scope.dealer_details.dealer_category != undefined && $scope.dealer_details.sales_contact_person != undefined) {
            delVerificationPostService.dealerActivation($scope.dealer_details).then(function (data) {
                if (data.data.status == 'success') {
                    alert(data.data.status);
                } else {
                    alert(data.data.status);
                };
                $scope.dealer_details = {};
                $scope.dealer_statustt.status = '';
                $scope.getDealersForSmAssignment();
            });

        } else {
            $scope.dealer_statustt.status = '';
            alert('Please Select an Option');
        }

    }

});


shopMyToolsApp.controller('adminPanelDealerHistoryCntrl', function ($scope, dealerVerificationService, $location, $modal) {

    $scope.B2BDashboard = function () {
        $location.path('Dashboard');
    };
    if (window.localStorage['token'] == undefined) {
        $location.path('/');
    }

    $scope.del_history = function () {
        dealerVerificationService.dealerHistory().then(function (data) {
            if (data.data.status == 'success') {
                $scope.delr_history = data.data.Dealer_history;
                $scope.data = $scope.delr_history;
                $scope.viewby = 10;
                $scope.totalItems = $scope.data.length;
                $scope.currentPage = 1;
                $scope.itemsPerPage = $scope.viewby;
                window.localStorage['user_id'] = data.data.User_id;
                for (i = 0; i < $scope.delr_history.length; i++) {
                    if (data.data.Dealer_history[i].dealer_status == "Active") {
                        $scope.acept_dealer = true;
                    }
                }
            } else {
                alert(data.data.status);
            }
        })
    }
    $scope.del_history();

    $scope.dealer_info = function (data) {
        $scope.delr_info = data;
        // $("#dealers_Information").modal('show');
        var modalInstance = $modal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            scope: $scope,
            templateUrl: 'views/dealer_more_info_Modal.html',
            controller: function ($scope) {
                $scope.close = function () {
                    modalInstance.dismiss('close');
                }
            }
        });
    }

});

shopMyToolsApp.controller('adminPanelDealerSegerigationCntrl', function ($scope, dealerVerificationService, delVerificationPostService, $location) {

    $scope.branch_code = window.localStorage['branch_code'];
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.verifaction_details = {};
    if (window.localStorage['token'] == undefined) {
        $location.path('/');
    }

    $scope.gotoPage = function (val) {
        if (val == '1') {
            $location.path('dealerSegregation');
        } else if (val == '2') {
            $location.path('dealerShopInfo');
        } else if (val == '3') {
            $location.path('shopeeCreation');
        }
    }

    $scope.B2BDashboard = function () {
        $location.path('Dashboard');
    };

    $scope.dealerVerificationGetDetails = function () {
        dealerVerificationService.dealerVerificationAuthentication().then(function (data) {
            if (data.data.status == 'Success') {
                $scope.delVerifyDetails = data.data.Dealers_info;
                $scope.branches = data.data.branches;
                $scope.categorylist = data.data.categories;
                $scope.salesManagers_list = data.data.SalesManager_info;
                $scope.totalItems = $scope.delVerifyDetails.length;

            }
        })
    }
    $scope.dealerVerificationGetDetails();

    //  alert($scope.verifaction_details.status);
    $scope.moveToBranch = function (status, dealerUser) {

        $scope.user_mail = dealerUser.dealer_mail;
        $scope.usertype = dealerUser.user_type;
        $scope.status = status;

        if (status == 'Accept') {
            if ($scope.select_branch_code != '' && $scope.select_branch_code == undefined) {
                $scope.verifaction_details.status = '';
                alert('Please Assign Branch');
            } else {
                $scope.CallMoveBranch();
            }
        } else {
            $scope.select_branch_code = $scope.branch_code;
            $scope.branch = "Hyderabad";
            $scope.CallMoveBranch();
        };

    };


    $scope.CallMoveBranch = function () {
        delVerificationPostService.delVerificationPostAuthentication($scope.user_mail, $scope.usertype, $scope.branch, $scope.select_branch_code, $scope.status).then(function (data) {
            if (data.data.status == 'Accepted') {
                alert(data.data.status);
            } else {
                alert(data.data.status);
            };
            $scope.verifaction_details = {};
            $scope.dealerVerificationGetDetails();
        })

    }



    $scope.getBranch = function (data) {
        $scope.branch = data.branch;
        $scope.select_branch_code = data.branch_code;
    };

});

shopMyToolsApp.controller('adminPanelDealerInfoCntrl', function ($scope, dealerVerificationService, $location, $modal) {

    $scope.dealerxdata = {};
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    if (window.localStorage['token'] == undefined) {
        $location.path('/');
    }

    //$scope.branch_code = "0000";
    $scope.branch_code = window.localStorage['branch_code'];

    $scope.gotoPage = function (val) {
        if (val == '1') {
            $location.path('dealerSegregation');
        } else if (val == '2') {
            $location.path('dealerShopInfo');
        } else if (val == '3') {
            $location.path('shopeeCreation');
        }
    }

    $scope.B2BDashboard = function () {
        $location.path('Dashboard');
    };

    $scope.activeDealersInfo = function () {
        dealerVerificationService.activeDealersInfo($scope.branch_code).then(function (data) {
            if (data.data.status == 'success') {
                $scope.delVerifyDetails = data.data.dealer_info;
                $scope.totalItems = $scope.delVerifyDetails.length;
            }
        })
    }
    $scope.activeDealersInfo();


    $scope.showDealeInfo = function (dealerData) {
        $scope.user_mail = dealerData.dealer_mail;
        // $("#dealers_Information").modal('show');

        var modalInstance = $modal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            scope: $scope,
            templateUrl: 'views/dealerShopInfoModal.html',
            controller: function ($scope) {
                $scope.close = function () {
                    modalInstance.dismiss('close');
                }
                $scope.submit = function () {
                    modalInstance.dismiss('close');
                    $scope.dealerActiveMethod();
                }
            }
        });

    };

    $scope.dealerActiveMethod = function () {

        $scope.billing_addr = [{
            "d_no": $scope.dealerxdata.shipping_dno,
            "street": $scope.dealerxdata.shipping_street,
            "city": $scope.dealerxdata.shipping_city,
            "state": $scope.dealerxdata.shipping_state,
            "zip_code": $scope.dealerxdata.shipping_zip
        }];

        $scope.shipping_addr = [{
            "d_no": $scope.dealerxdata.billing_dno,
            "street": $scope.dealerxdata.billing_street,
            "city": $scope.dealerxdata.billing_city,
            "state": $scope.dealerxdata.billing_state,
            "zip_code": $scope.dealerxdata.billing_zip
        }];

        dealerVerificationService.dealerActiveMethod($scope.dealerxdata, $scope.user_mail, $scope.billing_addr, $scope.shipping_addr).then(function (data) {
            if (data.data.status == 'Success') {
                alert(data.data.dealer_status);
                $scope.dealerxdata = {};
            } else {
                alert(data.data.dealer_status);
            }
            $scope.activeDealersInfo();
        })
    }

});


shopMyToolsApp.controller('adminPanelDealerShopeeCntrl', function ($scope, dealerVerificationService, adminPanelService, $location, $modal) {

    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.companyProfile = {};
    $scope.companyBranchDetails = {};
    if (window.localStorage['token'] == undefined) {
        $location.path('/');
    }


    $scope.gotoPage = function (val) {
        if (val == '1') {
            $location.path('dealerSegregation');
        } else if (val == '2') {
            $location.path('dealerShopInfo');
        } else if (val == '3') {
            $location.path('shopeeCreation');
        }
    }

    $scope.B2BDashboard = function () {
        $location.path('Dashboard');
    };

    $scope.getProcessDealersForShopee = function () {
        dealerVerificationService.getProcessDealersForShopee().then(function (data) {
            if (data.data.status == 'Success') {
                $scope.dealer_data = data.data.dealer_data;
                $scope.totalItems = $scope.dealer_data.length;
            }
        })
    };
    $scope.getProcessDealersForShopee();

    $scope.createShopee = function (data) {
        $scope.dealerData = data;
        $scope.user_mail = data.dealer_mail;
        $scope.companyProfile.user_mail = $scope.user_mail;
        // $("#create_shopee").modal('show');

        var modalInstance = $modal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            scope: $scope,
            templateUrl: 'views/create_shopee_info.html',
            controller: function ($scope) {
                $scope.close = function () {
                    modalInstance.dismiss('close');
                }
            }
        });

    };

    $scope.dealerVerificationGetDetails = function () {
        dealerVerificationService.dealerVerificationAuthentication().then(function (data) {
            if (data.data.status == 'Success') {
                $scope.branches = data.data.branches;
            }
        })
    }
    //  $scope.dealerVerificationGetDetails();

    $scope.getBranch = function (branch_data) {
        $scope.branch_code = branch_data.branch_code;
        $scope.companyProfile.branch_code = $scope.branch_code;
        $scope.getDealers($scope.branch_code);
    }

    $scope.getDealers = function (branch_code) {
        adminPanelService.getDealers(branch_code).then(function (data) {
            if (data.data.status == 'success') {
                $scope.branch_dealers = data.data.dealers;
                $scope.sub_dealers = data.data.sub_dealers;
            }
        })

    };

    $scope.getDealerCode = function (dealer_data) {
        $scope.dealer_code = dealer_data.dealer_code;
        $scope.companyProfile.dealer_code = $scope.dealer_code;
    }

    $scope.shopee_type = function () {
        //   $scope.companyProfile = {};
    };

    $scope.createShopeeSubmit = function (obj) {
        console.log($scope.companyProfile);
        adminPanelService.companyProfileService($scope.companyProfile).then(function (data) {
            if (data.data.status == 'success') {
                alert("successfully updated company details");
            }
            $("#create_shopee").modal('hide');
            $scope.companyProfile = {};
            $scope.getProcessDealersForShopee();
        });

    };



});

shopMyToolsApp.controller('adminPanelActiveDealersInfoCntrl', function ($scope, dealerVerificationService, adminPanelService, $location, $modal) {

    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.enablebtn = true;
    $scope.branch_code = window.localStorage['branch_code'];

    if (window.localStorage['token'] == undefined) {
        $location.path('/');
    }


    $scope.B2BDashboard = function () {
        $location.path('Dashboard');
    };

    $scope.getActiveDealersInfo = function () {
        dealerVerificationService.getActiveDealersInfo($scope.branch_code).then(function (data) {
            if (data.data.status == 'success') {
                $scope.dealer_data = data.data.Dealer_history;
                $scope.totalItems = $scope.dealer_data.length;
            }
        })
    };
    $scope.getActiveDealersInfo();

    $scope.dealer_info = function (data) {
        $scope.showData = true;
        $scope.delr_info = data;
        $scope.dealer_mail = data.dealer_mail;
        $scope.modalInstance = $modal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            scope: $scope,
            templateUrl: 'views/dealer_more_info_Modal.html',
            controller: function ($scope) {
                $scope.close = function () {
                    $scope.modalInstance.dismiss('close');
                }
            }
        });
    };

    $scope.getDealersForSmAssignment = function () {
        adminPanelService.getDealersForSmAssignment($scope.branch_code).then(function (data) {
            if (data.data.status == 'success') {
                $scope.delVerifyDetails = data.data.dealerinfo;
                $scope.categorylist = data.data.categories;
                $scope.salesManagers_list = data.data.SalesManager_info;
                $scope.dealer_codes = data.data.dealercode;
                $scope.subdealer_codes = data.data.subdealercode;
            }
        })
    }
    $scope.getDealersForSmAssignment();

    $scope.getSalesManager = function (data) {
        if (data == undefined) {
            $scope.enablebtn = true;
        } else {
            $scope.enablebtn = false;
            $scope.sales_mail = data.user_mail;
            $scope.sales_mobile = data.user_mobile;
        }
    };

    //  $scope.getSalesManager = function (data) {
    //    //  $scope.enablebtn = false;
    //         $scope.sales_mail = data.user_mail;
    //         $scope.sales_mobile = data.user_mobile;

    // };

    $scope.updateSalesManager = function () {
        dealerVerificationService.getActiveDealersSMupdate($scope.dealer_mail, $scope.sales_mail, $scope.sales_mobile).then(function (data) {
            if (data.data.status == 'SM updated successfully.') {
                alert(data.data.status);
                $scope.modalInstance.dismiss('close');
                $scope.getActiveDealersInfo();
            }
        })
    };

});
