//var app = angular.module("smtApp", []);

shopMyToolsApp.controller('distributorController', ['$scope', '$location',
    '$rootScope', 'distributorService', '$filter', '$modal', function ($scope, $location, $rootScope, distributorService, $filter, $modal) {


        $scope.dealer = false;
        $scope.purchase = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        $rootScope.user_type = window.localStorage['usertype'];

        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "Purchase-Manager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.purchase = true;
        }

        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }


        // alert($scope.user_type);
        $scope.dealerDashbord = function () {
            $location.path("Dashboard");
        }
        $scope.rfqQuote = function () {
            $location.path("rfqValidationpage")
        }
        $scope.distributor = function () {
            $location.path("createRFQ")
        }
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        $scope.getdistributorcatdata = function () {
            distributorService.getdistributortcat().then(function (data) {
                if (data.data.status == 'Success') {
                    $scope.CategoryArray = data.data.categories;
                    //  $scope.SubCategoryArray = data.data.subcategories;
                    // $scope.brandsArray = data.data.brands;
                }

            })
        }
        $scope.getdistributorcatdata();

        $scope.multiCategoriesList = [];

        $scope.allCatigory = {};
        $scope.checkAllCategories = function (allselected) {
            if (allselected) {
                $scope.selectedAll = true;
            } else {
                $scope.selectedAll = false;
                $scope.multiCategoriesList = [];
            }
            angular.forEach($scope.CategoryArray, function (item) {
                item.Checked = $scope.selectedAll;
                if ($scope.selectedAll) {
                    $scope.multiCategoriesList.push(item.categoryname);
                }
            });
            if (allselected) {
                $scope.getdistributorSubcatdata();
            }
        };


        $scope.getcategory = function (category, checkvalue) {
            var catIndex = $scope.multiCategoriesList.indexOf(category.categoryname)
            if (checkvalue) {
                $scope.multiCategoriesList.push(category.categoryname);
            } else {
                for (i = 0; i <= $scope.multiCategoriesList.length; i++) {
                    if ($scope.multiCategoriesList[i] == category.categoryname) {
                        $scope.multiCategoriesList.splice(catIndex, 1)
                    }
                }
            }
            $scope.getdistributorSubcatdata();
        }

        $scope.getdistributorSubcatdata = function () {
            distributorService.getdistributortSubcat($scope.multiCategoriesList).then(function (data) {
                if (data.data.status == 'Success') {
                    $scope.SubCategoryArray = data.data.subcategories;
                    $scope.brandsArray = data.data.brands;

                }

            })

        }

        $scope.allSubCatigory = {};
        $scope.multiSubcatList = [];

        $scope.checkAllSubCategories = function (allselected) {
            $scope.categorylist = false;
            if (allselected) {
                $scope.selectedAllSub = true;
            } else {
                $scope.selectedAllSub = false;
                $scope.multiSubcatList = [];
            }
            angular.forEach($scope.SubCategoryArray, function (item) {
                item.Checked = $scope.selectedAllSub;
                if ($scope.selectedAllSub) {
                    $scope.multiSubcatList.push(item.subcategory);
                }
            });
            if (allselected) {
                $scope.getdistributorbranddata();
            }
        };

        $scope.getsubcategory = function (subcategry, checkvalue) {
            $scope.categorylist = false;
            var subcatIndex = $scope.multiSubcatList.indexOf(subcategry.subcategory)
            if (checkvalue) {
                $scope.multiSubcatList.push(subcategry.subcategory);
            } else {
                for (i = 0; i <= $scope.multiSubcatList.length; i++) {
                    if ($scope.multiSubcatList[i] == subcategry.subcategory) {
                        $scope.multiSubcatList.splice(subcatIndex, 1)
                    }
                }
            }
            $scope.getdistributorbranddata();
        }


        $scope.getdistributorbranddata = function () {
            distributorService.getdistributorbrand($scope.multiSubcatList).then(function (data) {
                if (data.data.status == 'Success') {
                    $scope.brandsArray = data.data.brands;
                    $scope.brands = data.data.brands.brand;
                }
            })
        }


        $rootScope.multibrandList = [];

        $scope.getbrand = function (brandData, checkvalue) {
            $scope.subcategoryList = false;
            var brandIndex = $scope.multibrandList.indexOf(brandData.brand)
            if (checkvalue) {
                $scope.multibrandList.push(brandData.brand);
            } else {
                for (i = 0; i <= $scope.multibrandList.length; i++) {
                    if ($scope.multibrandList[i] == brandData.brand) {
                        $scope.multibrandList.splice(brandIndex, 1)
                    }
                }
            }
        }

        $scope.getVendorsList = function () {
            distributorService.getVendorsList().then(function (data) {
                if (data.data.status == 'success') {
                    $scope.vendorsList = data.data.vendor_list;
                }
            })
        }
        if ($scope.purchase) {
            $scope.getVendorsList();
        }


        $scope.multiVendorList = [];

        $scope.getVendor = function (vendorData, checkvalue) {
            $scope.brandList = false;
            var vendorIndex = $scope.multiVendorList.indexOf(vendorData.Id)
            if (checkvalue) {
                $scope.multiVendorList.push(vendorData.Id);
            } else {
                for (i = 0; i <= $scope.multiVendorList.length; i++) {
                    if ($scope.multiVendorList[i] == vendorData.Id) {
                        $scope.multiVendorList.splice(vendorIndex, 1)
                    }
                }
            }
        }

        $scope.getdistributorcatgetdata = function () {
            $scope.brandList = false;
            $scope.categorylist = false;
            $scope.subcategoryList = false;
            $scope.vendorList = false;

            $scope.loading = true;
            distributorService.getdistributortcatget($scope.multiCategoriesList, $scope.multiSubcatList, $scope.multibrandList, $scope.multiVendorList, window.localStorage['user_id']).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'Success') {
                    $scope.getCategoryData = data.data.product_data;
                    $rootScope.data = $scope.getCategoryData;
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                }
            })
        }


        $scope.getdistributorcatgetdata("");
        $scope.items = [];
        $scope.decreaseValue = function (obj, index, currentPage) {

            if (currentPage <= 1) {
                $scope.index = index;
            } else {
                $scope.index = (currentPage - 1) * 10 + index;
            }

            $scope.qty = JSON.parse(obj.qty)
            if (obj.qty > 0) {
                $scope.qty = JSON.parse(obj.qty)
                $scope.qty -= JSON.parse(obj.pieceperCarton);
                obj.qty = JSON.stringify($scope.qty);

                $scope.quantity = obj.qty;
                $scope.prodname = obj.upload_name;
                if ($scope.quantity >= 0) {
                    $scope.getdistributorVoldiscount($scope.quantity, $scope.prodname, $scope.index);
                }
            }
        }

        $scope.increaseValue = function (obj, index, currentPage) {

            if (currentPage <= 1) {
                $scope.index = index;
            } else {
                $scope.index = (currentPage - 1) * 10 + index;
            }

            $scope.qty = JSON.parse(obj.qty)
            $scope.qty += JSON.parse(obj.pieceperCarton);
            obj.qty = JSON.stringify($scope.qty);

            $scope.quantity = obj.qty;
            $scope.prodname = obj.upload_name;
            if ($scope.quantity >= 1) {
                $scope.getdistributorVoldiscount($scope.quantity, $scope.prodname, $scope.index);
            }


        }

        $scope.getQuantity = function (qty, productName, index, currentPage) {

            if (currentPage <= 1) {
                $scope.index = index;
            } else {
                $scope.index = (currentPage - 1) * 10 + index;
            }

            $scope.quantity = qty;
            $scope.prodname = productName;
            if ($scope.quantity >= 1) {
                $scope.getdistributorVoldiscount($scope.quantity, $scope.prodname, $scope.index);
            }

        }



        $scope.getdistributorVoldiscount = function (quantity, prodname, index) {
            distributorService.getvolumediscount(quantity, prodname,$scope.userid).then(function (data) {
                if (data.data.status == 'Success') {
                    $rootScope.voldiscount = data.data.Percentage;
                    $rootScope.voldiscpercentage = data.data.discount;
                    $scope.spclDiscountList = data.data.product_info;
                    $scope.getCategoryData[index].special_discount = $scope.spclDiscountList[0].special_disc;
                    $scope.getCategoryData[index].volume_discount = $scope.spclDiscountList[0].vol_Percentage;
                    $scope.getCategoryData[index].total = $scope.spclDiscountList[0].total;
                    $scope.getCategoryData[index].saving_percentage = $scope.spclDiscountList[0].saving_percentage;

                }

            })
        }

        $scope.addToCartIndividual = function (obj) {
            $scope.random_no = '';

            if (obj.qty > 0) {
                $scope.addToCartItems([obj]);
            } else {
                alert('Please select Item Quantity');
            }

        }
        $scope.addToCartItems = function (obj) {
            $scope.loading = true;
            distributorService.addToCartmethod(obj, window.localStorage['user_id'], $scope.random_no).then(function (data) {
                $scope.loading = false;
                $scope.displayMsg = "Added to Cart Successfully!";
                if (data.data.status == 'item added to cart') {
                    //$("#successTable").modal('show');
                    var modalInstance = $modal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        scope: $scope,
                        templateUrl: 'views/model_popup.html',
                        controller: function ($scope) {
                            $scope.close = function () {
                                modalInstance.dismiss('close');
                            }
                        }
                    });

                    $rootScope.totalItems = data.data.cart_count;
                }
                else {
                    alert(data.data.status)
                };
                $scope.getdistributorcatgetdata("");
            })
        }


        $scope.addToCartAll = function (obj) {
            $scope.carItemsList = [];
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].qty > 0) {
                    $scope.carItemsList.push(obj[i]);
                }
            }
            if ($scope.carItemsList.length > 0) {
                $scope.addToCartItems($scope.carItemsList);

            } else {
                alert('Please select Items Quantity');
            }
        }

        $scope.specificationsTable = function (item) {
            $rootScope.product_id = item.product_id;
            // $("#specificationsTable").modal('show');

            $scope.ModalInfo = item;


            var modalInstance = $modal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                scope: $scope,
                templateUrl: 'views/productSpecificationModal.html',
                controller: function ($scope) {
                    $scope.close = function () {
                        modalInstance.dismiss('close');
                    }
                }
            });

        }

        $rootScope.B2BDashboard = function () {
            $location.path("Dashboard");
        };
        $scope.gotoViewCart = function () {
            $location.path("dealerViewCart");
        };

    }]);




shopMyToolsApp.controller('viewCartItemsController', ['$scope', '$rootScope', 'distributorService', '$location', '$modal',
    function ($scope, $rootScope, distributorService, $location, $modal) {

        $scope.dealer = false;
        $scope.purchase = false;
        $scope.random_no = '';
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }

        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "Purchase-Manager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.purchase = true;
        }



        $scope.viewcartData = function () {
            $scope.loading = true;
            distributorService.addedCartView(window.localStorage['user_id'], $scope.random_no).then(function (data) {
                if (data.data.status == 'Success') {
                    $scope.loading = false;
                    $scope.cartItems = data.data.total_item_list;
                    $scope.grandTotal = data.data.total_price;
                    $rootScope.totalItems = data.data.total_item_list.length;
                    $scope.data = $scope.cartItems;
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                } else if (data.data.status == 'cart is empyt.') {
                    $scope.cartItems = [];
                    $scope.data = $scope.cartItems;
                    $rootScope.totalItems = '';
                }
                else {
                    alert(data.data.status)
                }
            })
        }
        $scope.viewcartData();

        $scope.specificationsTable = function (item) {
            $scope.product_id = item.product_id;
            $scope.ModalInfo = item;

            //   $("#specificationsTable").modal('show');
            var modalInstance = $modal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                scope: $scope,
                templateUrl: 'views/productSpecificationModal.html',
                controller: function ($scope) {
                    $scope.close = function () {
                        modalInstance.dismiss('close');
                    }
                }
            });
        }



        $scope.decreaseValue = function (obj, index, currentPage) {

            if (currentPage <= 1) {
                $scope.index = index;
            } else {
                $scope.index = (currentPage - 1) * 10 + index;
            }

            $scope.qty = JSON.parse(obj.qty)
            if (obj.qty > 0) {
                $scope.qty = JSON.parse(obj.qty)
                $scope.qty -= JSON.parse(obj.pieceperCarton);;

                if ($scope.qty > 0) {
                    obj.qty = JSON.stringify($scope.qty);
                    $scope.quantity = obj.qty;
                }

                $scope.prodname = obj.upload_name;
                if ($scope.quantity >= 1) {
                    $scope.getdistributorVoldiscount($scope.quantity, $scope.prodname, $scope.index);
                }
            }


        }

        $scope.increaseValue = function (obj, index, currentPage) {
            if (currentPage <= 1) {
                $scope.index = index;
            } else {
                $scope.index = (currentPage - 1) * 10 + index;
            }

            $scope.qty = JSON.parse(obj.qty)
            $scope.qty += JSON.parse(obj.pieceperCarton);;
            obj.qty = JSON.stringify($scope.qty);

            $scope.quantity = obj.qty;
            $scope.prodname = obj.upload_name;
            if ($scope.quantity >= 1) {
                $scope.getdistributorVoldiscount($scope.quantity, $scope.prodname, $scope.index);
            }
        }

        $scope.getQuantity = function (qty, productName, index, currentPage) {

            if (currentPage <= 1) {
                $scope.index = index;
            } else {
                $scope.index = (currentPage - 1) * 10 + index;
            }

            $scope.quantity = qty;
            $scope.prodname = productName;
            if ($scope.quantity >= 1) {
                $scope.getdistributorVoldiscount($scope.quantity, $scope.prodname, $scope.index);
            }

        }

        $scope.deleteDealerCartItem = function (item) {
            $scope.loading = true;
            distributorService.deleteDealerCartItemData(item.upload_name, window.localStorage['user_id']).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'product deleted successfully') {
                    // alert(data.data.status);
                    //  $("#deleteItemSuccessModal").modal('show');
                    $scope.displayMsg = "Item deleted from Cart Successfully!";
                    var modalInstance = $modal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        scope: $scope,
                        templateUrl: 'views/model_popup.html',
                        controller: function ($scope) {
                            $scope.close = function () {
                                modalInstance.dismiss('close');
                            }
                        }
                    });
                    $scope.viewcartData();
                }

            })
        }

        $scope.submitdealerorderdetails = function (obj) {
            $scope.rfqitemslist = obj;
            $scope.loading = true;
            distributorService.submitdealerorderdetails($scope.rfqitemslist, window.localStorage['user_id'], window.localStorage['usertype']).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'success') {
                    $scope.rfq_no = data.data.rfq_no;
                    //  alert('RFQ Generated');
                    //  $("#rfqSuccessModal").modal('show');
                    $scope.displayNumber = $scope.rfq_no;
                    $scope.displayMsg = "RFQ Number Generated Successfully!";
                    var modalInstance = $modal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        scope: $scope,
                        templateUrl: 'views/numberModalPopup.html',
                        controller: function ($scope) {
                            $scope.close = function () {
                                modalInstance.dismiss('close');
                            }
                        }
                    });


                    $rootScope.totalItems = "";
                    $scope.viewcartData();
                }
            })
        }

        // $scope.closeRFQModal = function () {
        //     $("#rfqSuccessModal").modal('hide');
        //     $location.path("Dashboard");
        // }

        $scope.getdistributorVoldiscount = function (quantity, prodname, index) {
            distributorService.getvolumediscount(quantity, prodname).then(function (data) {
                // alert(JSON.parse(data.data.product_data))

                if (data.data.status == 'Success') {
                    $rootScope.voldiscount = data.data.Percentage;
                    $rootScope.voldiscpercentage = data.data.discount;
                    $scope.spclDiscountList = data.data.product_info;
                    $scope.cartItems[index].special_discount = $scope.spclDiscountList[0].special_disc;
                    $scope.cartItems[index].volume_discount = $scope.spclDiscountList[0].vol_Percentage;
                    $scope.cartItems[index].total = $scope.spclDiscountList[0].total;
                    $scope.cartItems[index].saving_percentage = $scope.spclDiscountList[0].saving_percentage;
                    $scope.addToCartItems([$scope.cartItems[index]]);
                }

            })
        }

        $scope.addToCartItems = function (obj) {
            distributorService.updateViewCart(obj, window.localStorage['user_id'], $scope.random_no).then(function (data) {

                if (data.data.status == 'success') {
                    //    $scope.viewcart();
                    $rootScope.totalItems = data.data.cart_count;
                    $scope.grandTotal = data.data.total_price;
                }
                else {
                    alert(data.data.status)
                }
            })
        }

        $rootScope.B2BDashboard = function () {
            $location.path("Dashboard");
        };

        $scope.gotoShopping = function () {
            $location.path("createRFQ");
        }


    }]);
