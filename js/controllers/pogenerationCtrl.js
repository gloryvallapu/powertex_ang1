//var app = angular.module("smtApp", []);

shopMyToolsApp.controller('pogenerationController', ['$scope', 'pogenerationService', '$filter', '$rootScope', '$location', '$modal', '$anchorScroll',
    function ($scope, pogenerationService, $filter, $rootScope, $location, $modal, $anchorScroll) {

        $scope.dealer = false;
        $scope.sales = false;
        $scope.accountant = false;
        $scope.whmanger = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];

        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.sales = true;
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountant = true;
        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            $scope.accountant = true;
            $scope.userid = window.localStorage['user_id'];
        }

        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }

        $scope.rfqNo = window.localStorage['rfqnumber'];
        $scope.min_split_date = new Date();
        console.log(window.localStorage['user_id']);
        $scope.rfqQuote = function () {
            $scope.loading = true;
            pogenerationService.pogeneration(window.localStorage['rfqnumber']).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'Success') {
                    $scope.RFQgeneratedList = data.data.rfq_list;
                    $scope.data = $scope.RFQgeneratedList;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = 10;
                } else {
                    $scope.RFQgeneratedList = [];
                }

            })

        }
        $scope.rfqQuote();

        $scope.rfqdata = {};
        $scope.splitActions = [{
            id: "Accept",
            name: "Accept"
        }
            //  {
            //     id: "Partially Rejected",
            //     name: "Partially Rejected"
            // }
        ];


        $scope.productConfirmation = function (obj, status, sno) {
            $scope.rfqdata = {};
            $scope.rfqdata.split_status = "Accept";
            $scope.showSplitInfoData = true;
            $scope.sno = sno;
            $scope.dealer_qty = obj.dealer_qty;
            $scope.productName = obj.productname;
            $scope.reqQty = obj.dealer_qty;
            $scope.productid = obj.productid;
            $scope.remainingqty = obj.balance_qty;
            $scope.productStatus = obj.product_status;
            $scope.status = status;
            if (status != 'Split') {
                $scope.rfqdata.accept_prod_qty = $scope.reqQty;
                $scope.rfqdata.split_status = $scope.status;
            }
        }

        $scope.showSplitData = function (id, name) {
            $scope.productid = id;
            $scope.productName = name;
            $scope.splitedData();
        }


        $scope.splitedData = function () {
            pogenerationService.splitdataMethod($scope.userid, window.localStorage['rfqnumber']).then(function (data) {
                if (data.data.status == 'Success') {
                    $scope.splitedList = data.data.rfq_list;

                    //  $('#splitInfoTable').modal('show');
                    var modalInstance = $modal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        scope: $scope,
                        templateUrl: 'views/splitInfoModal.html',
                        controller: function ($scope) {
                            $scope.close = function () {
                                modalInstance.dismiss('close');
                            }
                        }
                    });
                }
            })
        };

        $scope.acceptedData = [];

        $scope.rfqsplitConfirm = function (obj, form) {
            $scope.acceptedData = obj;
            $scope.acceptedData.accept_productname = $scope.productName;
            //  $scope.acceptedData.unique_nbr = $scope.uniqueNbr;

            if ($scope.status != 'Reject') {
                $scope.acceptedData.split_delvry_date = $filter('date')(obj.split_delvry_date, "dd/MM/yyyy");
                $scope.dealer_status = 'Inprogress';
            } else {
                $scope.dealer_status = 'Reject';
                $scope.acceptedData.split_delvry_date = "";
            }

            if ($scope.status != 'Split') {
                $scope.acceptedData.balance_qty = 0;
                $scope.remainingqty = 0;
            } else {
                $scope.acceptedData.balance_qty = $scope.remainingqty - obj.accept_prod_qty;
            }

            $scope.loading = true;
            if ($scope.productStatus == 'Pending for SM') {
                pogenerationService.splitconformMethod($scope.status, $scope.userid, [$scope.acceptedData], window.localStorage['rfqnumber']).then(function (data) {
                    $scope.loading = false;

                    $scope.displayMsg = "Product Scheduled Successfully!";
                    if (data.data.status == 'Success') {
                        // alert('splited Quantity Successfully');                      
                        // $('#splitItemSuccessModal').modal('show');

                        var modalInstance = $modal.open({
                            animation: true,
                            ariaLabelledBy: 'modal-title',
                            ariaDescribedBy: 'modal-body',
                            scope: $scope,
                            templateUrl: 'views/model_popup.html',
                            controller: function ($scope) {
                                $scope.close = function () {
                                    modalInstance.dismiss('close');
                                }
                            }
                        });

                        $scope.rfqQuote();
                        $scope.showSplitInfoData = false;
                    }
                    else {
                        alert(data.data.status)
                    };
                });

            } else {

                $scope.acceptedData.product_status = $scope.status;

                $scope.acceptedData.dealer_status = $scope.dealer_status;

                pogenerationService.updateSplitconformMethod($scope.userid, [$scope.acceptedData], window.localStorage['rfqnumber']).then(function (data) {
                    $scope.loading = false;

                    // $scope.displayNumber = $scope.rfq_no;
                    $scope.displayMsg = "Product Scheduled Successfully!";
                    if (data.data.status == 'Success') {
                        // alert('Updated');
                        // $('#splitItemSuccessModal').modal('show');
                        //$("#successTable").modal('show');
                        var modalInstance = $modal.open({
                            animation: true,
                            ariaLabelledBy: 'modal-title',
                            ariaDescribedBy: 'modal-body',
                            scope: $scope,
                            templateUrl: 'views/model_popup.html',
                            controller: function ($scope) {
                                $scope.close = function () {
                                    modalInstance.dismiss('close');
                                }
                            }
                        });

                        $scope.rfqQuote();
                        $scope.showSplitInfoData = false;
                    }
                    else {
                        alert(data.data.status)
                    };
                });
            }
        }

        $rootScope.B2BDashboard = function () {
            $location.path("Dashboard");
        };
        $scope.gotoPendingRfq = function () {
            $location.path("pendingrfq");
        }



    }]);





