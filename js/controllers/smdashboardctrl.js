
shopMyToolsApp.controller('smdashboardController', ['$scope', '$rootScope', 'podashboardService', '$location', '$modal',

    function ($scope, $rootScope, podashboardService, $location, $modal) {

        $scope.dealer = false;
        $scope.sales = false;
        $scope.accountant = false;
        $scope.whmanger = false;
        $scope.purchase = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];

        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.sales = true;
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountant = true;
        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            $scope.accountant = true;
            $scope.userid = window.localStorage['user_id'];
        } else if (window.localStorage['usertype'] == "Purchase-Manager") {
            $scope.purchase = true;
            $scope.userid = window.localStorage['user_id'];
        }

        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }


        $scope.getpendingPoData = function () {
            $scope.moreRfqDataList = [];
            $scope.loading = true;
            podashboardService.pendingPodetails($scope.userid, window.localStorage['usertype']).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'Success') {
                    localStorage.setItem('company_details', JSON.stringify(data.data.dealer[0]));
                    $scope.Po_moreinfo = data.data.more_info;
                    $scope.data = $scope.Po_moreinfo;
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                    $scope.Po_moreinfo.forEach(function (items) {
                        items.rfq_list.forEach(function (item) {
                            $scope.moreRfqDataList.push(item);
                        })
                    })
                } else {
                    $scope.Po_moreinfo = [];
                }
            })
        }
        $scope.getpendingPoData();

        $scope.Podetails = function (item) {
            $scope.rfqNum = item.rfq_num;
            $scope.po_num = item.po_num;
            $scope.status = item.status;
            $scope.podetailshow = true;
        }

        $scope.getSplitData = function (item) {
            $scope.splitData = item.split_info;
            $scope.rfqNum = item.rfq_num;
            $scope.productname = item.productname;
            $scope.product_id = item.productid;
            //  $("#splitInfoTable").modal('show');
            $scope.split_info = item.split_info;
            //   $("#addedToCart").modal('show');

            var modalInstance = $modal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                scope: $scope,
                templateUrl: 'views/splitScheduleModal.html',
                controller: function ($scope) {
                    $scope.close = function () {
                        modalInstance.dismiss('close');
                    }
                }
            });
        }

        $scope.poApproval = function (status) {
            $scope.loading = true;
            podashboardService.PostPoApproved($scope.rfqNum, status, $scope.userid).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'Success') {
                    // alert('Accepted PO Request');
                    // $("#POApproveModal").modal('show');
                    $scope.displayNumber = $scope.po_num;
                    $scope.displayMsg = "PO Request Approved Successfully!";
                    var modalInstance = $modal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        scope: $scope,
                        templateUrl: 'views/numberModalPopup.html',
                        controller: function ($scope) {
                            $scope.close = function () {
                                modalInstance.dismiss('close');
                            }
                        }
                    });

                    $scope.podetailshow = false;
                }
                $scope.getpendingPoData();
            })

        }
        $scope.printPurchaseOrder = function (data) {
            localStorage.setItem('print_po_data', JSON.stringify(data));
            $location.path("po_print_page");

        }
        $scope.B2BDashboard = function () {
            $location.path("Dashboard");
        }

    }]);

shopMyToolsApp.controller('printPOController', ['$scope', '$location',
    function ($scope, $location) {

        $scope.rfqList = [];
        $scope.company_details = JSON.parse(localStorage.getItem('company_details'));
        $scope.print_po_data = JSON.parse(localStorage.getItem('print_po_data'));


        $scope.print_po_data.rfq_list.forEach(function (item) {
            if (Object.keys(item).length > 0) {
                $scope.rfqList.push(item);
            }
        })

        $scope.B2BDashboard = function () {
            $location.path("Dashboard");
        }
        $scope.gotoPoOrder = function () {
            $location.path("pendingpo");
        };
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }

    }]);

shopMyToolsApp.controller('pendingInvoiceController', ['$scope', '$location', '$filter', 'smLoginService', '$rootScope', '$modal',
    function ($scope, $location, $filter, smLoginService, $rootScope, $modal) {

        $scope.dealer = false;
        $scope.sales = false;
        $scope.accountant = false;
        $scope.whmanger = false;
        $scope.emptyValue = 0;
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.sales = true;
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountant = true;
            //  $scope.salesHead = true;
        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            $scope.userid = window.localStorage['user_id'];
        } else if (window.localStorage['usertype'] == "salesHead") {
            $scope.userid = window.localStorage['user_id'];
            $scope.salesHead = true;
        }
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }


        $scope.cureentDate = new Date();
        $scope.fromDate = $scope.cureentDate;
        $scope.from_date = $filter('date')($scope.cureentDate, "MM/dd/yyyy");
        $scope.from_date_frmt = $filter('date')($scope.cureentDate, "dd/MM/yyyy");

        $scope.to_date = $filter('date')($scope.cureentDate, "MM/dd/yyyy");
        $scope.to_date_frmt = $filter('date')($scope.cureentDate, "dd/MM/yyyy");

        // var numberOfDaysToAdd = 2;
        // $scope.cureentDate.setDate($scope.cureentDate.getDate() + numberOfDaysToAdd);

        // var dd = $scope.cureentDate.getDate();
        // var mm = $scope.cureentDate.getMonth() + 1;
        // if (mm < 9) {
        //     var mm = '0' + mm;
        // }
        // var y = $scope.cureentDate.getFullYear();

        // $scope.to_date = mm + '/' + dd + '/' + y;
        // $scope.to_date_frmt = dd + '/' + mm + '/' + y;

        $scope.getFromDate = function (fromDate) {
            $scope.fromDate = fromDate;
            $scope.from_date = $filter('date')(fromDate, "MM/dd/yyyy");
            $scope.from_date_frmt = $filter('date')(fromDate, "dd/MM/yyyy");
        }

        $scope.getToDate = function (toDate) {
            $scope.to_date = $filter('date')(toDate, "MM/dd/yyyy");
            $scope.to_date_frmt = $filter('date')(toDate, "dd/MM/yyyy");
        }


        $scope.getproformaRecords = function () {
            $scope.invoicelistData = [];
            $scope.productDetailshow = false;
            $scope.smpendingInvoice();
        }

        $scope.invGenDefault = true
        $scope.proformaData = [];
        $scope.smpendingInvoice = function () {
            $scope.proformaData = [];
            $scope.loading = true;
            $scope.moreRfqDataList = [];
            smLoginService.acInvoiceDetailsget($scope.userid, window.localStorage['usertype'], $scope.to_date_frmt, $scope.from_date_frmt).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'Success') {
                    $scope.invoicelistData = data.data.more_info;

                    $scope.invoicelistData.forEach(function (items) {
                        items.rfq_list.forEach(function (item) {
                            if (Object.keys(item).length > 0 && item.accept_productname != undefined) {
                                $scope.moreRfqDataList.push(item);
                                $scope.match = true;

                            }
                        });
                        if ($scope.match) {
                            $scope.proformaData.push(items);
                        }
                    });
                    $scope.data = $scope.proformaData;
                    console.log($scope.data);
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                    for (i = 0; i < $scope.moreRfqDataList.length; i++) {
                        $scope.moreRfqDataList[i].count = '';
                    }
                } else {
                    $scope.invoicelistData = [];
                }
            })
        }
        $scope.smpendingInvoice();

        $scope.moreDetails = function (item) {
            $scope.rfqNum = item.rfq_num;
            $scope.po_num = item.po_num;
            $scope.productDetailshow = true;
        }

        $scope.invPrepartionList = [];

        $scope.getForInvoicePreparation = function (item) {
            $scope.rfqNum = item.rfq_num;
            $scope.po_num = item.po_num;
            $scope.productDetailshow = true;
            window.localStorage['po_num'] = item.po_num;


            $scope.acceptFinalproformaRecord = [];
            $scope.splitAcceptProformaRecrds = [];

            if ($scope.accountant) {
                $scope.moreRfqDataList.forEach(function (item) {
                    if (item.rfq_num == $scope.rfqNum && item.product_status == 'Accept' && item.flag == 'yes') {
                        $scope.acceptFinalproformaRecord.push(item);
                    }
                    if (item.rfq_num == $scope.rfqNum && item.product_status == 'Split') {
                        $scope.splitAcceptProformaRecrds.push(item);
                    }
                });

                $scope.countVal = 0;
                if ($scope.splitAcceptProformaRecrds.length > 0) {
                    $scope.splitAcceptProformaRecrds.forEach(function (item) {
                        $scope.product_id = item.productid;
                        item.split_info.forEach(function (data) {
                            if (data.flag == 'yes') {
                                $scope.countVal += JSON.parse(data.remain_accept_qty);
                            }
                        });
                        for (i = 0; i < $scope.moreRfqDataList.length; i++) {
                            if ($scope.moreRfqDataList[i].productid == $scope.product_id && $scope.moreRfqDataList[i].rfq_num == $scope.rfqNum) {
                                $scope.moreRfqDataList[i].count = $scope.countVal;

                            }
                        }
                    });
                };




                //  split_info



                $scope.acntFinalSplitRecrd = [];

                $scope.acntFinalAcptRecrd = [];
                //  $scope.splitFinalPerformaRecord = [];

                for (i = 0; i < $scope.acceptFinalproformaRecord.length; i++) {
                    var item = $scope.acceptFinalproformaRecord[i];
                    $scope.inv_info = {
                        "product_qty": item.qty,
                        "product_name": item.productname,
                        "total_amt": item.remain_total,
                        "inv_qty": item.remain_qty,
                        "product_id": item.productid,
                        "delivry_date": item.delivry_date,
                        "product_status": item.product_status,
                        "piece_per_carton": item.pieceperCarton,
                        "remain_total": 0,
                        "acpt_inv_qty": 0,
                        "split_inv_qty": 0,
                        "unique_nbr": 0,
                        "rem_qty": 0
                    };
                    $scope.acntFinalAcptRecrd.push($scope.inv_info);
                }




                for (i = 0; i < $scope.splitAcceptProformaRecrds.length; i++) {
                    var itemdata = $scope.splitAcceptProformaRecrds[i];

                    for (j = 0; j < $scope.splitAcceptProformaRecrds[i].split_info.length; j++) {
                        var item = $scope.splitAcceptProformaRecrds[i].split_info[j]
                        if (item.flag == 'yes') {
                            $scope.inv_info = {
                                "prdct_rmain_total": itemdata.remain_total,
                                "remain_qty": itemdata.remain_qty,
                                "product_qty": itemdata.qty,
                                "product_name": itemdata.productname,
                                "total_amt": itemdata.remain_total * (item.remain_accept_qty / itemdata.remain_qty),
                                "inv_qty": item.remain_accept_qty,
                                "product_id": itemdata.productid,
                                "rem_qty": itemdata.remain_qty - item.remain_accept_qty,
                                "unique_nbr": item.unique_nbr,
                                "delivry_date": item.split_delvry_date,
                                "acpt_inv_qty": item.remain_accept_qty,
                                "split_inv_qty": item.remain_accept_qty - item.remain_accept_qty,
                                "product_status": itemdata.product_status,
                                "piece_per_carton": itemdata.pieceperCarton,
                                "remain_total": itemdata.remain_total - (itemdata.remain_total * (item.remain_accept_qty / itemdata.remain_qty))
                            };
                            $scope.acntFinalSplitRecrd.push($scope.inv_info);
                        }
                    }
                }
                console.log($scope.acntFinalSplitRecrd);
            }
        }

        $scope.getSplitData = function (item, index) {
            $scope.product_name = item.productname;
            $scope.count = 0;
            $scope.index = index;
            $scope.splitData = item.split_info;
            $scope.product_id = item.productid;
            //  $("#splitInfoTable").modal('show');

            $scope.modalInstance = $modal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                scope: $scope,
                templateUrl: 'views/proformaSplitModal.html',
                controller: function ($scope) {
                    $scope.close = function () {
                        $scope.modalInstance.dismiss('close');
                    }
                }
            });
        };

        $scope.closeModal = function () {
            $scope.modalInstance.close();
        }


        var todayDate = new Date();
        var invoice_date = $filter('date')(todayDate, "dd/MM/yyyy");

        $scope.invPrepartionList = [];
        $scope.getInvQty = function (item, invQty) {
            $scope.invGenDefault = false;
            if (invQty > 0) {
                $scope.inv_info = {
                    "product_qty": item.qty,
                    "product_name": item.productname,
                    "total_amt": item.remain_total * (item.count / item.remain_qty),
                    "inv_qty": invQty,
                    "product_id": item.productid,
                    "rem_qty": item.remain_qty - invQty,
                    "delivry_date": item.delivry_date,
                    "product_status": item.product_status,
                    "piece_per_carton": item.pieceperCarton,
                    "remain_total": item.remain_total - (item.remain_total * (item.count / item.remain_qty)),
                    "acpt_inv_qty": 0,
                    "split_inv_qty": 0,
                    "unique_nbr": 0
                };
                if ($scope.invPrepartionList.length > 0) {
                    for (i = 0; i < $scope.invPrepartionList.length; i++) {
                        if ($scope.invPrepartionList[i].product_id == item.productid) {
                            $scope.invPrepartionList[i].total_amt = item.total * (item.remain_total / item.remain_qty);
                            $scope.invPrepartionList[i].inv_qty = invQty;
                            $scope.invPrepartionList[i].rem_qty = item.remain_qty - invQty;
                            $scope.invPrepartionList[i].remain_total = $scope.inv_info.remain_total;
                            $scope.match = false;
                            break;
                        } else {
                            $scope.match = true;
                        }
                    }
                    if ($scope.match) {
                        $scope.invPrepartionList.push($scope.inv_info);
                    }
                } else {
                    $scope.invPrepartionList.push($scope.inv_info);
                }
            }

        }


        $scope.generateInvoice = function () {

            if ($scope.accountant) {
                $scope.finalInvList = $scope.acntFinalAcptRecrd.concat($scope.acntFinalSplitRecrd);
            } else {
                $scope.finalInvList = $scope.invPrepartionList.concat($scope.invSplitPrdoucts);
            }
            if ($scope.finalInvList.length > 0) {
                $scope.loading = true;
                smLoginService.acInvoiceDetailsPost(window.localStorage['usertype'], $scope.finalInvList, $scope.po_num, $scope.userid, invoice_date).then(function (data) {
                    $scope.loading = false;
                    if (data.data.status == "Succes") {
                        // alert('Invoice Generated');
                        window.localStorage['p_inv_num'] = data.data.inv_num;
                        $scope.p_inv_num = data.data.inv_num;
                        // $("#pinvoiceSuccessModal").modal('show');
                        $scope.displayNumber = $scope.p_inv_num;
                        $scope.displayMsg = "Invoice Generated Successfully!";
                        var modalInstance = $modal.open({
                            animation: true,
                            ariaLabelledBy: 'modal-title',
                            ariaDescribedBy: 'modal-body',
                            scope: $scope,
                            templateUrl: 'views/numberModalPopup.html',
                            controller: function ($scope) {
                                $scope.close = function () {
                                    modalInstance.dismiss('close');
                                    $location.path("print_invoice_page");
                                }
                            }
                        });


                        $scope.productDetailshow = false;
                        $scope.moreRfqDataList = [];
                    }
                });
            } else {
                alert('Please Selecte Dates for Proforma Invoice Generation');
            }

        }

        // $scope.closePinvoiceModal = function () {
        //     // $("#pinvoiceSuccessModal").modal('hide');
        //     $scope.closeModal = function () {
        //         $scope.modalInstance.close();
        //     }
        //     $location.path("print_invoice_page");
        // }

        $scope.invSplitPrdoucts = [];

        $scope.splitProducts = function (item, itemdata, index) {
            $scope.index = index;
            $scope.productname = itemdata.productname;
            $scope.product_id = itemdata.productid;
            $scope.invGenDefault = false;

            if (item.invAccQty >= 0) {
                $scope.inv_info = {
                    "prdct_rmain_total": itemdata.remain_total,
                    "remain_qty": itemdata.remain_qty,
                    "product_qty": itemdata.qty,
                    "product_name": itemdata.productname,
                    "total_amt": itemdata.remain_total * (item.invAccQty / itemdata.remain_qty),
                    "inv_qty": item.invAccQty,
                    "product_id": itemdata.productid,
                    // "rem_qty": itemdata.remain_qty - item.invAccQty,
                    "unique_nbr": item.unique_nbr,
                    "delivry_date": item.split_delvry_date,
                    "acpt_inv_qty": item.remain_accept_qty,
                    "split_inv_qty": item.remain_accept_qty - item.invAccQty,
                    "product_status": itemdata.product_status,
                    "piece_per_carton": itemdata.pieceperCarton,
                    // "remain_total": itemdata.remain_total - (itemdata.remain_total * (item.invAccQty / itemdata.remain_qty)),
                };

                if ($scope.invSplitPrdoucts.length > 0) {
                    for (i = 0; i < $scope.invSplitPrdoucts.length; i++) {
                        if ($scope.invSplitPrdoucts[i].unique_nbr == item.unique_nbr) {
                            $scope.invSplitPrdoucts[i].total_amt = itemdata.total * (item.invAccQty / itemdata.remain_qty);
                            $scope.invSplitPrdoucts[i].inv_qty = item.invAccQty;
                            // $scope.invSplitPrdoucts[i].rem_qty = itemdata.remain_qty - item.invAccQty;
                            // $scope.invSplitPrdoucts[i].remain_total = $scope.inv_info.remain_total;
                            $scope.match = false;
                            break;
                        } else {
                            $scope.match = true;
                        }
                    }
                    if ($scope.match) {
                        $scope.invSplitPrdoucts.push($scope.inv_info);
                    }
                } else {
                    $scope.invSplitPrdoucts.push($scope.inv_info);
                }
            }
        }


        $scope.splitCount = function (productname) {
            $scope.count = 0;
            //Accountant
            if ($scope.accountant) {
                if ($scope.acntFinalSplitRecrd.length > 0) {
                    for (i = 0; i < $scope.acntFinalSplitRecrd.length; i++) {
                        if (productname == $scope.acntFinalSplitRecrd[i].product_name) {
                            $scope.count += JSON.parse($scope.acntFinalSplitRecrd[i].inv_qty);
                        }
                    }

                }

                for (j = 0; j < $scope.acntFinalSplitRecrd.length; j++) {
                    if ($scope.acntFinalSplitRecrd[j].product_id == $scope.product_id) {
                        $scope.acntFinalSplitRecrd[j].rem_qty = $scope.acntFinalSplitRecrd[j].remain_qty - $scope.count;
                        $scope.acntFinalSplitRecrd[j].remain_total = $scope.acntFinalSplitRecrd[j].prdct_rmain_total * ($scope.acntFinalSplitRecrd[j].rem_qty / $scope.acntFinalSplitRecrd[j].remain_qty);
                    }
                }
            }
            //Sales Head
            if ($scope.invSplitPrdoucts.length > 0) {
                for (i = 0; i < $scope.invSplitPrdoucts.length; i++) {
                    if (productname == $scope.invSplitPrdoucts[i].product_name) {
                        $scope.count += JSON.parse($scope.invSplitPrdoucts[i].inv_qty);
                    }
                }
            }
            for (j = 0; j < $scope.invSplitPrdoucts.length; j++) {
                if ($scope.invSplitPrdoucts[j].product_id == $scope.product_id) {
                    $scope.invSplitPrdoucts[j].rem_qty = $scope.invSplitPrdoucts[j].remain_qty - $scope.count;
                    $scope.invSplitPrdoucts[j].remain_total = $scope.invSplitPrdoucts[j].prdct_rmain_total * ($scope.invSplitPrdoucts[j].rem_qty / $scope.invSplitPrdoucts[j].remain_qty);
                }
            }
            //table binding qty
            for (i = 0; i < $scope.moreRfqDataList.length; i++) {
                if ($scope.moreRfqDataList[i].productid == $scope.product_id && $scope.moreRfqDataList[i].rfq_num == $scope.rfqNum) {
                    $scope.moreRfqDataList[i].count = $scope.count;

                }
            }
        }

        $rootScope.B2BDashboard = function () {
            $location.path("Dashboard");
        }


    }]);


shopMyToolsApp.controller('invoicePreparatonController', ['$scope', 'smLoginService', '$rootScope', '$location', '$modal',
    function ($scope, smLoginService, $rootScope, $location, $modal) {

        $rootScope.B2BDashboard = function () {
            $location.path("Dashboard");
        }
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }
        $scope.dealer = false;
        $scope.sales = false;
        $scope.accountant = false;
        $scope.whmanger = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];

        $scope.deliverDetails = {};
        $scope.deliverDetails.delivery_type = "Vehicle";

        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.sales = true;
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountant = true;
        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            //   $scope.accountant = true;
            $scope.userid = window.localStorage['user_id'];
        }

        $scope.getEstimateQtyDetails = function () {
            // $scope.EstimateQtyList = [];
            $scope.loading = true;
            smLoginService.getEstimateQtyDetails(window.localStorage['usertype'], window.localStorage['user_id']).then(function (data) {
                $scope.loading = false;
                if (data.data.Status == 'Success') {
                    $scope.EstimateQtyList = data.data.more_Info;
                } else {
                    $scope.EstimateQtyList = [];
                }

            })
        };
        $scope.getEstimateQtyDetails();

        $scope.invDetails = function (item) {
            $scope.po_num = item.po_num;
            $scope.inv_no = item.p_inv_num;
            $scope.invMoreInfo = item.moreinv_info;
            $scope.invoice_date = item.invoice_date;
            $scope.dealerName = item.dealer_name;
            $scope.invDetailshow = true;
            window.localStorage['po_num'] = item.po_num;
        }

        $scope.showInvoiceData = function (item) {
            $scope.po_num = item.po_num;
            $scope.inv_no = item.inv_num;
            $scope.showMoreInfo = true;
            window.localStorage['po_num'] = item.po_num
        }
        $scope.invPreparationDataList = [];

        $scope.estimatedQty = function (data) {

            $scope.estimated_qty_info = {
                "product_name": data.productname,
                "inv_num": data.p_inv_num,
                "avl_carton": data.avl_carton,
                "check_qty": data.check_qty,
                "restrictions": data.restrictions,
                "unique_num": data.unique_num
            }

            if ($scope.invPreparationDataList.length > 0) {
                for (i = 0; i < $scope.invPreparationDataList.length; i++) {
                    if ($scope.invPreparationDataList[i].product_name == data.productname && $scope.invPreparationDataList[i].unique_num == data.unique_num) {
                        $scope.invPreparationDataList[i].avl_carton = data.avl_carton;
                        $scope.invPreparationDataList[i].check_qty = data.check_qty;
                        $scope.invPreparationDataList[i].restrictions = data.restrictions
                        $scope.match = false;
                        break;
                    } else {
                        $scope.match = true;
                    }
                }
                if ($scope.match) {
                    $scope.invPreparationDataList.push($scope.estimated_qty_info);
                }
            } else {
                $scope.invPreparationDataList.push($scope.estimated_qty_info);
            }
        }

        $scope.prepareInvoice = function () {
            smLoginService.prepareInvoice(window.localStorage['po_num'], $scope.invPreparationDataList).then(function (data) {

                $scope.displayMsg = "Packing List Prepared Successfully!";
                if (data.data.status == 'qty updated successfully.') {
                    // $("#successTable").modal('show');
                    var modalInstance = $modal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        scope: $scope,
                        templateUrl: 'views/model_popup.html',
                        controller: function ($scope) {
                            $scope.close = function () {
                                modalInstance.dismiss('close');
                            }
                        }
                    });

                    $scope.invPreparationDataList = [];
                    $scope.getEstimateQtyDetails();
                    $scope.invDetailshow = false;
                };

            })
        }

    }]);

shopMyToolsApp.controller('printPerformaInvController', ['$scope', 'smLoginService', '$rootScope', '$location', '$modal',
    function ($scope, smLoginService, $rootScope, $location, $modal) {
        $scope.dealer = false;
        $scope.sales = false;
        $scope.accountant = false;
        $scope.whmanger = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }
        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.sales = true;
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountant = true;
        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            $scope.accountant = true;
            $scope.userid = window.localStorage['user_id'];
        }


        $scope.moreInvList = [];
        //  window.localStorage['p_inv_num'] = "PIN00001";
        $scope.printInvoice = function () {
            smLoginService.printSingleInvoice(window.localStorage['p_inv_num'], window.localStorage['usertype']).then(function (data) {
                if (data.data.Status == 'Success') {
                    $scope.printData = data.data.more_Info;
                    $scope.rfqData = data.data.rfq;
                    $scope.total_qty = data.data.total_qty;
                    $scope.delaerData = data.data.dealer;
                    $scope.moreInvInfo = data.data.more_Info[0].moreinv_info;

                } else {
                    $scope.printData = [];
                }
            })
        }

        $scope.printInvoice();

        $scope.printInvoiceList = function (printSectionId) {
            var printContents = document.getElementById("printSectionId").innerHTML;
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"><link href="./css/smtCommonStyles.css" rel="stylesheet"></head><body onload="window.print()">' + printContents + '</body></html>');
            popupWin.document.close();

        }

        $rootScope.B2BDashboard = function () {
            $location.path("Dashboard");
        }

        $scope.gotoProforma = function () {
            $location.path("proformaInvoice");
        }

        $scope.gotoFinalInv = function () {
            $location.path("finalInvoiceGeneration");
        }

    }])

shopMyToolsApp.controller('pInvoicePackListController', ['$scope', '$http', '$location', '$filter', '$rootScope', 'smLoginService', '$modal',
    function ($scope, $http, $location, $filter, $rootScope, smLoginService, $modal) {
        $scope.dealer = false;
        $scope.sales = false;
        $scope.accountant = false;
        $scope.whmanger = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }
        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.sales = true;
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountant = true;
        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            $scope.accountant = true;
            $scope.userid = window.localStorage['user_id'];
        }



        $scope.getPackingListQtyDetails = function () {
            smLoginService.getPackingListQtyDetails(window.localStorage['usertype']).then(function (data) {
                if (data.data.Status == 'Success') {
                    $scope.EstimateQtyList = data.data.more_Info;
                    $scope.data = $scope.EstimateQtyList;
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                } else {
                    $scope.EstimateQtyList = [];
                }

            })
        };
        $scope.getPackingListQtyDetails();

        $scope.printList = function (item) {
            $scope.po_num = item.po_num;
            window.localStorage['po_num'] = item.po_num;
            $location.path("packingListPrint");
        }

        $rootScope.B2BDashboard = function () {
            $location.path("Dashboard");
        }

    }]);


shopMyToolsApp.controller('printPackingListController', ['$scope', 'smLoginService', '$rootScope', '$location', '$modal',
    function ($scope, smLoginService, $rootScope, $location, $modal) {
        $scope.dealer = false;
        $scope.sales = false;
        $scope.accountant = false;
        $scope.whmanger = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }
        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.sales = true;
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountant = true;
        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            $scope.accountant = true;
            $scope.userid = window.localStorage['user_id'];
        }
        //   window.localStorage['po_num'] = "PO00001";
        $scope.po_num = window.localStorage['po_num']

        $scope.printpackList = function () {
            smLoginService.printInvoice(window.localStorage['po_num'], window.localStorage['usertype']).then(function (data) {
                if (data.data.Status == 'Success') {
                    $scope.printData = data.data.more_Info;
                    $scope.rfqData = data.data.rfq;
                    $scope.total_qty = data.data.total_qty;
                    $scope.delaerData = data.data.dealer;
                } else {
                    $scope.printData = [];
                }
            })
        }

        $scope.printpackList();

        $scope.printInvoiceList = function (printSectionId) {
            var printContents = document.getElementById("printSectionId").innerHTML;
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"><link href="./css/smtCommonStyles.css" rel="stylesheet"></head><body onload="window.print()">' + printContents + '</body></html>');
            popupWin.document.close();

        }

        $rootScope.B2BDashboard = function () {
            $location.path("Dashboard");
        };
        $scope.gotoPackList = function () {
            $location.path("packingList");
        }

    }])

shopMyToolsApp.controller('finalInvoiceController', ['$scope', 'smLoginService', '$location', '$rootScope', '$modal',
    function ($scope, smLoginService, $location, $rootScope, $modal) {


        $scope.dealer = false;
        $scope.sales = false;
        $scope.accountant = false;
        $scope.whmanger = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }
        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.sales = true;
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountant = true;
            // $scope.salesHead = true;
        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            $scope.accountant = true;
            $scope.userid = window.localStorage['user_id'];
        } else if (window.localStorage['usertype'] == "salesHead") {
            $scope.salesHead = true;
            $scope.userid = window.localStorage['user_id'];
        }



        $scope.getEstimateQtyDetails = function () {
            $scope.loading = true;
            $scope.EstimateQtyList = [];
            smLoginService.getEstimateQtyDetails(window.localStorage['usertype'], window.localStorage['user_id']).then(function (data) {
                $scope.loading = false;
                if (data.data.Status == 'Success') {
                    $scope.EstimateQtyList = data.data.more_Info;
                    $scope.data = $scope.EstimateQtyList;
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                } else {
                    $scope.EstimateQtyList = [];
                }

            })
        };
        $scope.getEstimateQtyDetails();


        $scope.priceAdjustment = function (data) {
            var val = data.mrp + data.mrp_adj;
            $scope.eff_mrp = val;
            $scope.productId = data.productid;
            $scope.eff_amt = (val - (val * data.savings / 100)) * data.check_qty;

            for (i = 0; i < $scope.moreInvData.length; i++) {
                if ($scope.moreInvData[i].productid == $scope.productId) {
                    $scope.moreInvData[i].eff_mrp = $scope.eff_mrp;
                    $scope.moreInvData[i].eff_amt = $scope.eff_amt;
                }
            }

            for (i = 0; i < $scope.moreInvData.length; i++) {
                var data = $scope.moreInvData[i];
                $scope.product_name = data.productname;

                if (data.unique_num == 0) {
                    $scope.acpt_carton_info = {
                        "product_name": data.productname,
                        "inv_num": data.p_inv_num,
                        "inv_acc_qty": data.check_qty,
                        "unique_num": data.unique_num,
                        "remain_qty": JSON.parse(data.remain_qty) + JSON.parse(data.p_inv_qty - data.check_qty),
                        "total_amt": data.eff_amt,
                        "remain_total": JSON.parse(data.remain_total) + data.total_amt * (data.p_inv_qty - data.check_qty),
                        "split_rem_qty": 0
                    }

                    $scope.finvAcceptRecord.push($scope.acpt_carton_info);
                } else {
                    $scope.acpt_carton_info = {
                        "product_name": data.productname,
                        "p_inv_qty": data.p_inv_qty,
                        "inv_num": data.p_inv_num,
                        "inv_acc_qty": data.check_qty,
                        "unique_num": data.unique_num,
                        "total_amt": data.eff_amt,
                        "split_rem_qty": data.acpt_inv_qty - data.check_qty
                    }
                    $scope.finvSplitRecord.push($scope.acpt_carton_info);

                    $scope.count = 0;
                    $scope.remain_total = 0;

                    for (i = 0; i < $scope.finvSplitRecord.length; i++) {
                        if ($scope.finvSplitRecord[i].product_name == $scope.product_name) {
                            $scope.count += $scope.finvSplitRecord[i].p_inv_qty - $scope.finvSplitRecord[i].inv_acc_qty;
                            $scope.remain_total += $scope.finvSplitRecord[i].total_amt;
                        }
                    }

                    for (i = 0; i < $scope.finvSplitRecord.length; i++) {
                        if ($scope.finvSplitRecord[i].product_name == $scope.product_name) {
                            $scope.finvSplitRecord[i].remain_qty = JSON.parse($scope.count) + JSON.parse(data.remain_qty);
                            $scope.finvSplitRecord[i].remain_total = $scope.remain_total * $scope.finvSplitRecord[i].remain_qty;
                        }
                    }
                }
            }

            console.log($scope.finvSplitRecord);
            console.log($scope.finvAcceptRecord);
        }



        $scope.invDetails = function (item) {
            $scope.p_inv_num = item.p_inv_num;
            $scope.po_num = item.po_num;
            $scope.dealerName = item.dealer_name;
            $scope.invDetailshow = true;
            window.localStorage['po_num'] = item.po_num;
            $scope.p_inv_num = item.p_inv_num;
            window.localStorage['p_inv_num'] = item.p_inv_num;

            // for Accountant

            $scope.moreInvData = item.moreinv_info;
            $scope.finvAcceptRecord = [];
            $scope.finvSplitRecord = [];

            if ($scope.accountant) {
                for (k = 0; k < $scope.moreInvData.length; k++) {
                    var data = $scope.moreInvData[k];
                    $scope.product_name = data.productname;

                    if (data.unique_num == 0) {
                        if ($scope.accountant) {
                            $scope.acpt_carton_info = {
                                "product_name": data.productname,
                                "inv_num": data.p_inv_num,
                                "inv_acc_qty": data.check_qty,
                                "unique_num": data.unique_num,
                                "remain_qty": JSON.parse(data.remain_qty) + JSON.parse(data.p_inv_qty - data.check_qty),
                                "total_amt": data.total_amt * (data.check_qty / data.p_inv_qty),
                                "remain_total": JSON.parse(data.remain_total) + data.total_amt * (data.p_inv_qty - data.check_qty),
                                "split_rem_qty": 0
                            }
                        } else {
                            $scope.acpt_carton_info = {
                                "product_name": data.productname,
                                "inv_num": data.p_inv_num,
                                "inv_acc_qty": data.check_qty,
                                "unique_num": data.unique_num,
                                "remain_qty": JSON.parse(data.remain_qty) + JSON.parse(data.p_inv_qty - data.check_qty),
                                "total_amt": eff_amt,
                                "remain_total": JSON.parse(data.remain_total) + data.total_amt * (data.p_inv_qty - data.check_qty),
                                "split_rem_qty": 0
                            }
                        }

                        $scope.finvAcceptRecord.push($scope.acpt_carton_info);
                    } else {

                        if ($scope.accountant) {
                            $scope.acpt_carton_info = {
                                "product_name": data.productname,
                                "p_inv_qty": data.p_inv_qty,
                                "inv_num": data.p_inv_num,
                                "inv_acc_qty": data.check_qty,
                                "unique_num": data.unique_num,
                                "total_amt": data.total_amt * (data.check_qty / data.p_inv_qty),
                                "split_rem_qty": data.acpt_inv_qty - data.check_qty
                            }
                        } else {
                            $scope.acpt_carton_info = {
                                "product_name": data.productname,
                                "p_inv_qty": data.p_inv_qty,
                                "inv_num": data.p_inv_num,
                                "inv_acc_qty": data.check_qty,
                                "unique_num": data.unique_num,
                                "total_amt": eff_amt,
                                "split_rem_qty": data.acpt_inv_qty - data.check_qty
                            }
                        }



                        $scope.finvSplitRecord.push($scope.acpt_carton_info);

                        $scope.count = 0;
                        $scope.remain_total = 0;

                        for (i = 0; i < $scope.finvSplitRecord.length; i++) {
                            if ($scope.finvSplitRecord[i].product_name == $scope.product_name) {
                                $scope.count += $scope.finvSplitRecord[i].p_inv_qty - $scope.finvSplitRecord[i].inv_acc_qty;
                                $scope.remain_total += $scope.finvSplitRecord[i].total_amt;
                            }
                        }



                        for (j = 0; j < $scope.finvSplitRecord.length; j++) {
                            if ($scope.finvSplitRecord[j].product_name == $scope.product_name) {
                                $scope.finvSplitRecord[j].remain_qty = JSON.parse($scope.count);
                                $scope.finvSplitRecord[j].remain_total = $scope.remain_total * $scope.finvSplitRecord[j].remain_qty;
                            }
                        }


                    }


                }

                //   console.log($scope.finvAcceptRecord);
            }
        }


        $scope.invPreparationDataList = [];
        $scope.splitInvPreparationDataList = [];
        $scope.estimatedQty = function (data) {

            $scope.product_name = data.productname;

            if (data.unique_num == 0) {
                $scope.acpt_carton_info = {
                    "product_name": data.productname,
                    "inv_num": data.p_inv_num,
                    "inv_acc_qty": data.inv_acc_qty,
                    "unique_num": data.unique_num,
                    "remain_qty": JSON.parse(data.remain_qty) + JSON.parse(data.p_inv_qty - data.inv_acc_qty),
                    "total_amt": data.total_amt * (data.inv_acc_qty / data.p_inv_qty),
                    "remain_total": JSON.parse(data.remain_total) + data.total_amt * (data.p_inv_qty - data.inv_acc_qty),
                    "split_rem_qty": 0
                }

                if ($scope.invPreparationDataList.length > 0) {
                    for (i = 0; i < $scope.invPreparationDataList.length; i++) {
                        if ($scope.invPreparationDataList[i].product_name == data.productname && $scope.invPreparationDataList[i].unique_num == data.unique_num) {
                            $scope.invPreparationDataList[i].inv_acc_qty = $scope.acpt_carton_info.inv_acc_qty;
                            $scope.invPreparationDataList[i].remain_qty = $scope.acpt_carton_info.remain_qty;
                            $scope.invPreparationDataList[i].total_amt = $scope.acpt_carton_info.total_amt;
                            $scope.invPreparationDataList[i].remain_total = $scope.acpt_carton_info.remain_total;
                            $scope.match = false;
                            break;
                        } else {
                            $scope.match = true;
                        }
                    }
                    if ($scope.match) {
                        $scope.invPreparationDataList.push($scope.acpt_carton_info);
                    }
                } else {
                    $scope.invPreparationDataList.push($scope.acpt_carton_info);
                }
            } else if (data.unique_num != '') {

                $scope.acpt_carton_info = {
                    "product_name": data.productname,
                    "inv_num": data.p_inv_num,
                    "inv_acc_qty": data.inv_acc_qty,
                    "unique_num": data.unique_num,
                    "total_amt": data.total_amt * (data.inv_acc_qty / data.p_inv_qty),
                    "split_rem_qty": data.acpt_inv_qty - data.inv_acc_qty
                }
                if ($scope.splitInvPreparationDataList.length > 0) {
                    for (i = 0; i < $scope.splitInvPreparationDataList.length; i++) {
                        if ($scope.splitInvPreparationDataList[i].product_name == data.productname && $scope.splitInvPreparationDataList[i].unique_num == data.unique_num) {
                            $scope.splitInvPreparationDataList[i].inv_acc_qty = $scope.acpt_carton_info.inv_acc_qty;
                            $scope.splitInvPreparationDataList[i].split_rem_qty = $scope.acpt_carton_info.split_rem_qty;
                            $scope.splitInvPreparationDataList[i].total_amt = $scope.acpt_carton_info.total_amt;
                            $scope.match = false;
                            break;
                        } else {
                            $scope.match = true;
                        }
                    }
                    if ($scope.match) {
                        $scope.splitInvPreparationDataList.push($scope.acpt_carton_info);
                    }
                } else {
                    $scope.splitInvPreparationDataList.push($scope.acpt_carton_info);
                }
            }

            $scope.count = 0;
            $scope.remain_total = 0;



            // for SalesHead
            for (i = 0; i < $scope.splitInvPreparationDataList.length; i++) {
                if ($scope.splitInvPreparationDataList[i].product_name == $scope.product_name) {
                    $scope.count += $scope.splitInvPreparationDataList[i].inv_acc_qty;
                    $scope.remain_total += $scope.splitInvPreparationDataList[i].total_amt;
                }
            }

            for (i = 0; i < $scope.splitInvPreparationDataList.length; i++) {
                if ($scope.splitInvPreparationDataList[i].product_name == $scope.product_name) {
                    $scope.splitInvPreparationDataList[i].remain_qty = JSON.parse($scope.count) + JSON.parse(data.remain_qty);
                    $scope.splitInvPreparationDataList[i].remain_total = $scope.remain_total * $scope.splitInvPreparationDataList[i].remain_qty;
                }
            }

        }




        $scope.generateFinalInvoice = function () {
            $scope.loading = true;
            if (window.localStorage['usertype'] == "accountant") {
                $scope.finalInvList = $scope.finvAcceptRecord.concat($scope.finvSplitRecord);
            } else {
                $scope.finalInvList = $scope.invPreparationDataList.concat($scope.splitInvPreparationDataList);
            }

            console.log($scope.finalInvList);
            smLoginService.finalInvoiceGeneration($scope.finalInvList, $scope.po_num, $scope.userid).then(function (data) {
                $scope.loading = false;
                if (data.data.status == "qty updated successfully.") {
                    $scope.inv_num = data.data.inv_num;
                    // $("#FinvSuccModal").modal('show');

                    $scope.displayNumber = $scope.inv_num;
                    $scope.displayMsg = "Invoice Generated Successfully!";
                    var modalInstance = $modal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        scope: $scope,
                        templateUrl: 'views/numberModalPopup.html',
                        controller: function ($scope) {
                            $scope.close = function () {
                                modalInstance.dismiss('close');
                                $location.path("finv_print");
                            }
                        }
                    });

                    $scope.invDetailshow = false;
                    $scope.getEstimateQtyDetails();
                }
            })
        }


        // $scope.closePinvoiceModal = function () {
        //     $("#FinvSuccModal").modal('hide');
        //     $location.path("finv_print");
        // }

        $rootScope.B2BDashboard = function () {
            $location.path("Dashboard");
        }

    }]);


shopMyToolsApp.controller('challanGenerationController', ['$scope', 'smLoginService', '$rootScope', '$location', '$modal',
    function ($scope, smLoginService, $rootScope, $location, $modal) {

        $scope.dealer = false;
        $scope.sales = false;
        $scope.accountant = false;
        $scope.whmanger = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }
        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.sales = true;
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountant = true;
        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            $scope.accountant = true;
            $scope.userid = window.localStorage['user_id'];
        }

        $scope.deliverDetails = {};
        $scope.deliverDetails.delivery_type = "Vehicle";

        $scope.getChallanList = function () {
            $scope.loading = true;
            $scope.challanCompleteList = [];
            $scope.challanList = [];

            smLoginService.getChallanList(window.localStorage['usertype'], window.localStorage['user_id']).then(function (data) {
                $scope.loading = false;
                if (data.data.Status == 'Success') {
                    $scope.challanList = data.data.more_Info;
                    $scope.data = $scope.challanList;
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                    $scope.billingAddress = data.data.billing_address;
                    $scope.shipingAddress = data.data.shipping_address;
                } else {
                    $scope.challanList = [];
                };
            })
        };
        $scope.getChallanList();

        $scope.showInvoiceData = function (item) {
            $scope.inv_no = item.f_inv_num;
            $scope.po_num = item.po_num;
            $scope.invDate = item.invoice_date;
            $scope.showMoreInvInfo = true;
            window.localStorage['po_num'] = item.po_num
            $scope.showDeliverDetails = true;
        }


        $scope.prepareChallan = function (data) {
            $scope.invoice_info = [];
            $scope.invoice_info.push(data);
            $scope.loading = true;
            smLoginService.whchallanpostMethod($scope.po_num, $scope.inv_no, $scope.invoice_info).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'Succes') {
                    window.localStorage['chalan_num'] = data.data.chalan_num;
                    $scope.challan_num = data.data.chalan_num;
                    // $("#challanGenModal").modal('show');

                    $scope.displayNumber = $scope.challan_num;
                    $scope.displayMsg = "Challan No Generated Successfully!";
                    var modalInstance = $modal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        scope: $scope,
                        templateUrl: 'views/numberModalPopup.html',
                        controller: function ($scope) {
                            $scope.close = function () {
                                modalInstance.dismiss('close');
                                $location.path("challanPrint");
                            }
                        }
                    });

                    $scope.getChallanList();
                    $scope.showMoreInvInfo = false;
                    $scope.showDeliverDetails = false;
                }
            })
        }
        // $scope.closeChalanModal = function () {
        //     $("#challanGenModal").modal('hide');
        //     $location.path("challanPrint");
        // }

        $rootScope.B2BDashboard = function () {
            $location.path("Dashboard");
        }

    }]);


shopMyToolsApp.controller('challanCompletedController', ['$scope', 'smLoginService', '$rootScope', '$location', '$modal',
    function ($scope, smLoginService, $rootScope, $location, $modal) {
        $scope.dealer = false;
        $scope.sales = false;
        $scope.accountant = false;
        $scope.whmanger = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }
        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.sales = true;
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountant = true;
        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            $scope.accountant = true;
            $scope.userid = window.localStorage['user_id'];
        }

        $scope.getChallanCompletedList = function () {
            smLoginService.getChallanCompletedList(window.localStorage['user_id'], window.localStorage['usertype']).then(function (data) {
                if (data.data.status == 'success') {
                    $scope.challanList = data.data.chalan_list;
                    $scope.data = $scope.challanList;
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                    $scope.billingAddress = data.data.billing_address;
                    $scope.shipingAddress = data.data.shipping_address;
                } else {
                    $scope.challanList = [];
                };
            })
        };

        $scope.getChallanCompletedList();


        $scope.showChalanInfo = function (data) {
            $scope.challan_num = data.chalan_num;
            $scope.showChallanDetails = true;
        }

        $rootScope.B2BDashboard = function () {
            $location.path("Dashboard");
        }

    }]);

shopMyToolsApp.controller('challanPrintController', ['$scope', 'smLoginService', '$rootScope', '$location', '$modal',
    function ($scope, smLoginService, $rootScope, $location, $modal) {
        $scope.dealer = false;
        $scope.sales = false;
        $scope.accountant = false;
        $scope.whmanger = false;
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];

        } else if (window.localStorage['usertype'] == "salesManager") {
            $scope.userid = window.localStorage['user_id'];
            $scope.sales = true;
        } else if (window.localStorage['usertype'] == "accountant") {
            $scope.userid = window.localStorage['user_id'];
            $scope.accountant = true;
        } else if (window.localStorage['usertype'] == "whManager") {
            $scope.whmanger = true;
            $scope.accountant = true;
            $scope.userid = window.localStorage['user_id'];
        }

        //  window.localStorage['chalan_num'] = 'CH00001';


        $scope.getReqChallanCompletedList = function () {
            smLoginService.getReqChallanCompletedList(window.localStorage['chalan_num']).then(function (data) {
                if (data.data.status == 'success') {
                    $scope.challanList = data.data.more_Info;
                    $scope.rfqData = data.data.rfq;
                    $scope.total_qty = data.data.total_qty;
                    $scope.delaerData = data.data.dealer;
                    $scope.moreInvInfo = data.data.more_Info[0].moreinv_info;
                } else {
                    $scope.challanList = [];
                };
            })
        };



        $scope.getReqChallanCompletedList();


        $scope.printInvoiceList = function (printSectionId) {
            var printContents = document.getElementById("printSectionId").innerHTML;
            var popupWin = window.open('', '_blank');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"><link href="./css/smtCommonStyles.css" rel="stylesheet"></head><body onload="window.print()">' + printContents + '</body></html>');
            popupWin.document.close();
        };

        $rootScope.B2BDashboard = function () {
            $location.path("Dashboard");
        }

        $scope.gotoChallanGen = function () {
            $location.path("challanGeneration");
        }

    }]);

shopMyToolsApp.controller('inwardGenController', ['$scope', 'smLoginService', '$location', '$modal',
    function ($scope, smLoginService, $location, $modal) {

        $scope.dealer = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];
        };
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }

        $scope.generateInwardNo = function () {
            $scope.loading = true;
            $scope.inwardList = [];
            smLoginService.generateInwardNo(window.localStorage['user_id']).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'success') {
                    $scope.inwardList = data.data.more_Info;
                    $scope.data = $scope.inwardList;
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                } else {
                    $scope.inwardList = [];
                };
            })
        };
        $scope.generateInwardNo();

        $scope.deliverDetails = {};
        $scope.showInwardData = function (item) {
            $scope.inv_no = item.f_inv_num;
            $scope.deliverDetails = item;
            $scope.showMoreInvInfo = true;
        }

        $scope.generateInward = function () {
            $scope.loading = true;
            smLoginService.generateInward($scope.inv_no).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'success') {
                    window.localStorage['inward_num'] = data.data.inward_num;
                    $scope.inward_num = data.data.inward_num;
                    // $("#inwardGenModal").modal('show');

                    $scope.displayNumber = $scope.inward_num;
                    $scope.displayMsg = "Inward No Generated Successfully!";
                    var modalInstance = $modal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        scope: $scope,
                        templateUrl: 'views/numberModalPopup.html',
                        controller: function ($scope) {
                            $scope.close = function () {
                                modalInstance.dismiss('close');
                            }
                        }
                    });

                    $scope.generateInwardNo();
                    $scope.showMoreInvInfo = false;
                } else {
                    alert(data.data.status);
                }
            })
        };

        $scope.B2BDashboard = function () {
            $location.path("Dashboard");
        }

    }]);

shopMyToolsApp.controller('grnPreparationController', ['$scope', 'smLoginService', '$location', '$modal',
    function ($scope, smLoginService, $location, $modal) {

        $scope.dealer = false;
        $scope.btnDisable = false;
        $scope.welcome_msg = window.localStorage['welcome_msg'];
        if (window.localStorage['usertype'] == "Dealer") {
            $scope.dealer = true;
            $scope.userid = window.localStorage['user_id'];
        };
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }
        $scope.getInwardList = function () {
            $scope.loading = true;
            $scope.inwardList = [];

            smLoginService.getInwardList(window.localStorage['user_id'], window.localStorage['usertype']).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'success') {
                    $scope.inwardListData = data.data.more_Info;
                    $scope.data = $scope.inwardListData;
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.data.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;

                    $scope.inwardListData.forEach(function (items) {
                        items.moreinv_info.forEach(function (data) {
                            data.accQty = data.f_inv_qty;
                            data.rejQty = 0;
                        })
                    })
                } else {
                    $scope.inwardListData = [];
                };
            })
        };
        $scope.getInwardList();
        $scope.grnPreparationList = [];
        $scope.showInwardData = function (item) {
            $scope.inv_no = item.f_inv_num;
            $scope.inward_num = item.inward_num;
            $scope.inward_date = item.inward_date;
            $scope.inwardList = item.moreinv_info;
            $scope.showMoreInvInfo = true;

            for (i = 0; i < $scope.inwardList.length; i++) {
                var data = $scope.inwardList[i];
                $scope.pushQty = {
                    "req_product_qty": data.req_product_qty,
                    "total_amt": data.total_amt,
                    "productid": data.productid,
                    "productname": data.productname,
                    "f_inv_qty": data.f_inv_qty,
                    "acpt_inv_qty": data.f_inv_qty,
                    "rej_inv_qty": 0
                }
                $scope.grnPreparationList.push($scope.pushQty);
            }
        };

        $scope.B2BDashboard = function () {
            $location.path("Dashboard");
        };



        $scope.pushAccQty = function (data, accQty) {
            $scope.pushQty = data;
            $scope.qtyCompare = JSON.parse(data.accQty) + JSON.parse(data.rejQty);
            if ($scope.qtyCompare > data.f_inv_qty) {
                alert('Please Enter Quantity Correctly');
                $scope.btnDisable = true;
            } else {
                $scope.btnDisable = false;
                $scope.pushQty = {
                    "req_product_qty": data.req_product_qty,
                    "total_amt": data.total_amt,
                    "productid": data.productid,
                    "productname": data.productname,
                    "f_inv_qty": data.f_inv_qty
                }
                if (accQty >= 0) {
                    $scope.pushQty.acpt_inv_qty = accQty;
                    if ($scope.grnPreparationList.length > 0) {
                        for (i = 0; i < $scope.grnPreparationList.length; i++) {
                            if ($scope.grnPreparationList[i].productid == data.productid) {
                                $scope.grnPreparationList[i].acpt_inv_qty = accQty;
                                $scope.match = false;
                                break;
                            } else {
                                $scope.match = true;
                            }
                        }
                        if ($scope.match) {
                            $scope.grnPreparationList.push($scope.pushQty);
                        }
                    } else {
                        $scope.grnPreparationList.push($scope.pushQty);
                    }
                }
            }

        };

        $scope.pushRejQty = function (data, rejQty) {
            $scope.pushQty = data;
            $scope.pushQty = {
                "req_product_qty": data.req_product_qty,
                "total_amt": data.total_amt,
                "productid": data.productid,
                "productname": data.productname,
                "f_inv_qty": data.f_inv_qty
            }
            if (rejQty >= 0) {
                $scope.pushQty.rej_inv_qty = rejQty;
                if ($scope.grnPreparationList.length > 0) {
                    for (i = 0; i < $scope.grnPreparationList.length; i++) {
                        if ($scope.grnPreparationList[i].productid == data.productid) {
                            $scope.grnPreparationList[i].rej_inv_qty = rejQty;
                            $scope.match = false;
                            break;
                        } else {
                            $scope.match = true;
                        }
                    }
                    if ($scope.match) {
                        $scope.grnPreparationList.push($scope.pushQty);
                    }
                } else {
                    $scope.grnPreparationList.push($scope.pushQty);
                }
            }
        };

        $scope.generateGRN = function () {
            $scope.loading = true;
            smLoginService.generateGRN($scope.grnPreparationList, $scope.inward_num, $scope.inward_date, $scope.userid).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'success') {
                    window.localStorage['grn_num'] = data.data.grn_num;
                    $scope.grn_num = data.data.grn_num;
                    // $("#grnGenModal").modal('show');

                    $scope.displayNumber = $scope.grn_num;
                    $scope.displayMsg = "GRN Generated Successfully!";
                    var modalInstance = $modal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        scope: $scope,
                        templateUrl: 'views/numberModalPopup.html',
                        controller: function ($scope) {
                            $scope.close = function () {
                                modalInstance.dismiss('close');
                            }
                        }
                    });

                    $scope.getInwardList();
                    $scope.showMoreInvInfo = false;
                } else {
                    alert(data.data.status);
                }
            });
        };

    }]);


shopMyToolsApp.controller('grnDelaerOrderListController', ['$scope', 'smLoginService', '$location', '$modal',
    function ($scope, smLoginService, $location, $modal) {

        $scope.welcome_msg = window.localStorage['welcome_msg'];
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }
        $scope.getGrnList = function () {
            $scope.loading = true;
            smLoginService.getGrnList(window.localStorage['user_id']).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'success') {
                    $scope.dealerGrnList = data.data.more_Info;
                    $scope.totalItems = $scope.dealerGrnList.length;
                } else {
                    $scope.dealerGrnList = [];
                };
            })
        };
        $scope.getGrnList();

        $scope.showInwardData = function (item) {
            $scope.inv_no = item.f_inv_num;
            $scope.inward_num = item.inward_num;
            $scope.inward_date = item.inward_date;
            $scope.inwardList = item.moreinv_info;
            $scope.showMoreInvInfo = true;
        };
        $scope.B2BDashboard = function () {
            $location.path("Dashboard");
        };

    }]);

shopMyToolsApp.controller('poHistoryController', ['$scope', 'smLoginService', '$location', '$modal',
    function ($scope, smLoginService, $location, $modal) {

        $scope.welcome_msg = window.localStorage['welcome_msg'];
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }
        $scope.getPoRecords = function () {
            $scope.loading = true;
            smLoginService.getPoRecords(window.localStorage['user_id'], window.localStorage['usertype']).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'success') {
                    $scope.dealerGrnList = data.data.poinfo;
                    $scope.totalItems = $scope.dealerGrnList.length;
                } else {
                    // alert(data.data.status);
                    $scope.dealerGrnList = [];
                };
            })
        };
        $scope.getPoRecords();

        $scope.showInwardData = function (item) {
            $scope.po_num = item.po_num;
            $scope.inwardList = item.product_info;
            $scope.showMoreInvInfo = true;
        };
        $scope.B2BDashboard = function () {
            $location.path("Dashboard");
        };

    }]);

shopMyToolsApp.controller('invoiceHistoryController', ['$scope', 'smLoginService', '$location', '$modal',
    function ($scope, smLoginService, $location, $modal) {

        $scope.welcome_msg = window.localStorage['welcome_msg'];
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        if (window.localStorage['token'] == undefined) {
            $location.path('/');
        }
        $scope.getInvoiceRecords = function () {
            $scope.loading = true;
            smLoginService.getInvoiceRecords(window.localStorage['user_id'], window.localStorage['usertype']).then(function (data) {
                $scope.loading = false;
                if (data.data.status == 'success') {
                    $scope.dealerGrnList = data.data.invinfo;
                    $scope.totalItems = $scope.dealerGrnList.length;
                } else {
                    $scope.dealerGrnList = [];
                };
            })
        };
        $scope.getInvoiceRecords();

        $scope.showInwardData = function (item) {
            $scope.po_num = item.po_num;
            $scope.inwardList = item.moreinv_info;
            $scope.showMoreInvInfo = true;
        };
        $scope.B2BDashboard = function () {
            $location.path("Dashboard");
        };

    }]);