shopMyToolsApp.controller('barcodecnt', ['$scope', '$http', '$location', '$rootScope', '$filter', '$window', 'getmobileservice', 'registrationService', '$modal',
  function ($scope, $http, $location, $rootScope, $filter, $window, getmobileservice, registrationService, $modal) {
    $scope.registrationData = { "gstnumber": '', "street": '', "city": '', "pincode": '', "state": '' }
    $scope.getDetailsShow = false;
    $scope.productsShow = false;
    $scope.B2BDashboard = function () {
      $location.path('Dashboard');
    }
    if (window.localStorage['token'] == undefined) {
      $location.path('/');
    }

    $scope.getmobile = function (mobile) {
      // alert(mobile)
      getmobileservice.getaddress(mobile).then(function (data) {
        // console.log(data.data)
        if (data.data.status == "Mobile number is not registered") {
          alert("Mobile number is not registered please register")
        } else if (data.data.status == 'success') {
          $scope.getDetailsShow = true;
          $scope.shippedTo = data.data.Shiiping_address;
          $scope.billedTo = data.data.Billing_address;
          window.localStorage['mobile_no'] = data.data.mobile;
        }
      })
    }

    $scope.barcodeUserReg = function () {
      var modalInstance = $modal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        scope: $scope,
        templateUrl: 'views/barcodeModal.html',
        controller: function ($scope) {
          $scope.close = function () {
            modalInstance.dismiss('close');
          }
        }
      });
    }

    $scope.adduser = function (reg) {
      window.localStorage['mobile'] = reg.user_mobile;
      $scope.shippingaddress = [{
        "full_name": reg.firstname + reg.lastname, "mobile": reg.user_mobile, "d_no": '',
        "street": reg.street, "city": reg.city, "state": reg.state, "zipcode": reg.pincode
      }];
      $scope.billingaddress = $scope.shippingaddress;
      // alert(JSON.stringify($scope.shippingaddress))
      getmobileservice.userRegistration(reg, $scope.shippingaddress).then(function (data) {
        // console.log(data.data)
        if (data.data.status == "Customer registred successfully") {
          alert("Customer registered successfully")
          $("#barcodeModel").modal('hide');
          //  $("#otpmodel").modal('show');
        }
      })
    }

    $scope.sendotp = function (otp) {
      registrationService.verifyOTP(otp, window.localStorage['mobile'], '').then(function (data) {
        console.log(data.data)
        if (data.data.status == "Data saved successfully") {
          //   alert("Mobile number is not registered please register")
          // $("#barcodeModel").modal('hide');
          // $("#otpmodel").modal('show');
          alert("User Added Successfully")
        }
      })
    }
    $scope.productdetal = [];

    $scope.getdetails = function (bar) {
      // alert(bar)
      getmobileservice.getproductdetails(bar).then(function (data) {
        console.log(data.data)
        if (data.data.status == "success") {
          $scope.productsShow = true;
          $scope.productdetal.push(data.data.product_details);

          //   alert("Mobile number is not registered please register")
          // $("#barcodeModel").modal('hide');
          // $("#otpmodel").modal('show');
        }
      })
    }

    $scope.desc = function (productdetaldata) {
      if (productdetaldata.qty != 0) {
        productdetaldata.qty--;
      }
      else {
        return false;
      }
    }

    $scope.insc = function (productdetaldata) {

      productdetaldata.qty++;
    }

    $scope.invgenerate = function () {

      $scope.date = new Date();
      $scope.moreInfo = [];
      $scope.productdetal.forEach(function (product) {
        $scope.moreInfo.push({
          "total_amt": product.qty * product.MRP, "productname": product.proddescription,
          "req_product_qty": product.qty, "productid": product.productid, "mrp": product.MRP, "brand": product.brand, "hsn": product.upload_hsncode
        });

      })
      getmobileservice.saveorder($scope.date, window.localStorage['user_id'], $scope.moreInfo).then(function (data) {
        // console.log(data)
        if (data.data.status == 'success') {
          window.localStorage['f_inv'] = data.data.invdata;
          $scope.productsShow = false;
          $scope.getDetailsShow = false;
          alert('Order placed Successfully')
          $scope.productdetal = [];
          $location.path("pos_finv");
        }
      })
    }




  }]);

shopMyToolsApp.controller('posFinvController', ['$scope', '$http', '$location', '$rootScope', 'getmobileservice', '$modal',
  function ($scope, $http, $location, $rootScope, getmobileservice, $modal) {
    $scope.finalinvoice = function () {
      getmobileservice.getPosf_invMethod(window.localStorage['f_inv'], window.localStorage['mobile_no'], window.localStorage['user_id']).then(function (data) {
        // console.log(data)
        if (data.data.Status == 'Success') {
          $scope.shippedTo = data.data.shipping;
          $scope.billedTo = data.data.billing;
          $scope.companyinfo = data.data.company_info;
          $scope.invinfo = data.data.more_Info;
          $scope.dealerinfo = data.data.data;
          $scope.totalamount = data.data.total;
        }
      })

    }
    $scope.finalinvoice();

    $scope.printInvoiceList = function (printSectionId) {
      var printContents = document.getElementById("printSectionId").innerHTML;
      var popupWin = window.open('', '_blank');
      popupWin.document.open();
      popupWin.document.write('<html><head><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"><link href="./css/smtCommonStyles.css" rel="stylesheet"></head><body onload="window.print()">' + printContents + '</body></html>');
      popupWin.document.close();

    }
    $scope.pos = function () {
      $location.path('pos_finv');
    }


  }])